-- phpMyAdmin SQL Dump
-- version 4.6.6deb4+deb9u2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Ноя 25 2020 г., 10:14
-- Версия сервера: 5.7.28
-- Версия PHP: 7.0.33-0+deb9u10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `thekeys`
--

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_blog`
--

CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_post`
--

CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_post_to_tag`
--

CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_tag`
--

CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_user_to_blog`
--

CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_carbrands`
--

CREATE TABLE `yupe_carbrands` (
  `id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'Родитель',
  `page_id` int(11) DEFAULT NULL COMMENT 'Страница',
  `city_id` int(11) DEFAULT NULL COMMENT 'Город',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое название',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `slug` varchar(255) DEFAULT NULL COMMENT 'Alias',
  `name_h1` varchar(255) DEFAULT NULL COMMENT 'Заголовок на странице',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description_short` text COMMENT 'Короткое описание',
  `description` text COMMENT 'Описание',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Title (SEO)',
  `meta_keywords` text COMMENT 'Ключевые слова SEO',
  `meta_description` text COMMENT 'Описание SEO',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `back` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_carbrands`
--

INSERT INTO `yupe_carbrands` (`id`, `create_user_id`, `update_user_id`, `create_time`, `update_time`, `parent_id`, `page_id`, `city_id`, `name_short`, `name`, `slug`, `name_h1`, `image`, `description_short`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `position`, `back`) VALUES
(1, 1, 1, '2020-11-21 21:56:48', '2020-11-21 23:11:07', NULL, NULL, 1, 'Hyundai', 'Hyundai', 'hyundai', '', 'c1df876886790f91e147ad143e2f744f.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 1, NULL),
(2, 1, 1, '2020-11-21 21:57:26', '2020-11-21 23:11:33', NULL, NULL, 1, 'Subaru', 'Subaru', 'subaru', '', '416dfc6527daddfa62c4b99dffd19be9.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 2, NULL),
(3, 1, 1, '2020-11-21 21:57:54', '2020-11-21 23:12:24', NULL, NULL, 1, 'Opel', 'Opel', 'opel', '', 'fa500046cecaaa2ef44041f0f466592c.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 3, NULL),
(4, 1, 1, '2020-11-21 21:58:34', '2020-11-21 23:12:50', NULL, NULL, 1, 'Lexus', 'Lexus', 'lexus', '', 'b83e4174f19b29effb25215c7edb3258.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 4, NULL),
(5, 1, 1, '2020-11-21 21:59:07', '2020-11-21 23:13:16', NULL, NULL, 1, 'Daewoo', 'Daewoo', 'daewoo', '', '4049a535d290feb8446b03694578d415.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 5, NULL),
(6, 1, 1, '2020-11-21 21:59:39', '2020-11-24 00:07:00', NULL, NULL, 1, 'Audi', 'Audi', 'audi', 'Открыть АУДИ', 'bb44cbbb034ee3e4cdd36480b0000f5c.jpg', '', '<h2>Не открывается АУДИ? Откроем в Оренбурге и Оренбургской области</h2>\r\n<ul class=\"ul-style pull-left\">\r\n<li>Работаем со всеми марками<br />и моделями автомобилей</li>\r\n<li>Используем современные<br />инструменты и методы</li>\r\n<li>В команде специалисты<br />с опытом более 5 лет</li>\r\n</ul>\r\n<p>Закрылся автомобиль и не открывается? <br />Не знаете, как попасть в салон собственной машины? <br />Не спешите паниковать, обратитесь в нашу службу! Мы работаем на всей территории Оренбурге и Оренбургской области, круглосуточно помогаем вскрывать авто любых марок и моделей, делаем это аккуратно и без малейших повреждений.</p>\r\n<p>Попали в экстренную ситуацию? <br />Наш мастер приедет в течение 20 минут в любую точку и поможет вскрыть Audi любой модели: А6, А4, Audi 80, Q7, Audi 100, A3, A8, A5, Q5.</p>\r\n<h3>Цены по вскрытию</h3>\r\n<table class=\"price\">\r\n<tbody>\r\n<tr>\r\n<td>Багажника, капота, бензобака</td>\r\n<td>от 780 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Дверей автомобиля</td>\r\n<td>от 920 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Блокиратора руля</td>\r\n<td>от 1000 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Замки коробки передач</td>\r\n<td>от 1480 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Авто премиум-класса</td>\r\n<td>от 1800 р.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>Когда может понадобиться экстренное вскрытие замков Audi?</h3>\r\n<ol>\r\n<li>Произошла самопроизвольная блокировка авто, ключи остались внутри;</li>\r\n<li>Замочные механизмы вышли из строя;</li>\r\n<li>Ключи были утеряны;</li>\r\n<li>Перестал открываться багажник, капот, лючок бензобака, неисправен брелок управления ЦЗ;</li>\r\n<li>Открыть машину не получается: ключ сломался, его обломок застрял в замочной скважине;</li>\r\n<li>Заклинил бардачок;</li>\r\n<li>Сел аккумулятор авто.</li>\r\n</ol>\r\n<h3>Как открыть капот у Ауди?</h3>\r\n<p><img class=\"pull-right\" style=\"width: 420px;\" src=\"/uploads/image/a7ce2f996de81f97e3be89c35d0d9f71.jpg\" alt=\"\" />Передняя крышка авто может перестать открываться обычным способом по разным причинам &ndash; из-за поломки или засорения замка, из-за обрыва тросика.</p>\r\n<p>Если причина заключается в неисправности замочного механизма, можно воспользоваться обычной отверткой: её нужно аккуратно вставить в подкапотное пространство через зазор между передней фарой и её местом крепления, нащупать ушко замка, толкнуть его в направлении радиатора. Если причина в засорении &ndash; можно воспользоваться специальной жидкостью WD-40. Это средство эффективно удаляет скопления пыли и частички грязи, простое в применении &ndash; его достаточно просто залить внутрь замка на несколько минут.</p>\r\n<p>Когда поднять капот не получается по причине обрыва троса, поможет любой металлический прутик. На конце прутика нужно сделать крючок, вставить приспособление под крышку капота через отверстие на защитной решетке радиатора, нащупать конец троса и резко дернуть. Проблема решена.</p>\r\n<h3>Не получается открыть дверь в Ауди</h3>\r\n<p>То, как выполнить вскрытие двери Audi, зависит от конкретной модели. Есть несколько простых способов:</p>\r\n<ol>\r\n<li>Можно взять небольшой нож, аккуратно отклеить им маленькое боковое стекло на задней двери, дотянуться через образовавшийся проем до внутренней ручки открывания, попасть в салон. Затем из салона выполнить открывание остальных дверей. Стекло затем можно приклеить обратно, используя герметик;</li>\r\n<li>Ещё один метод открыть Ауди &ndash; взять длинную металлическую линейку, аккуратно вставить её в щель между стеклом и корпусом водительской двери, внутри дверной конструкции нащупать тягу замка и надавить на неё;</li>\r\n<li>Бывает, что из-за обрыва одного из контактов ЦЗ блокируются все замки и отрывается только одна задняя дверь. В такой ситуации можно через заднюю дверь попасть в салон автомобиля, а из салона &ndash; в багажник, откинув спинки заднего сидения. Когда будет открыт багажник, нужно демонтировать пластиковую защиту с проводами, найти поврежденный контакт и восстановить его;</li>\r\n<li>Если ключи были захлопнуты в салоне, вместо механического ключа для поворота личинки в двери можно воспользоваться простой отверткой.</li>\r\n</ol>\r\n<h3>Как открыть багажник Ауди?</h3>\r\n<p><img class=\"pull-left\" style=\"width: 420px;\" src=\"/uploads/image/b3f05d11250eb14e3a49e48872fdb151.jpg\" alt=\"\" />Проблемы с открыванием пятой двери автомобиля возникают, как правило, поздней осенью и зимой. Причина &ndash; замерзание конденсата, скопившегося внутри замков. Есть несколько довольно простых решений в данной ситуации. Можно воспользоваться специальной размораживающей жидкостью, приобрести которую рекомендуется каждому автовладельцу перед наступлением холодов, либо отогреть примерзшие механизмы феном. Также можно взять механический ключ и перед тем, как вставить в личинку замка, нагреть его пламенем от зажигалки.</p>\r\n<p>Если неприятная ситуация с багажным отсеком машины произошла летом по причине заклинивания замка, открыть можно так:</p>\r\n<ol>\r\n<li>Откинуть в салоне спинки заднего сидения, залезть в багажник изнутри;</li>\r\n<li>Снять часть внутренней обшивки в области расположения замка, демонтировать пластиковую заглушку;</li>\r\n<li>Плоской отверткой поддеть фиксатор внутри замка и сдвинуть его в сторону;</li>\r\n<li>Затем подойти к багажной крышке снаружи и поднять её рукой.</li>\r\n</ol>\r\n<p>Если выполнить открывание багажника нужно срочно, можно разбить заднюю фару машины и через неё дотянуться до внутренней стороны запирающего механизма. Однако затем придется потратить денежные средства на замену фары.</p>\r\n<p>В любой ситуации, когда не открывается дверь, капот или багажник автомобиля, рекомендуется обратиться к квалифицированному мастеру. Он при помощи профессиональных навыков и инструментов выполнит взлом за считанные минуты и без малейших повреждений кузова и ЛКП.</p>\r\n<p>Все мастера по вскрытию авто из нашей службы имеют опыт работы более 5 лет, знают особенности всех марок и моделей машин.</p>\r\n<p class=\"color-orange\"><strong>Не открывается Audi? Звоните, мастер приедет через 20 минут!</strong></p>', '', '', '', 1, 6, '4d105109885ea2e921b059e8a42d93fa.jpg'),
(7, 1, 1, '2020-11-21 22:02:46', '2020-11-21 23:13:52', NULL, NULL, 1, 'Kia', 'Kia', 'kia', '', '4efd4e3525bb9d81666db3f4d3a8e3f0.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 7, NULL),
(8, 1, 1, '2020-11-21 23:14:37', '2020-11-21 23:14:37', NULL, NULL, 1, 'Renault', 'Renault', 'renault', '', '156fe3e42fcf89ce21b85a26134e2d66.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 8, NULL),
(9, 1, 1, '2020-11-21 23:15:23', '2020-11-21 23:15:23', NULL, NULL, 1, 'Mercedes Benz', 'Mercedes Benz', 'mercedes-benz', '', 'ca461f0a616ee738f0abcd42212759e0.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 9, NULL),
(10, 1, 1, '2020-11-21 23:15:47', '2020-11-21 23:17:58', NULL, NULL, 1, 'Toyota', 'Toyota', 'toyota', '', '8484a83781ae4f8cff2cdfc6d6f998ea.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 10, NULL),
(11, 1, 1, '2020-11-21 23:17:03', '2020-11-21 23:17:03', NULL, NULL, 1, 'BMW', 'BMW', 'bmw', '', '311d4059c485289215e480434982108b.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 11, NULL),
(12, 1, 1, '2020-11-21 23:33:54', '2020-11-21 23:34:13', NULL, NULL, 2, 'Audi', 'Audi', 'audi', '', '496c1743e7accaf444042cece7ee5ec2.jpg', '', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 12, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_category_category`
--

CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_city`
--

CREATE TABLE `yupe_city` (
  `id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'Родитель',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое название',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `slug` varchar(255) DEFAULT NULL COMMENT 'Alias',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `phone` text COMMENT 'Телефон',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail',
  `mode` varchar(255) DEFAULT NULL COMMENT 'График работы',
  `address` text COMMENT 'Адрес',
  `code_map` text COMMENT 'Код карты',
  `coords` varchar(255) DEFAULT NULL COMMENT 'Координаты на карте',
  `description` text COMMENT 'Описание',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Title (SEO)',
  `meta_keywords` text COMMENT 'Ключевые слова SEO',
  `meta_description` text COMMENT 'Описание SEO',
  `whatsapp` varchar(255) DEFAULT NULL COMMENT 'Whatsapp',
  `viber` varchar(255) DEFAULT NULL COMMENT 'Viber',
  `telegram` varchar(255) DEFAULT NULL COMMENT 'Telegram',
  `vk` varchar(255) DEFAULT NULL COMMENT 'Vkontakte',
  `instagram` varchar(255) DEFAULT NULL COMMENT 'Instagram',
  `facebook` varchar(255) DEFAULT NULL COMMENT 'Facebook',
  `ok` varchar(255) DEFAULT NULL COMMENT 'Odnoklasniki',
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Город по-умолчанию',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `notifyEmailsTo` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_city`
--

INSERT INTO `yupe_city` (`id`, `create_user_id`, `update_user_id`, `create_time`, `update_time`, `parent_id`, `name_short`, `name`, `slug`, `image`, `phone`, `email`, `mode`, `address`, `code_map`, `coords`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `whatsapp`, `viber`, `telegram`, `vk`, `instagram`, `facebook`, `ok`, `is_default`, `status`, `position`, `notifyEmailsTo`) VALUES
(1, 1, 1, '2020-11-12 09:49:46', '2020-11-22 16:12:29', NULL, 'Оренбург', 'Оренбург', 'orenburg', NULL, '<a href=\"tel:+79225473156\">+7 (922) 547-31-56</a>', '<a href=\"mailto:otkroem56@mail.ru\">otkroem56@mail.ru</a>', 'Работаем <br>круглосуточно', 'ул. Монтажников, 23, ТЦ Строй-Сити, 2-й этаж', 'https://yandex.ru/map-widget/v1/-/CCUAj-bP-D', '[51.802146, 55.155166]', '', '', '', '', '+79225473156', '+79225473156', '@telegram', '', '', '', '', 1, 1, 1, 'oren@otkroem.ru,oren2@otkroem.ru'),
(2, 1, 1, '2020-11-12 09:50:49', '2020-11-22 16:03:59', NULL, 'Санкт-Петербург', 'Санкт-Петербург', 'sankt-peterburg', NULL, '<a href=\"tel:+79333333156\">+7 (933) 333-31-56</a>', '<a href=\"mailto:otkroem@mail.ru\">otkroem@mail.ru</a>', 'Работаем <br>круглосуточно', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, 2, 'sankt-peterburg@otkroem.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_comment_comment`
--

CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_contentblock_content_block`
--

CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_contentblock_content_block`
--

INSERT INTO `yupe_contentblock_content_block` (`id`, `name`, `code`, `type`, `content`, `description`, `category_id`, `status`) VALUES
(5, 'Скрипты в шапке', 'skripty-v-shapke', 1, '', '', NULL, 1),
(6, 'Скрипты в футере', 'skripty-v-futere', 1, '', '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_gallery_gallery`
--

CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_gallery_gallery`
--

INSERT INTO `yupe_gallery_gallery` (`id`, `name`, `description`, `status`, `owner`, `preview_id`, `category_id`, `sort`, `parent_id`, `city_id`) VALUES
(2, 'Ключи', '<p>Галерея ключей</p>', 1, 1, NULL, NULL, 2, NULL, NULL),
(3, 'Преимущества', '<p>Преимущества на главной</p>', 1, 1, NULL, NULL, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_gallery_image_to_gallery`
--

CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_gallery_image_to_gallery`
--

INSERT INTO `yupe_gallery_image_to_gallery` (`id`, `image_id`, `gallery_id`, `create_time`, `position`) VALUES
(35, 35, 2, '2020-11-13 23:32:32', 10),
(36, 36, 2, '2020-11-13 23:32:32', 11),
(37, 37, 2, '2020-11-13 23:32:33', 12),
(38, 38, 2, '2020-11-13 23:32:34', 13),
(39, 39, 2, '2020-11-13 23:32:34', 14),
(40, 40, 2, '2020-11-13 23:32:35', 15),
(41, 41, 2, '2020-11-13 23:32:35', 16),
(42, 42, 2, '2020-11-13 23:32:36', 17),
(43, 43, 2, '2020-11-13 23:32:36', 18),
(44, 44, 2, '2020-11-13 23:32:36', 19),
(45, 45, 2, '2020-11-13 23:42:28', 20),
(46, 46, 2, '2020-11-13 23:42:28', 21),
(47, 47, 2, '2020-11-13 23:42:29', 22),
(48, 48, 2, '2020-11-13 23:42:29', 23),
(49, 49, 2, '2020-11-13 23:42:30', 24),
(50, 50, 2, '2020-11-13 23:42:30', 25),
(51, 51, 2, '2020-11-13 23:42:30', 26),
(52, 52, 2, '2020-11-13 23:42:31', 27),
(53, 53, 2, '2020-11-13 23:42:31', 28),
(54, 54, 2, '2020-11-13 23:42:31', 29),
(55, 55, 3, '2020-11-14 14:46:07', 30),
(56, 56, 3, '2020-11-14 14:46:09', 31),
(57, 57, 3, '2020-11-14 14:46:11', 32),
(58, 58, 3, '2020-11-14 14:46:13', 33),
(59, 59, 3, '2020-11-14 14:46:14', 34),
(60, 60, 3, '2020-11-14 14:46:16', 35);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_image_image`
--

CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_image_image`
--

INSERT INTO `yupe_image_image` (`id`, `category_id`, `parent_id`, `name`, `description`, `file`, `create_time`, `user_id`, `alt`, `type`, `status`, `sort`) VALUES
(1, NULL, NULL, 'Mazda', '', 'defb542e6a35018a51f48e49a51f95fc.png', '2020-11-12 00:02:02', 1, 'Mazda', 0, 1, 1),
(2, NULL, NULL, 'Toyota', '', '222d04e6c0e30ddb47aa1a7bd4294894.png', '2020-11-12 00:02:02', 1, 'Toyota', 0, 1, 2),
(3, NULL, NULL, 'Mini', '', '10de43cc327a7b498d33362ca666309d.png', '2020-11-12 00:02:03', 1, 'Mini', 0, 1, 3),
(4, NULL, NULL, 'Hyundai', '', 'fa4343f722eb9b3e195a7b5b632de485.png', '2020-11-12 00:02:04', 1, 'Hyundai', 0, 1, 4),
(5, NULL, NULL, 'Subaru', '', '51af1cf395c5fbfaafea6cfb1d376d0a.png', '2020-11-12 00:02:04', 1, 'Subaru', 0, 1, 5),
(6, NULL, NULL, 'Opel', '', '4fe3da61bbffef6b01cd5b253ee95d08.png', '2020-11-12 00:02:05', 1, 'Opel', 0, 1, 6),
(7, NULL, NULL, 'Lexus', '', 'b5c224c680b27aadc95c413ecd7c68d3.png', '2020-11-12 00:02:05', 1, 'Lexus', 0, 1, 7),
(8, NULL, NULL, 'Daewoo', '', '6c9f6cff203a9be435a33f01e02396b8.png', '2020-11-12 00:02:06', 1, 'Daewoo', 0, 1, 8),
(9, NULL, NULL, 'Toyota', '', 'c14a5a4871a77b6727544a4a5c80245e.png', '2020-11-13 00:08:20', 1, 'Toyota', 0, 1, 9),
(35, NULL, NULL, '1', '', '1d7a809e7234b3fa77fa3efd89d14106.png', '2020-11-13 23:32:32', 1, '1', 0, 1, 10),
(36, NULL, NULL, '2', '', '26ebf820530ca9999c8a34e966ec3b5a.png', '2020-11-13 23:32:32', 1, '2', 0, 1, 11),
(37, NULL, NULL, '4', '', 'c0bd24cde041b924b77bb1d0d37c5b65.png', '2020-11-13 23:32:33', 1, '4', 0, 1, 12),
(38, NULL, NULL, '3', '', '26bac43cbc8aafc422d2ef2d2f9303c9.png', '2020-11-13 23:32:34', 1, '3', 0, 1, 13),
(39, NULL, NULL, '5', '', '9c4ee01983765cee2bba02e752ee8431.png', '2020-11-13 23:32:34', 1, '5', 0, 1, 14),
(40, NULL, NULL, '6', '', '79cdcb6bbbec4b4eed0fef79e89b2a3d.png', '2020-11-13 23:32:35', 1, '6', 0, 1, 15),
(41, NULL, NULL, '7', '', '687cfe61d68a4184c972e3a580872de1.png', '2020-11-13 23:32:35', 1, '7', 0, 1, 16),
(42, NULL, NULL, '8', '', 'a6da04784827567f947f7532c09ada08.png', '2020-11-13 23:32:36', 1, '8', 0, 1, 17),
(43, NULL, NULL, '9', '', 'da74169081ed1f54a9757391a04d2707.png', '2020-11-13 23:32:36', 1, '9', 0, 1, 18),
(44, NULL, NULL, '10', '', 'd7fb143acec6bc41f8b2ab66b268f3d5.png', '2020-11-13 23:32:36', 1, '10', 0, 1, 19),
(45, NULL, NULL, '1', '', 'bc517414544b47d4e1a2480a0602babd.png', '2020-11-13 23:42:28', 1, '1', 0, 1, 20),
(46, NULL, NULL, '3', '', 'c65a114aba6e7f31d9538d1c9e143f80.png', '2020-11-13 23:42:28', 1, '3', 0, 1, 21),
(47, NULL, NULL, '4', '', '6824b0e978c190580ca0aa987a9b0d9d.png', '2020-11-13 23:42:29', 1, '4', 0, 1, 22),
(48, NULL, NULL, '2', '', 'd5a0e048c93611aba45c986751605668.png', '2020-11-13 23:42:29', 1, '2', 0, 1, 23),
(49, NULL, NULL, '5', '', 'd594ae134921c8c4f8748fd6f9fcdb8b.png', '2020-11-13 23:42:29', 1, '5', 0, 1, 24),
(50, NULL, NULL, '6', '', '9932b3f83c2a51f44b1374ab50e562dd.png', '2020-11-13 23:42:30', 1, '6', 0, 1, 25),
(51, NULL, NULL, '7', '', '7479126ef2afd51f67a58654cdf43cf3.png', '2020-11-13 23:42:30', 1, '7', 0, 1, 26),
(52, NULL, NULL, '8', '', '90c184b82d45eb4eab1608787ace3117.png', '2020-11-13 23:42:31', 1, '8', 0, 1, 27),
(53, NULL, NULL, '9', '', '24c56bd3ff8ebf5f01f083625ea5c853.png', '2020-11-13 23:42:31', 1, '9', 0, 1, 28),
(54, NULL, NULL, '10', '', '6f2dd4edb1611b1f1e02882fabbded95.png', '2020-11-13 23:42:31', 1, '10', 0, 1, 29),
(55, NULL, NULL, 'Мы приедем к вам <br />сами и все сделаем <br />на месте', '', 'd9158476545dac1310b086df62f724fe.png', '2020-11-14 14:46:07', 1, '', 0, 1, 30),
(56, NULL, NULL, 'Выезд по области, <br />в том числе <br />в удаленные районы', '', '9d80907fca7d807dbda174f5b3f45bec.png', '2020-11-14 14:46:09', 1, '', 0, 1, 31),
(57, NULL, NULL, 'Честные цены, <br />возможна рассрочка', '', '50833e98e49bfa80f2935836974207a4.png', '2020-11-14 14:46:11', 1, '', 0, 1, 32),
(58, NULL, NULL, 'Опытные <br />и отзывчивые мастера', '', 'd3e68187401437c678e40398d0cf6fb5.png', '2020-11-14 14:46:13', 1, '', 0, 1, 33),
(59, NULL, NULL, 'Вскрытие авто <br />без повреждений', '', '91948d19f8df3a3a1a65b6ee61ae876b.png', '2020-11-14 14:46:14', 1, '', 0, 1, 34),
(60, NULL, NULL, 'Все ключи в наличии', '', 'a428d50b582614ee3525bbb9ffefa554.png', '2020-11-14 14:46:16', 1, '', 0, 1, 35);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_mail_mail_event`
--

CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_mail_mail_template`
--

CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_menu_menu`
--

CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_menu_menu`
--

INSERT INTO `yupe_menu_menu` (`id`, `name`, `code`, `description`, `status`) VALUES
(1, 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', 1),
(2, 'Нижнее меню', 'bottom-menu', 'Нижнее меню под контактами', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_menu_menu_item`
--

CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_menu_menu_item`
--

INSERT INTO `yupe_menu_menu_item` (`id`, `parent_id`, `menu_id`, `regular_link`, `title`, `href`, `class`, `title_attr`, `before_link`, `after_link`, `target`, `rel`, `condition_name`, `condition_denial`, `sort`, `status`, `entity_module_name`, `entity_name`, `entity_id`) VALUES
(12, 0, 1, 1, 'Услуги', '/services', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 1, 1, NULL, NULL, NULL),
(13, 0, 1, 1, 'Прайс-лист', '/prays-list', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 2, 1, NULL, NULL, NULL),
(14, 0, 1, 1, 'Марки авто', '/carbrands', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 3, 1, NULL, NULL, NULL),
(16, 0, 1, 1, 'Отзывы клиентов', '/reviews', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 5, 1, NULL, NULL, NULL),
(17, 0, 1, 1, 'Контакты', '/contacts', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 6, 1, NULL, NULL, NULL),
(18, 0, 2, 1, 'Задать вопрос', '#', '', '', '', '', 'modal', '#questionModal', '', 0, 7, 1, '', '', NULL),
(19, 0, 2, 1, 'Политика конфиденциальности', '/policy', '', '', '', '', '', '', '', 0, 8, 1, '', '', NULL),
(20, 12, 1, 1, '{{service}}', '#', '', '', '', '', '', '', '', 0, 9, 1, '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_migrations`
--

CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_migrations`
--

INSERT INTO `yupe_migrations` (`id`, `module`, `version`, `apply_time`) VALUES
(1, 'user', 'm000000_000000_user_base', 1545824978),
(2, 'user', 'm131019_212911_user_tokens', 1545824978),
(3, 'user', 'm131025_152911_clean_user_table', 1545824979),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1545824980),
(5, 'user', 'm131106_111552_user_restore_fields', 1545824980),
(6, 'user', 'm131121_190850_modify_tokes_table', 1545824981),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1545824981),
(8, 'user', 'm150416_113652_rename_fields', 1545824981),
(9, 'user', 'm151006_000000_user_add_phone', 1545824981),
(10, 'yupe', 'm000000_000000_yupe_base', 1545824982),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1545824982),
(12, 'yupe', 'm150416_125517_rename_fields', 1545824983),
(13, 'yupe', 'm160204_195213_change_settings_type', 1545824983),
(14, 'category', 'm000000_000000_category_base', 1545824984),
(15, 'category', 'm150415_150436_rename_fields', 1545824984),
(16, 'image', 'm000000_000000_image_base', 1545824986),
(17, 'image', 'm150226_121100_image_order', 1545824986),
(18, 'image', 'm150416_080008_rename_fields', 1545824986),
(19, 'page', 'm000000_000000_page_base', 1545824988),
(20, 'page', 'm130115_155600_columns_rename', 1545824988),
(21, 'page', 'm140115_083618_add_layout', 1545824988),
(22, 'page', 'm140620_072543_add_view', 1545824989),
(23, 'page', 'm150312_151049_change_body_type', 1545824989),
(24, 'page', 'm150416_101038_rename_fields', 1545824989),
(25, 'page', 'm180224_105407_meta_title_column', 1545824989),
(26, 'page', 'm180421_143324_update_page_meta_column', 1545824989),
(27, 'mail', 'm000000_000000_mail_base', 1545824991),
(28, 'comment', 'm000000_000000_comment_base', 1545824992),
(29, 'comment', 'm130704_095200_comment_nestedsets', 1545824994),
(30, 'comment', 'm150415_151804_rename_fields', 1545824994),
(31, 'notify', 'm141031_091039_add_notify_table', 1545824994),
(32, 'blog', 'm000000_000000_blog_base', 1545825001),
(33, 'blog', 'm130503_091124_BlogPostImage', 1545825001),
(34, 'blog', 'm130529_151602_add_post_category', 1545825002),
(35, 'blog', 'm140226_052326_add_community_fields', 1545825003),
(36, 'blog', 'm140714_110238_blog_post_quote_type', 1545825003),
(37, 'blog', 'm150406_094809_blog_post_quote_type', 1545825004),
(38, 'blog', 'm150414_180119_rename_date_fields', 1545825004),
(39, 'blog', 'm160518_175903_alter_blog_foreign_keys', 1545825005),
(40, 'blog', 'm180421_143937_update_blog_meta_column', 1545825005),
(41, 'blog', 'm180421_143938_add_post_meta_title_column', 1545825006),
(42, 'contentblock', 'm000000_000000_contentblock_base', 1545825007),
(43, 'contentblock', 'm140715_130737_add_category_id', 1545825007),
(44, 'contentblock', 'm150127_130425_add_status_column', 1545825007),
(45, 'menu', 'm000000_000000_menu_base', 1545825009),
(46, 'menu', 'm121220_001126_menu_test_data', 1545825009),
(47, 'menu', 'm160914_134555_fix_menu_item_default_values', 1545825010),
(48, 'menu', 'm181214_110527_menu_item_add_entity_fields', 1545825010),
(49, 'gallery', 'm000000_000000_gallery_base', 1545825012),
(50, 'gallery', 'm130427_120500_gallery_creation_user', 1545825013),
(51, 'gallery', 'm150416_074146_rename_fields', 1545825013),
(52, 'gallery', 'm160514_131314_add_preview_to_gallery', 1545825013),
(53, 'gallery', 'm160515_123559_add_category_to_gallery', 1545825014),
(54, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1545825014),
(55, 'gallery', 'm181224_072816_add_sort_to_gallery', 1545825014),
(56, 'news', 'm000000_000000_news_base', 1545825016),
(57, 'news', 'm150416_081251_rename_fields', 1545825016),
(58, 'news', 'm180224_105353_meta_title_column', 1545825016),
(59, 'news', 'm180421_142416_update_news_meta_column', 1545825016),
(60, 'page', 'm180421_143325_add_column_image', 1547437100),
(61, 'rbac', 'm140115_131455_auth_item', 1547444421),
(62, 'rbac', 'm140115_132045_auth_item_child', 1547444422),
(63, 'rbac', 'm140115_132319_auth_item_assign', 1547444423),
(64, 'rbac', 'm140702_230000_initial_role_data', 1547444423),
(65, 'page', 'm180421_143326_add_column_body_Short', 1574077121),
(66, 'page', 'm180421_143328_add_page_image_tbl', 1574077121),
(67, 'page', 'm180421_143329_add_page_title_page_h1', 1574077122),
(68, 'page', 'm180421_143330_directions_add_column_data', 1590064486),
(69, 'city', 'm000000_000000_city_base', 1605103140),
(70, 'page', 'm180421_143331_add_column_city_id', 1605155881),
(71, 'page', 'm180421_143332_add_index_city_id', 1605157900),
(72, 'review', 'm000000_000000_review_base', 1605188055),
(73, 'review', 'm000000_000001_review_add_column', 1605188059),
(74, 'review', 'm000000_000002_review_add_column_pos', 1605188061),
(75, 'review', 'm000000_000003_review_add_column_name_service', 1605188064),
(76, 'review', 'm000000_000004_review_add_column_rating', 1605188066),
(77, 'review', 'm000000_000005_review_rename_column', 1605188070),
(78, 'review', 'm000000_000006_add_table_images', 1605188073),
(79, 'review', 'm000000_000007_rename_table_images_name', 1605188073),
(80, 'review', 'm000000_000008_add_column_count', 1605188074),
(81, 'review', 'm000000_000009_add_column_city_id', 1605188075),
(82, 'review', 'm000000_000010_add_column_desc', 1605189912),
(83, 'review', 'm000000_000011_rename_column_audio', 1605190954),
(84, 'gallery', 'm181224_072817_add_city_parent_id', 1605330994),
(85, 'service', 'm000000_000000_service_base', 1605358346),
(86, 'city', 'm000000_000001_city_add_column', 1605531784),
(87, 'carbrands', 'm000000_000000_carbrands_base', 1605635558),
(88, 'review', 'm000000_000012_add_column_carbrands_id', 1606042047),
(89, 'carbrands', 'm000000_000001_add_column_back', 1606052439),
(90, 'service', 'm000000_000001_add_column_back', 1606054434),
(91, 'user', 'm200715_154204_add_super_user', 1606244547);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_news_news`
--

CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_notify_settings`
--

CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_page_page`
--

CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `body_short` text,
  `name_h1` varchar(255) DEFAULT NULL,
  `data` longtext,
  `city_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_page_page`
--

INSERT INTO `yupe_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `icon`, `body_short`, `name_h1`, `data`, `city_id`) VALUES
(1, NULL, 'ru', NULL, '2019-01-14 09:39:32', '2020-11-14 20:18:20', 1, 1, 'Главная', 'Главная', 'glavnaya', '<p>Изготовление ключей <br />для автомобиля</p>', '', '', 1, 0, 1, '', '', '', 'fbca308fcf3855396e7807039d9697f2.png', NULL, '', 'Изготовление ключей <br />для автомобиля', NULL, NULL),
(2, NULL, 'ru', NULL, '2019-01-14 09:57:27', '2020-11-14 20:18:20', 1, 1, '<span class=\"color-orange\">Услуги</span> и цены', 'Услуги и цены', 'services', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 6, '', 'view-services', '', NULL, NULL, '', '', NULL, NULL),
(3, NULL, 'ru', NULL, '2019-01-14 09:57:56', '2020-11-14 20:18:20', 1, 1, 'Прайс-лист', 'Прайс-лист', 'prays-list', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 3, '', '', '', NULL, NULL, '', '', NULL, NULL),
(4, NULL, 'ru', NULL, '2019-01-14 09:58:14', '2020-11-22 14:38:12', 1, 1, 'Марки авто', 'Марки авто', 'carbrands', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 3, '', 'view-carbrands', '', NULL, NULL, '', '', NULL, NULL),
(5, NULL, 'ru', NULL, '2019-01-14 09:58:29', '2020-11-14 20:18:20', 1, 1, 'О компании', 'О компании', 'o-kompanii', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 3, '', '', '', NULL, NULL, '', '', NULL, NULL),
(6, NULL, 'ru', NULL, '2019-01-14 09:58:44', '2020-11-22 12:30:20', 1, 1, '<span class=\"color-orange\">Отзывы</span> <br />наших клиентов', 'Отзывы клиентов', 'reviews', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 4, '', 'view-reviews', '', NULL, NULL, '<p>У нас более 100 отзывов от реальных клиентов! <br />Мы ценим ваше мнение и вы можете оставить свой отзыв <br />прямо на сайте. Не теряйте времени и деньги. <br />Обращайтесь к профессионалам своего дела!</p>', '', NULL, NULL),
(7, NULL, 'ru', NULL, '2019-01-14 09:58:58', '2020-11-17 23:29:08', 1, 1, 'Контакты', 'Контакты', 'contacts', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 5, '', 'view-contact', '', NULL, NULL, '', '', NULL, NULL),
(11, NULL, 'ru', 2, '2020-11-12 10:11:48', '2020-11-17 22:23:50', 1, 1, '<span class=\"color-orange\">Услуги</span> и цены', 'Услуги и цены', 'services', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 7, '', '', '', NULL, NULL, '', 'Услуги и цены в Оренбурге', NULL, 1),
(12, NULL, 'ru', 1, '2020-11-14 10:33:19', '2020-11-23 21:17:24', 1, 1, '', 'Главная в Оренбурге', 'glavnaya-v-orenburge', '<p>Соль-Илецке, Экодолье, Пруды, Чистый, Заречье, <br />Нежинка, Дедуровка, Покровка, Татарская <br />Каргала, Новосергиевка, Илек</p>', '', '', 1, 0, 2, '', '', '', NULL, NULL, '<p>, Соль-Илецке, Экодолье, Пруды, Чистый</p>', 'Изготовление ключей <br />для автомобиля <br /><span class=\"color-orange\">в Оренбурге</span>', NULL, 1),
(13, NULL, 'ru', 1, '2020-11-14 10:34:45', '2020-11-23 21:01:00', 1, 1, '', 'Главная в Санкт-Петербурге', 'glavnaya-v-sankt-peterburge', '<p>Соль-Илецке, Экодолье, Пруды, Чистый, Заречье, <br />Нежинка, Дедуровка, Покровка, Татарская <br />Каргала, Новосергиевка, Илек</p>', '', '', 1, 0, 2, '', '', '', '9e67e348e2aafeb162718cdc33e26217.png', NULL, '', 'Изготовление ключей <br />для автомобиля <br /><span class=\"color-orange\">в Санкт-Петербурге</span>', NULL, 2),
(14, NULL, 'ru', 2, '2020-11-14 20:17:09', '2020-11-17 22:23:45', 1, 1, '<span class=\"color-orange\">Услуги</span> и цены', 'Услуги и цены', 'services', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 9, '', '', '', NULL, NULL, '', 'Услуги и цены в Санкт-Петербурге', NULL, 2),
(15, NULL, 'ru', 6, '2020-11-14 22:30:32', '2020-11-14 22:50:44', 1, 1, '<span class=\"color-orange\">Отзывы</span> <br />наших клиентов', 'Отзывы клиентов', 'reviews', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 10, '', '', '', NULL, NULL, '<p>У нас более 100 отзывов от реальных клиентов! <br />Мы ценим ваше мнение и вы можете оставить свой отзыв <br />прямо на сайте. Не теряйте времени и деньги. <br />Обращайтесь к профессионалам своего дела!</p>', 'Отзывы клиентов в Оренбурге', NULL, 1),
(16, NULL, 'ru', 6, '2020-11-14 22:31:13', '2020-11-14 23:01:53', 1, 1, '<span class=\"color-orange\">Отзывы</span> <br />наших клиентов', 'Отзывы клиентов', 'reviews', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 11, '', '', '', NULL, NULL, '<p>У нас более 100 отзывов от реальных клиентов! <br />Мы ценим ваше мнение и вы можете оставить свой отзыв <br />прямо на сайте. Не теряйте времени и деньги. <br />Обращайтесь к профессионалам своего дела!</p>', 'Отзывы клиентов в Санкт-Петербурге', NULL, 2),
(17, NULL, 'ru', NULL, '2020-11-14 23:28:32', '2020-11-14 23:28:32', 1, 1, '<span class=\"color-orange\">Почему</span> мы?', 'Почему мы?', 'pochemu-my', '<p>Раздел в стадии наполнения!</p>', '', '', 1, 0, 12, '', '', '', NULL, NULL, '<p>Мы аккуратно и быстро откроем замок <br />без повреждений, сколов и дефектов</p>', '', NULL, NULL),
(18, NULL, 'ru', 17, '2020-11-14 23:30:33', '2020-11-14 23:31:38', 1, 1, 'Мы не используем подушки, открываем замки отмычками', 'Мы не используем подушки, открываем замки отмычками', 'my-ne-ispolzuem-podushki-otkryvaem-zamki-otmychkami', '<p>Раздел в стадии наполнения!</p>', '', '', 1, 0, 13, '', '', '', 'a7dfb2f7c379117871cfb9b857044b63.png', NULL, '<p>Дополнительное текстовое описание работы по вскрытию замков</p>', '', NULL, NULL),
(19, NULL, 'ru', 17, '2020-11-14 23:31:20', '2020-11-14 23:31:20', 1, 1, 'Не отгибаем, а открываем двери', 'Не отгибаем, а открываем двери', 'ne-otgibaem-a-otkryvaem-dveri', '<p>Раздел в стадии наполнения!</p>', '', '', 1, 0, 14, '', '', '', 'fff70abfc0684f8089e8dd3308464ef2.png', NULL, '<p>Мы аккуратно откроем любую дверь без повреждений, тестовый текст по вскрытию замков</p>', '', NULL, NULL),
(20, NULL, 'ru', 17, '2020-11-14 23:32:30', '2020-11-14 23:32:30', 1, 1, 'Приезжаем даже в удаленные районы', 'Приезжаем даже в удаленные районы', 'priezzhaem-dazhe-v-udalennye-rayony', '<p>Раздел в стадии наполнения!</p>', '', '', 1, 0, 15, '', '', '', '95adb387eaf4e4de6e69553625c5908c.png', NULL, '<p>Мы осуществляем выезды по области даже в удаленные районы, тестовый текст</p>', '', NULL, NULL),
(21, NULL, 'ru', NULL, '2020-11-15 00:14:40', '2020-11-15 00:14:40', 1, 1, 'Остались вопросы?', 'Остались вопросы?', 'ostalis-voprosy', '<p>Раздел в стадии наполнения!</p>', '', '', 1, 0, 16, '', '', '', NULL, NULL, '<p>Остались вопросы? <br />Задайте их нам! <br />Работаем круглосуточно</p>', '', NULL, NULL),
(22, NULL, 'ru', 4, '2020-11-17 23:33:19', '2020-11-18 00:07:06', 1, 1, 'Марки авто', 'Марки авто', 'marki-avto', '<p>Раздел в стадии наполнения</p>', '', '', 1, 0, 17, '', '', '', NULL, NULL, '', 'Марки авто в Оренбурге', NULL, 1),
(23, NULL, 'ru', 4, '2020-11-17 23:34:58', '2020-11-18 00:07:01', 1, 1, 'Марки авто', 'Марки авто', 'marki-avto', '<p>Раздел в стадии наполнения!</p>', '', '', 1, 0, 18, '', '', '', NULL, NULL, '', 'Марки авто в Санкт-Петербурге', NULL, 2),
(31, NULL, 'ru', 3, '2020-11-22 14:12:09', '2020-11-22 14:12:46', 1, 1, 'Прайс-лист', 'Прайс-лист', 'prays-list', '<table>\r\n<tbody>\r\n<tr>\r\n<td>Багажника, капота, бензобака</td>\r\n<td>от 780 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Дверей автомобиля</td>\r\n<td>от 920 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Блокиратора руля</td>\r\n<td>от 1000 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Замки коробки передач</td>\r\n<td>от 1480 р.</td>\r\n</tr>\r\n<tr>\r\n<td>Авто премиум-класса</td>\r\n<td>от 1800 р.</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', '', 1, 0, 26, '', '', '', NULL, NULL, '', 'Прайс-лист в Оренбурге', NULL, 1),
(32, NULL, 'ru', NULL, '2020-11-22 15:02:04', '2020-11-22 15:22:49', 1, 1, 'Политика  конфиденциальности', 'Политика  конфиденциальности', 'policy', '<p>Настоящая Политика конфиденциальности персональных данных (далее &ndash; Политика конфиденциальности) действует в отношении всей информации, которую сайт <strong>Изготовление автоключей</strong>, (далее &ndash; Сайт) расположенный на доменном имени <strong>otkroem.ru</strong> (а также его субдоменах), может получить о Пользователе во время использования сайта otkroem.ru (а также его субдоменов), его программ и его продуктов.</p>\r\n<h2>1. Определение терминов</h2>\r\n<p><strong>1.1 В настоящей Политике конфиденциальности используются следующие термины:</strong></p>\r\n<p>1.1.1. &laquo;<strong>Администрация сайта</strong>&raquo; (далее &ndash; Администрация) &ndash; уполномоченные сотрудники на управление сайтом <strong>Изготовление автоключей</strong>, действующие от имени ИП Черномырдин Алексей Владимирович, которые организуют и (или) осуществляют обработку персональных данных, а также определяет цели обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными.</p>\r\n<p>1.1.2. &laquo;Персональные данные&raquo; - любая информация, относящаяся к прямо или косвенно определенному, или определяемому физическому лицу (субъекту персональных данных).</p>\r\n<p>1.1.3. &laquo;Обработка персональных данных&raquo; - любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных.</p>\r\n<p>1.1.4. &laquo;Конфиденциальность персональных данных&raquo; - обязательное для соблюдения Оператором или иным получившим доступ к персональным данным лицом требование не допускать их распространения без согласия субъекта персональных данных или наличия иного законного основания.</p>\r\n<p>1.1.5. &laquo;Сайт <strong> Изготовление автоключей</strong>&raquo; - это совокупность связанных между собой веб-страниц, размещенных в сети Интернет по уникальному адресу (URL): <a href=\"/backend/page/page/update/otkroem.ru\"><strong>otkroem.ru</strong></a>, а также его субдоменах.</p>\r\n<p>1.1.6. &laquo;Субдомены&raquo; - это страницы или совокупность страниц, расположенные на доменах третьего уровня, принадлежащие сайту Изготовление автоключей, а также другие временные страницы, внизу который указана контактная информация Администрации</p>\r\n<p>1.1.5. &laquo;Пользователь сайта <strong>Изготовление автоключей</strong> &raquo; (далее Пользователь) &ndash; лицо, имеющее доступ к сайту <strong>Изготовление автоключей</strong>, посредством сети Интернет и использующее информацию, материалы и продукты сайта <strong>Изготовление автоключей</strong>.</p>\r\n<p>1.1.7. &laquo;Cookies&raquo; &mdash; небольшой фрагмент данных, отправленный веб-сервером и хранимый на компьютере пользователя, который веб-клиент или веб-браузер каждый раз пересылает веб-серверу в HTTP-запросе при попытке открыть страницу соответствующего сайта.</p>\r\n<p>1.1.8. &laquo;IP-адрес&raquo; &mdash; уникальный сетевой адрес узла в компьютерной сети, через который Пользователь получает доступ на Сайт.</p>\r\n<p>1.1.9. &laquo;Товар &raquo; - продукт, который Пользователь заказывает на сайте и оплачивает через платёжные системы.</p>\r\n<h2>2. Общие положения</h2>\r\n<p>2.1. Использование сайта Изготовление автоключей Пользователем означает согласие с настоящей Политикой конфиденциальности и условиями обработки персональных данных Пользователя.</p>\r\n<p>2.2. В случае несогласия с условиями Политики конфиденциальности Пользователь должен прекратить использование сайта Изготовление автоключей .</p>\r\n<p>2.3. Настоящая Политика конфиденциальности применяется к сайту Изготовление автоключей. Сайт не контролирует и не несет ответственность за сайты третьих лиц, на которые Пользователь может перейти по ссылкам, доступным на сайте Изготовление автоключей.</p>\r\n<p>2.4. Администрация не проверяет достоверность персональных данных, предоставляемых Пользователем.</p>\r\n<h2>3. Предмет политики конфиденциальности</h2>\r\n<p>3.1. Настоящая Политика конфиденциальности устанавливает обязательства Администрации по неразглашению и обеспечению режима защиты конфиденциальности персональных данных, которые Пользователь предоставляет по запросу Администрации при регистрации на сайте Изготовление автоключей, при подписке на информационную e-mail рассылку или при оформлении заказа.</p>\r\n<p>3.2. Персональные данные, разрешённые к обработке в рамках настоящей Политики конфиденциальности, предоставляются Пользователем путём заполнения форм на сайте Изготовление автоключей и включают в себя следующую информацию:<br />3.2.1. фамилию, имя, отчество Пользователя;<br />3.2.2. контактный телефон Пользователя;<br />3.2.3. адрес электронной почты (e-mail)<br />3.2.4. место жительство Пользователя (при необходимости)<br />3.2.5. адрес доставки Товара (при необходимости) <br />3.2.6. фотографию (при необходимости).</p>\r\n<p>3.3. Сайт защищает Данные, которые автоматически передаются при посещении страниц:<br />- IP адрес;<br />- информация из cookies;<br />- информация о браузере <br />- время доступа;<br />- реферер (адрес предыдущей страницы).</p>\r\n<p>3.3.1. Отключение cookies может повлечь невозможность доступа к частям сайта <!--?php echo Изготовление автоключей ?-->, требующим авторизации.</p>\r\n<p>3.3.2. Сайт осуществляет сбор статистики об IP-адресах своих посетителей. Данная информация используется с целью предотвращения, выявления и решения технических проблем.</p>\r\n<p>3.4. Любая иная персональная информация неоговоренная выше (история посещения, используемые браузеры, операционные системы и т.д.) подлежит надежному хранению и нераспространению, за исключением случаев, предусмотренных в п.п. 5.2. и 5.3. настоящей Политики конфиденциальности.</p>\r\n<h2>4. Цели сбора персональной информации пользователя</h2>\r\n<p>4.1. Персональные данные Пользователя Администрация может использовать в целях:<br />4.1.1. Идентификации Пользователя, зарегистрированного на сайте Изготовление автоключей для его дальнейшей авторизации, оформления заказа и других действий.<br />4.1.2. Предоставления Пользователю доступа к персонализированным данным сайта Изготовление автоключей.<br />4.1.3. Установления с Пользователем обратной связи, включая направление уведомлений, запросов, касающихся использования сайта Изготовление автоключей, оказания услуг и обработки запросов и заявок от Пользователя.<br />4.1.4. Определения места нахождения Пользователя для обеспечения безопасности, предотвращения мошенничества.<br />4.1.5. Подтверждения достоверности и полноты персональных данных, предоставленных Пользователем.<br />4.1.6. Создания учетной записи для использования частей сайта Изготовление автоключей, если Пользователь дал согласие на создание учетной записи.<br />4.1.7. Уведомления Пользователя по электронной почте.<br />4.1.8. Предоставления Пользователю эффективной технической поддержки при возникновении проблем, связанных с использованием сайта Изготовление автоключей.<br />4.1.9. Предоставления Пользователю с его согласия специальных предложений, информации о ценах, новостной рассылки и иных сведений от имени сайта Изготовление автоключей.<br />4.1.10. Осуществления рекламной деятельности с согласия Пользователя.</p>\r\n<h2>5. Способы и сроки обработки персональной информации</h2>\r\n<p>5.1. Обработка персональных данных Пользователя осуществляется без ограничения срока, любым законным способом, в том числе в информационных системах персональных данных с использованием средств автоматизации или без использования таких средств.</p>\r\n<p>5.2. Пользователь соглашается с тем, что Администрация вправе передавать персональные данные третьим лицам, в частности, курьерским службам, организациями почтовой связи (в том числе электронной), операторам электросвязи, исключительно в целях выполнения заказа Пользователя, оформленного на сайте Изготовление автоключей, включая доставку Товара, документации или e-mail сообщений.</p>\r\n<p>5.3. Персональные данные Пользователя могут быть переданы уполномоченным органам государственной власти Российской Федерации только по основаниям и в порядке, установленным законодательством Российской Федерации.</p>\r\n<p>5.4. При утрате или разглашении персональных данных Администрация вправе не информировать Пользователя об утрате или разглашении персональных данных.</p>\r\n<p>5.5. Администрация принимает необходимые организационные и технические меры для защиты персональной информации Пользователя от неправомерного или случайного доступа, уничтожения, изменения, блокирования, копирования, распространения, а также от иных неправомерных действий третьих лиц.</p>\r\n<p>5.6. Администрация совместно с Пользователем принимает все необходимые меры по предотвращению убытков или иных отрицательных последствий, вызванных утратой или разглашением персональных данных Пользователя.</p>\r\n<h2>6. Права и обязанности сторон</h2>\r\n<p><strong>6.1. Пользователь вправе:</strong></p>\r\n<p>6.1.1. Принимать свободное решение о предоставлении своих персональных данных, необходимых для использования сайта Изготовление автоключей, и давать согласие на их обработку.</p>\r\n<p>6.1.2. Обновить, дополнить предоставленную информацию о персональных данных в случае изменения данной информации.</p>\r\n<p>6.1.3. Пользователь имеет право на получение у Администрации информации, касающейся обработки его персональных данных, если такое право не ограничено в соответствии с федеральными законами. Пользователь вправе требовать от Администрации уточнения его персональных данных, их блокирования или уничтожения в случае, если персональные данные являются неполными, устаревшими, неточными, незаконно полученными или не являются необходимыми для заявленной цели обработки, а также принимать предусмотренные законом меры по защите своих прав.</p>\r\n<p><strong>6.2. Администрация обязана:</strong></p>\r\n<p>6.2.1. Использовать полученную информацию исключительно для целей, указанных в п. 4 настоящей Политики конфиденциальности.</p>\r\n<p>6.2.2. Обеспечить хранение конфиденциальной информации в тайне, не разглашать без предварительного письменного разрешения Пользователя, а также не осуществлять продажу, обмен, опубликование, либо разглашение иными возможными способами переданных персональных данных Пользователя, за исключением п.п. 5.2 и 5.3. настоящей Политики Конфиденциальности.</p>\r\n<p>6.2.3. Принимать меры предосторожности для защиты конфиденциальности персональных данных Пользователя согласно порядку, обычно используемого для защиты такого рода информации в существующем деловом обороте.</p>\r\n<p>6.2.4. Осуществить блокирование персональных данных, относящихся к соответствующему Пользователю, с момента обращения или запроса Пользователя, или его законного представителя либо уполномоченного органа по защите прав субъектов персональных данных на период проверки, в случае выявления недостоверных персональных данных или неправомерных действий.</p>\r\n<h2>7. Ответственность сторон</h2>\r\n<p>7.1. Администрация, не исполнившая свои обязательства, несёт ответственность за убытки, понесённые Пользователем в связи с неправомерным использованием персональных данных, в соответствии с законодательством Российской Федерации, за исключением случаев, предусмотренных п.п. 5.2., 5.3. и 7.2. настоящей Политики Конфиденциальности.</p>\r\n<p>7.2. В случае утраты или разглашения Конфиденциальной информации Администрация не несёт ответственность, если данная конфиденциальная информация:<br />7.2.1. Стала публичным достоянием до её утраты или разглашения.<br />7.2.2. Была получена от третьей стороны до момента её получения Администрацией Ресурса.<br />7.2.3. Была разглашена с согласия Пользователя.</p>\r\n<p>7.3. Пользователь несет полную ответственность за соблюдение требований законодательства РФ, в том числе законов о рекламе, о защите авторских и смежных прав, об охране товарных знаков и знаков обслуживания, но не ограничиваясь перечисленным, включая полную ответственность за содержание и форму материалов.</p>\r\n<p>7.4. Пользователь признает, что ответственность за любую информацию (в том числе, но не ограничиваясь: файлы с данными, тексты и т. д.), к которой он может иметь доступ как к части сайта Изготовление автоключей, несет лицо, предоставившее такую информацию.</p>\r\n<p>7.5. Пользователь соглашается, что информация, предоставленная ему как часть сайта Изготовление автоключей, может являться объектом интеллектуальной собственности, права на который защищены и принадлежат другим Пользователям, партнерам или рекламодателям, которые размещают такую информацию на сайте Изготовление автоключей. <br />Пользователь не вправе вносить изменения, передавать в аренду, передавать на условиях займа, продавать, распространять или создавать производные работы на основе такого Содержания (полностью или в части), за исключением случаев, когда такие действия были письменно прямо разрешены собственниками такого Содержания в соответствии с условиями отдельного соглашения.</p>\r\n<p>7.6. В отношение текстовых материалов (статей, публикаций, находящихся в свободном публичном доступе на сайте Изготовление автоключей) допускается их распространение при условии, что будет дана ссылка на Сайт.</p>\r\n<p>7.7. Администрация не несет ответственности перед Пользователем за любой убыток или ущерб, понесенный Пользователем в результате удаления, сбоя или невозможности сохранения какого-либо Содержания и иных коммуникационных данных, содержащихся на сайте Изготовление автоключей или передаваемых через него.</p>\r\n<p>7.8. Администрация не несет ответственности за любые прямые или косвенные убытки, произошедшие из-за: использования либо невозможности использования сайта, либо отдельных сервисов; несанкционированного доступа к коммуникациям Пользователя; заявления или поведение любого третьего лица на сайте.</p>\r\n<p>7.9. Администрация не несет ответственность за какую-либо информацию, размещенную пользователем на сайте Изготовление автоключей, включая, но не ограничиваясь: информацию, защищенную авторским правом, без прямого согласия владельца авторского права.</p>\r\n<h2>8. Разрешение споров</h2>\r\n<p>8.1. До обращения в суд с иском по спорам, возникающим из отношений между Пользователем и Администрацией, обязательным является предъявление претензии (письменного предложения или предложения в электронном виде о добровольном урегулировании спора).</p>\r\n<p>8.2. Получатель претензии в течение 30 календарных дней со дня получения претензии, письменно или в электронном виде уведомляет заявителя претензии о результатах рассмотрения претензии.</p>\r\n<p>8.3. При не достижении соглашения спор будет передан на рассмотрение Арбитражного суда г. Оренбург.</p>\r\n<p>8.4. К настоящей Политике конфиденциальности и отношениям между Пользователем и Администрацией применяется действующее законодательство Российской Федерации.</p>\r\n<h2>9. Дополнительные условия</h2>\r\n<p>9.1. Администрация вправе вносить изменения в настоящую Политику конфиденциальности без согласия Пользователя.</p>\r\n<p>9.2. Новая Политика конфиденциальности вступает в силу с момента ее размещения на сайте Изготовление автоключей, если иное не предусмотрено новой редакцией Политики конфиденциальности.</p>\r\n<p>9.3. Все предложения или вопросы касательно настоящей Политики конфиденциальности следует сообщать по адресу: <a href=\"mailto:otkroem56@mail.ru\">otkroem56@mail.ru</a></p>\r\n<p>9.4. Действующая Политика конфиденциальности размещена на странице по адресу <a href=\"/policy\">http://otkroem.ru/policy</a></p>\r\n<p>Обновлено: 22 Ноября 2020 года</p>\r\n<p>г. Оренбург, ИНН 561015569682 ОГРНИП 320565800027526</p>', '', '', 1, 0, 27, '', '', '', NULL, NULL, '', 'Политика конфиденциальности персональных данных', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_page_page_image`
--

CREATE TABLE `yupe_page_page_image` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `title` varchar(255) NOT NULL COMMENT 'Название изображения',
  `alt` varchar(255) NOT NULL COMMENT 'Alt изображения',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_page_page_image`
--

INSERT INTO `yupe_page_page_image` (`id`, `page_id`, `image`, `title`, `alt`, `position`) VALUES
(1, 12, '4e19f35d3600c5f920af6499a559a084.jpg', '', '', 3),
(2, 12, '47a5a2a3ed872c92ddc674672c360182.jpg', '', '', 1),
(3, 12, 'd28be708e6f7ebfc04b82acfc0ee61d4.jpg', '', '', 4),
(4, 12, 'b7fd627c58b970a2ae3d40b14eabcdec.jpg', '', '', 2),
(5, 13, 'f077ad54431b89b178c9b8b230640b33.jpg', '', '', 7),
(6, 13, '057ccf0f2bc921e27f20c3c5ab7ce4cf.jpg', '', '', 5),
(7, 13, 'b0c6b3b6e7356bdca4a3fde685a2e65a.jpg', '', '', 8),
(8, 13, 'be57eb97e5b6758e936d05fb5730da7a.jpg', '', '', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_review`
--

CREATE TABLE `yupe_review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `text` text NOT NULL,
  `moderation` int(11) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `useremail` varchar(256) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `product_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `countImage` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `short_desc` text,
  `audioreview` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `carbrands_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_review`
--

INSERT INTO `yupe_review` (`id`, `user_id`, `date_created`, `text`, `moderation`, `username`, `image`, `useremail`, `category_id`, `position`, `product_id`, `rating`, `countImage`, `city_id`, `short_desc`, `audioreview`, `video`, `code`, `carbrands_id`) VALUES
(1, 1, '2020-11-12 16:45:01', 'Здравствуйте. Хочу оставить отзыв о работе компании “Откроем 56”. \r\nРебята приехали моментально, буквально успел положить телефонную трубку. Выполнили работу качественно, нареканий никаких нет. Надеюсь больше не попадать в ситуацию с забытыми ключами внутри автомобиля, но если вдруг что-то подобное случится, то обязательно буду обращаться снова к Вам.', 1, 'Иван', '309fde2a3d3c1e0c4057446bf2ec8721.png', NULL, NULL, 1, NULL, NULL, 0, 1, 'Открыли автомобиль за 27 минут', 'fc0dc15808d088d95304738871f7a728.mp3', NULL, NULL, 6),
(2, 1, '2020-11-12 17:37:33', 'Здравствуйте. Хочу оставить отзыв о работе компании “Откроем 56”. \r\nРебята приехали моментально, буквально успел положить телефонную трубку. Выполнили работу качественно, нареканий никаких нет. Надеюсь больше не попадать в ситуацию с забытыми ключами внутри автомобиля, но если вдруг что-то подобное случится, то обязательно буду обращаться снова к Вам.', 1, 'Дмитрий', '70a577e096b81250311464a2d3ff8b5a.png', NULL, NULL, 2, NULL, NULL, 0, 1, 'Открыли автомобиль за 27 минут', '48dd2ccaabb7ba1e9f206dd9de5f1b40.mp3', NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_review_image`
--

CREATE TABLE `yupe_review_image` (
  `id` int(11) NOT NULL,
  `review_id` int(11) DEFAULT NULL COMMENT 'Id отзыва',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `alt` varchar(255) DEFAULT NULL COMMENT 'Alt',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_service`
--

CREATE TABLE `yupe_service` (
  `id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'Родитель',
  `page_id` int(11) DEFAULT NULL COMMENT 'Страница',
  `city_id` int(11) DEFAULT NULL COMMENT 'Город',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое название',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `slug` varchar(255) DEFAULT NULL COMMENT 'Alias',
  `name_h1` varchar(255) DEFAULT NULL COMMENT 'Заголовок на странице',
  `price` varchar(255) DEFAULT NULL COMMENT 'Цена',
  `price_prefix` varchar(255) DEFAULT 'От' COMMENT 'Префикс для цены',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description_short` text COMMENT 'Короткое описание',
  `description` text COMMENT 'Описание',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Title (SEO)',
  `meta_keywords` text COMMENT 'Ключевые слова SEO',
  `meta_description` text COMMENT 'Описание SEO',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `back` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_service`
--

INSERT INTO `yupe_service` (`id`, `create_user_id`, `update_user_id`, `create_time`, `update_time`, `parent_id`, `page_id`, `city_id`, `name_short`, `name`, `slug`, `name_h1`, `price`, `price_prefix`, `image`, `description_short`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `position`, `back`) VALUES
(1, 1, 1, '2020-11-14 20:21:28', '2020-11-15 18:01:28', NULL, 11, 1, 'Вскрытие <br>автомобилей', 'Вскрытие автомобилей', 'vskrytie-avtomobiley', '', '1 499', 'От', 'cdacf5fa235257a91055c3a14e553708.png', '<p>Иномарки и отечественные автомобили. От Лада и ЗАЗ до BMW и Bentley</p>', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 1, NULL),
(2, 1, 1, '2020-11-14 20:23:40', '2020-11-15 18:01:34', NULL, 11, 1, 'Изготовление <br>автомобильных <br>ключей', 'Изготовление автомобильных ключей', 'izgotovlenie-avtomobilnyh-klyuchey', '', '1 499', 'От', 'eec4ef1c029937a58ae19610002ccf2a.png', '<p>Иномарки и отечественные автомобили. От Лада и ЗАЗ до BMW и Bentley</p>', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 2, NULL),
(3, 1, 1, '2020-11-14 20:24:45', '2020-11-15 18:01:57', NULL, 11, 1, 'Восстановление <br>ключей при полной <br>утере', 'Восстановление ключей при полной утере', 'vosstanovlenie-klyuchey-pri-polnoy-utere', '', '1 499', 'От', 'fe662c64cea8a013a0193b5b8d1c42ed.png', '<p>Иномарки и отечественные автомобили. От Лада и ЗАЗ до BMW и Bentley</p>', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 3, NULL),
(4, 1, 1, '2020-11-14 22:10:20', '2020-11-15 18:02:19', NULL, 11, 1, 'Изготовление <br>дубликата ключа', 'Изготовление дубликата ключа', 'izgotovlenie-dublikata-klyucha', '', '499', 'От', 'f2880d8860248372730a8dba7d766491.png', '<p>Иномарки и отечественные автомобили. От Лада и ЗАЗ до BMW и Bentley</p>', '<p>Раздел в стадии наполения!</p>', '', '', '', 1, 4, NULL),
(5, 1, 1, '2020-11-15 12:01:13', '2020-11-15 18:03:09', NULL, 11, 1, 'Выезд <br>в удаленные районы', 'Выезд в удаленные районы', 'vyezd-v-udalennye-rayony', '', '1 999', 'От', '4a01d3aa6960f352960c05b68799eb25.png', '<p>Иномарки и отечественные автомобили. От Лада и ЗАЗ до BMW и Bentley</p>', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 5, NULL),
(6, 1, 1, '2020-11-15 12:02:08', '2020-11-15 18:03:51', NULL, 11, 1, 'Чип <br>для автозапуска', 'Чип для автозапуска', 'chip-dlya-avtozapuska', '', '2 499', 'От', '9d8e93e5bbb8e7b958249bcc251b41d6.png', '<p>Иномарки и отечественные автомобили. От Лада и ЗАЗ до BMW и Bentley</p>', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 6, NULL),
(7, 1, 1, '2020-11-15 12:03:01', '2020-11-15 18:04:23', NULL, 11, 1, 'Замена/восстановление <br>брелков сигнализаций', 'Замена/восстановление брелков сигнализаций', 'zamenavosstanovlenie-brelkov-signalizaciy', '', '1 499', 'От', 'f713b0fd1fa80a88608f90debf29d7db.png', '<p>Иномарки и отечественные автомобили. От Лада и ЗАЗ до BMW и Bentley</p>', '<p>Раздел в стадии наполнения!</p>', '', '', '', 1, 7, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_tokens`
--

CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_tokens`
--

INSERT INTO `yupe_user_tokens` (`id`, `user_id`, `token`, `type`, `status`, `create_time`, `update_time`, `ip`, `expire_time`) VALUES
(19, 1, 'qLGLx55K97LQA66nvo8x182oEdxc~1g1', 4, 0, '2020-11-25 09:22:04', '2020-11-25 09:22:04', '145.255.22.83', '2020-12-02 09:22:04'),
(20, 2, 'IR9JFrCJvumQBX5Zi9sojKR0TYzV9v54', 4, 0, '2020-11-25 09:22:41', '2020-11-25 09:22:41', '145.255.22.83', '2020-12-02 09:22:41');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user`
--

CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '50443789b000cb912ae2c026e7e117600.90330600 1545824979',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  `super_user` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user`
--

INSERT INTO `yupe_user_user` (`id`, `update_time`, `first_name`, `middle_name`, `last_name`, `nick_name`, `email`, `gender`, `birth_date`, `site`, `about`, `location`, `status`, `access_level`, `visit_time`, `create_time`, `avatar`, `hash`, `email_confirm`, `phone`, `super_user`) VALUES
(1, '2018-12-26 17:50:44', '', '', '', 'admin', 'nariman-abenov@mail.ru', 0, NULL, '', '', '', 1, 1, '2020-11-25 09:22:04', '2018-12-26 17:50:44', NULL, '$2y$13$n8zKhyL8z0DR9yUE1HmNlu.QUR6GwqIuebNFXyeHzb2pBqF3DcNci', 1, NULL, 1),
(2, '2020-11-25 00:05:30', '', '', '', 'administrator', 'administrator@otkroem.ru', 2, NULL, '', '', '', 1, 1, '2020-11-25 09:22:41', '2020-11-25 00:04:52', NULL, '$2y$13$ihIxNTmsVsPc1xpjnMcfluhn8349/D6w5hDn.GCcrLRAHsO.NYYQG', 1, '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_assignment`
--

CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user_auth_assignment`
--

INSERT INTO `yupe_user_user_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 1, NULL, NULL),
('admin', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_item`
--

CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user_auth_item`
--

INSERT INTO `yupe_user_user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_item_child`
--

CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_yupe_settings`
--

CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_yupe_settings`
--

INSERT INTO `yupe_yupe_settings` (`id`, `module_id`, `param_name`, `param_value`, `create_time`, `update_time`, `user_id`, `type`) VALUES
(1, 'yupe', 'siteDescription', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', 1, 1),
(2, 'yupe', 'siteName', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', 1, 1),
(3, 'yupe', 'siteKeyWords', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', 1, 1),
(4, 'yupe', 'email', 'admin@otkroem.ru', '2018-12-26 17:51:10', '2020-11-22 15:51:41', 1, 1),
(5, 'yupe', 'theme', 'default', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(6, 'yupe', 'backendTheme', '', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(9, 'homepage', 'mode', '2', '2019-01-14 09:39:38', '2019-01-14 09:39:38', 1, 1),
(10, 'homepage', 'target', '1', '2019-01-14 09:39:38', '2019-01-14 09:39:41', 1, 1),
(11, 'homepage', 'limit', '', '2019-01-14 09:39:38', '2019-01-14 09:39:38', 1, 1),
(12, 'page', 'editor', 'tinymce5', '2020-05-22 09:50:05', '2020-08-26 11:23:26', 1, 1),
(13, 'page', 'mainCategory', '', '2020-05-22 09:50:05', '2020-05-22 09:50:05', 1, 1),
(14, 'yupe', 'coreCacheTime', '3600', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(15, 'yupe', 'uploadPath', 'uploads', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(16, 'yupe', 'editor', 'tinymce5', '2020-08-26 10:12:15', '2020-11-11 20:18:47', 1, 1),
(17, 'yupe', 'availableLanguages', 'ru,uk,en,zh', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(18, 'yupe', 'allowedIp', '', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(19, 'yupe', 'hidePanelUrls', '0', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(20, 'yupe', 'logo', 'images/logo.png', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(21, 'yupe', 'allowedExtensions', 'gif,jpeg,png,jpg,zip,rar,doc,docx,xls,xlsx,pdf,svg,mp3', '2020-08-26 10:12:15', '2020-11-14 12:26:54', 1, 1),
(22, 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, image/svg+xml, application/xhtml+xml, application/xml,audio/mpeg', '2020-08-26 10:12:15', '2020-11-14 12:26:54', 1, 1),
(23, 'yupe', 'maxSize', '5242880', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(24, 'yupe', 'defaultImage', '/images/nophoto.jpg', '2020-08-26 10:12:15', '2020-08-26 10:12:15', 1, 1),
(25, 'user', 'avatarMaxSize', '5242880', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(26, 'user', 'avatarExtensions', 'jpg,png,gif,jpeg', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(27, 'user', 'defaultAvatarPath', 'images/avatar.png', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(28, 'user', 'avatarsDir', 'avatars', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(29, 'user', 'showCaptcha', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(30, 'user', 'minCaptchaLength', '3', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(31, 'user', 'maxCaptchaLength', '6', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(32, 'user', 'minPasswordLength', '8', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(33, 'user', 'autoRecoveryPassword', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(34, 'user', 'recoveryDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(35, 'user', 'registrationDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(36, 'user', 'notifyEmailFrom', 'no-reply@dcmr.ru', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(37, 'user', 'logoutSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(38, 'user', 'loginSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(39, 'user', 'accountActivationSuccess', '/user/account/login', '2020-08-26 10:13:59', '2020-08-26 10:13:59', 1, 1),
(40, 'user', 'accountActivationFailure', '/user/account/registration', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(41, 'user', 'loginAdminSuccess', '/yupe/backend/index', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(42, 'user', 'registrationSuccess', '/user/account/login', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(43, 'user', 'sessionLifeTime', '7', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(44, 'user', 'usersPerPage', '20', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(45, 'user', 'emailAccountVerification', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(46, 'user', 'badLoginCount', '3', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(47, 'user', 'phoneMask', '+7(999) 999-99-99', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(48, 'user', 'phonePattern', '/\\+7\\(\\d{3}\\) \\d{3}\\-\\d{2}\\-\\d{2}$/', '2020-08-26 10:14:00', '2020-08-26 10:14:00', 1, 1),
(49, 'user', 'generateNickName', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:10', 1, 1),
(50, 'image', 'uploadPath', 'image', '2020-11-14 11:12:29', '2020-11-14 11:12:29', 1, 1),
(51, 'image', 'allowedExtensions', 'jpg,jpeg,png,gif,svg', '2020-11-14 11:12:29', '2020-11-14 12:16:00', 1, 1),
(52, 'image', 'minSize', '0', '2020-11-14 11:12:29', '2020-11-14 11:12:29', 1, 1),
(53, 'image', 'maxSize', '5242880', '2020-11-14 11:12:29', '2020-11-14 11:12:29', 1, 1),
(54, 'image', 'mainCategory', '', '2020-11-14 11:12:29', '2020-11-14 11:12:29', 1, 1),
(55, 'image', 'mimeTypes', 'image/gif, image/jpeg, image/png, image/svg+xml', '2020-11-14 11:12:29', '2020-11-14 13:46:06', 1, 1),
(56, 'image', 'width', '1950', '2020-11-14 11:12:29', '2020-11-14 11:45:34', 1, 1),
(57, 'image', 'height', '1950', '2020-11-14 11:12:29', '2020-11-14 11:45:34', 1, 1),
(58, 'city', 'defaultCity', 'orenburg', '2020-11-14 15:28:10', '2020-11-14 15:28:10', 1, 1),
(59, 'carbrands', 'titleHome', 'Марки авто', '2020-11-21 22:17:06', '2020-11-21 22:17:06', 1, 1),
(60, 'yupe', 'notifyEmailFrom', 'no-reply@otkroem.ru', '2020-11-22 15:51:42', '2020-11-22 15:51:42', 1, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `yupe_blog_blog`
--
ALTER TABLE `yupe_blog_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  ADD KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  ADD KEY `ix_yupe_blog_blog_status` (`status`),
  ADD KEY `ix_yupe_blog_blog_type` (`type`),
  ADD KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  ADD KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  ADD KEY `ix_yupe_blog_blog_lang` (`lang`),
  ADD KEY `ix_yupe_blog_blog_slug` (`slug`),
  ADD KEY `ix_yupe_blog_blog_category_id` (`category_id`);

--
-- Индексы таблицы `yupe_blog_post`
--
ALTER TABLE `yupe_blog_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  ADD KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  ADD KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  ADD KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  ADD KEY `ix_yupe_blog_post_status` (`status`),
  ADD KEY `ix_yupe_blog_post_access_type` (`access_type`),
  ADD KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  ADD KEY `ix_yupe_blog_post_lang` (`lang`),
  ADD KEY `ix_yupe_blog_post_slug` (`slug`),
  ADD KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  ADD KEY `ix_yupe_blog_post_category_id` (`category_id`);

--
-- Индексы таблицы `yupe_blog_post_to_tag`
--
ALTER TABLE `yupe_blog_post_to_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  ADD KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`);

--
-- Индексы таблицы `yupe_blog_tag`
--
ALTER TABLE `yupe_blog_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`);

--
-- Индексы таблицы `yupe_blog_user_to_blog`
--
ALTER TABLE `yupe_blog_user_to_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`);

--
-- Индексы таблицы `yupe_carbrands`
--
ALTER TABLE `yupe_carbrands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_carbrands_slug_city` (`slug`,`city_id`),
  ADD KEY `fk_yupe_carbrands_city_id` (`city_id`),
  ADD KEY `fk_yupe_carbrands_page_id` (`page_id`);

--
-- Индексы таблицы `yupe_category_category`
--
ALTER TABLE `yupe_category_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  ADD KEY `ix_yupe_category_category_status` (`status`);

--
-- Индексы таблицы `yupe_city`
--
ALTER TABLE `yupe_city`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `yupe_comment_comment`
--
ALTER TABLE `yupe_comment_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_comment_comment_status` (`status`),
  ADD KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  ADD KEY `ix_yupe_comment_comment_model` (`model`),
  ADD KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  ADD KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  ADD KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  ADD KEY `ix_yupe_comment_comment_level` (`level`),
  ADD KEY `ix_yupe_comment_comment_root` (`root`),
  ADD KEY `ix_yupe_comment_comment_lft` (`lft`),
  ADD KEY `ix_yupe_comment_comment_rgt` (`rgt`);

--
-- Индексы таблицы `yupe_contentblock_content_block`
--
ALTER TABLE `yupe_contentblock_content_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  ADD KEY `ix_yupe_contentblock_content_block_type` (`type`),
  ADD KEY `ix_yupe_contentblock_content_block_status` (`status`);

--
-- Индексы таблицы `yupe_gallery_gallery`
--
ALTER TABLE `yupe_gallery_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_gallery_gallery_status` (`status`),
  ADD KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  ADD KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  ADD KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`),
  ADD KEY `ix_yupe_gallery_gallery_sort` (`sort`),
  ADD KEY `fk_yupe_gallery_gallery_gallery_city_id` (`city_id`);

--
-- Индексы таблицы `yupe_gallery_image_to_gallery`
--
ALTER TABLE `yupe_gallery_image_to_gallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  ADD KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  ADD KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`);

--
-- Индексы таблицы `yupe_image_image`
--
ALTER TABLE `yupe_image_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_image_image_status` (`status`),
  ADD KEY `ix_yupe_image_image_user` (`user_id`),
  ADD KEY `ix_yupe_image_image_type` (`type`),
  ADD KEY `ix_yupe_image_image_category_id` (`category_id`),
  ADD KEY `fk_yupe_image_image_parent_id` (`parent_id`);

--
-- Индексы таблицы `yupe_mail_mail_event`
--
ALTER TABLE `yupe_mail_mail_event`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`);

--
-- Индексы таблицы `yupe_mail_mail_template`
--
ALTER TABLE `yupe_mail_mail_template`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  ADD KEY `ix_yupe_mail_mail_template_status` (`status`),
  ADD KEY `ix_yupe_mail_mail_template_event_id` (`event_id`);

--
-- Индексы таблицы `yupe_menu_menu`
--
ALTER TABLE `yupe_menu_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  ADD KEY `ix_yupe_menu_menu_status` (`status`);

--
-- Индексы таблицы `yupe_menu_menu_item`
--
ALTER TABLE `yupe_menu_menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  ADD KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  ADD KEY `ix_yupe_menu_menu_item_status` (`status`);

--
-- Индексы таблицы `yupe_migrations`
--
ALTER TABLE `yupe_migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_migrations_module` (`module`);

--
-- Индексы таблицы `yupe_news_news`
--
ALTER TABLE `yupe_news_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_yupe_news_news_status` (`status`),
  ADD KEY `ix_yupe_news_news_user_id` (`user_id`),
  ADD KEY `ix_yupe_news_news_category_id` (`category_id`),
  ADD KEY `ix_yupe_news_news_date` (`date`);

--
-- Индексы таблицы `yupe_notify_settings`
--
ALTER TABLE `yupe_notify_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_notify_settings_user_id` (`user_id`);

--
-- Индексы таблицы `yupe_page_page`
--
ALTER TABLE `yupe_page_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_page_page_slug_lang_city` (`slug`,`lang`,`city_id`),
  ADD KEY `ix_yupe_page_page_status` (`status`),
  ADD KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  ADD KEY `ix_yupe_page_page_user_id` (`user_id`),
  ADD KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  ADD KEY `ix_yupe_page_page_menu_order` (`order`),
  ADD KEY `ix_yupe_page_page_category_id` (`category_id`),
  ADD KEY `fk_yupe_page_page_city_id` (`city_id`);

--
-- Индексы таблицы `yupe_page_page_image`
--
ALTER TABLE `yupe_page_page_image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `yupe_review`
--
ALTER TABLE `yupe_review`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `yupe_review_image`
--
ALTER TABLE `yupe_review_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_yupe_review_image_review_id` (`review_id`);

--
-- Индексы таблицы `yupe_service`
--
ALTER TABLE `yupe_service`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_service_slug_city` (`slug`,`city_id`),
  ADD KEY `fk_yupe_service_city_id` (`city_id`),
  ADD KEY `fk_yupe_service_page_id` (`page_id`);

--
-- Индексы таблицы `yupe_user_tokens`
--
ALTER TABLE `yupe_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_user_tokens_user_id` (`user_id`);

--
-- Индексы таблицы `yupe_user_user`
--
ALTER TABLE `yupe_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  ADD UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  ADD KEY `ix_yupe_user_user_status` (`status`);

--
-- Индексы таблицы `yupe_user_user_auth_assignment`
--
ALTER TABLE `yupe_user_user_auth_assignment`
  ADD PRIMARY KEY (`itemname`,`userid`),
  ADD KEY `fk_yupe_user_user_auth_assignment_user` (`userid`);

--
-- Индексы таблицы `yupe_user_user_auth_item`
--
ALTER TABLE `yupe_user_user_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `ix_yupe_user_user_auth_item_type` (`type`);

--
-- Индексы таблицы `yupe_user_user_auth_item_child`
--
ALTER TABLE `yupe_user_user_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `fk_yupe_user_user_auth_item_child_child` (`child`);

--
-- Индексы таблицы `yupe_yupe_settings`
--
ALTER TABLE `yupe_yupe_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  ADD KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  ADD KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  ADD KEY `fk_yupe_yupe_settings_user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `yupe_blog_blog`
--
ALTER TABLE `yupe_blog_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_blog_post`
--
ALTER TABLE `yupe_blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_blog_tag`
--
ALTER TABLE `yupe_blog_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_blog_user_to_blog`
--
ALTER TABLE `yupe_blog_user_to_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_carbrands`
--
ALTER TABLE `yupe_carbrands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `yupe_category_category`
--
ALTER TABLE `yupe_category_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_city`
--
ALTER TABLE `yupe_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `yupe_comment_comment`
--
ALTER TABLE `yupe_comment_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_contentblock_content_block`
--
ALTER TABLE `yupe_contentblock_content_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `yupe_gallery_gallery`
--
ALTER TABLE `yupe_gallery_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `yupe_gallery_image_to_gallery`
--
ALTER TABLE `yupe_gallery_image_to_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT для таблицы `yupe_image_image`
--
ALTER TABLE `yupe_image_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT для таблицы `yupe_mail_mail_event`
--
ALTER TABLE `yupe_mail_mail_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_mail_mail_template`
--
ALTER TABLE `yupe_mail_mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_menu_menu`
--
ALTER TABLE `yupe_menu_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `yupe_menu_menu_item`
--
ALTER TABLE `yupe_menu_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `yupe_migrations`
--
ALTER TABLE `yupe_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT для таблицы `yupe_news_news`
--
ALTER TABLE `yupe_news_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_notify_settings`
--
ALTER TABLE `yupe_notify_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_page_page`
--
ALTER TABLE `yupe_page_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `yupe_page_page_image`
--
ALTER TABLE `yupe_page_page_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `yupe_review`
--
ALTER TABLE `yupe_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `yupe_review_image`
--
ALTER TABLE `yupe_review_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_service`
--
ALTER TABLE `yupe_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `yupe_user_tokens`
--
ALTER TABLE `yupe_user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `yupe_user_user`
--
ALTER TABLE `yupe_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `yupe_yupe_settings`
--
ALTER TABLE `yupe_yupe_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `yupe_blog_blog`
--
ALTER TABLE `yupe_blog_blog`
  ADD CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_blog_post`
--
ALTER TABLE `yupe_blog_post`
  ADD CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_blog_post_to_tag`
--
ALTER TABLE `yupe_blog_post_to_tag`
  ADD CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_blog_user_to_blog`
--
ALTER TABLE `yupe_blog_user_to_blog`
  ADD CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_carbrands`
--
ALTER TABLE `yupe_carbrands`
  ADD CONSTRAINT `fk_yupe_carbrands_city_id` FOREIGN KEY (`city_id`) REFERENCES `yupe_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_carbrands_page_id` FOREIGN KEY (`page_id`) REFERENCES `yupe_page_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_category_category`
--
ALTER TABLE `yupe_category_category`
  ADD CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_comment_comment`
--
ALTER TABLE `yupe_comment_comment`
  ADD CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_gallery_gallery`
--
ALTER TABLE `yupe_gallery_gallery`
  ADD CONSTRAINT `fk_yupe_gallery_gallery_gallery_city_id` FOREIGN KEY (`city_id`) REFERENCES `yupe_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_gallery_image_to_gallery`
--
ALTER TABLE `yupe_gallery_image_to_gallery`
  ADD CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_image_image`
--
ALTER TABLE `yupe_image_image`
  ADD CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_mail_mail_template`
--
ALTER TABLE `yupe_mail_mail_template`
  ADD CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_menu_menu_item`
--
ALTER TABLE `yupe_menu_menu_item`
  ADD CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_news_news`
--
ALTER TABLE `yupe_news_news`
  ADD CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_notify_settings`
--
ALTER TABLE `yupe_notify_settings`
  ADD CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_page_page`
--
ALTER TABLE `yupe_page_page`
  ADD CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_page_page_city_id` FOREIGN KEY (`city_id`) REFERENCES `yupe_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_review_image`
--
ALTER TABLE `yupe_review_image`
  ADD CONSTRAINT `fk_yupe_review_image_review_id` FOREIGN KEY (`review_id`) REFERENCES `yupe_review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_service`
--
ALTER TABLE `yupe_service`
  ADD CONSTRAINT `fk_yupe_service_city_id` FOREIGN KEY (`city_id`) REFERENCES `yupe_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_service_page_id` FOREIGN KEY (`page_id`) REFERENCES `yupe_page_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_user_tokens`
--
ALTER TABLE `yupe_user_tokens`
  ADD CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_user_user_auth_assignment`
--
ALTER TABLE `yupe_user_user_auth_assignment`
  ADD CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_user_user_auth_item_child`
--
ALTER TABLE `yupe_user_user_auth_item_child`
  ADD CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_yupe_settings`
--
ALTER TABLE `yupe_yupe_settings`
  ADD CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
