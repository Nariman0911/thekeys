<?php
/**
 * Directions install migration
 * Класс миграций для модуля Directions:
 *
 **/
class m180421_143331_add_column_city_id extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'city_id', 'integer');

        $this->addForeignKey(
            "fk_{{page_page}}_city_id",
            '{{page_page}}',
            'city_id',
            '{{city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropColumn('{{page_page}}', 'city_id');
    }
}
