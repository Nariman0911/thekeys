<?php
/**
 * Directions install migration
 * Класс миграций для модуля Directions:
 *
 **/
class m180421_143332_add_index_city_id extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->dropIndex("ux_{{page_page}}_slug_lang", '{{page_page}}');
        $this->createIndex("ux_{{page_page}}_slug_lang_city", '{{page_page}}', "slug,lang,city_id", true);
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
    }
}
