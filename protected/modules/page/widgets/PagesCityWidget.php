<?php
/**
 * PagesCityWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesCityWidget
 */
class PagesCityWidget extends yupe\widgets\YWidget
{
    public $page_id;
    public $view = 'pages-widget';

    protected $city = false;
    protected $page;
    protected $citypage;

    public function init()
    {
        /*$citySlug = Yii::app()->controller->citySlug;
        $city = City::model()->published()->findByAttributes(['slug' => $citySlug]);*/
        $city = Yii::app()->controller->geoCity;
        if($city){
            $this->city = $city;
        }

        if($this->page_id){
            $this->page = Page::model()->published()->findByPk($this->page_id);

            if($this->page){
                $childPage = [];
                foreach ($this->page->getChildCityPage() as $key => $item) {
                    $childPage = $item;
                }

                $this->citypage = ($childPage) ?: $this->page;
            }
        }

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, [
            'page' => $this->citypage,
            'city' => $this->city
        ]);
    }
}
