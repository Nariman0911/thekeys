<?php

/**
 * Class PageUrlRule
 */
class PageUrlRule extends CBaseUrlRule
{
    const CACHE_KEY = 'page::slugs';

    /**
     * @param CUrlManager $manager
     * @param string $route
     * @param array $params
     * @param string $amp
     * @return bool
     */
    public function createUrl($manager, $route, $params, $amp)
    {
        /*if ($route === 'page/page/view' && isset($params['slug'])) {
            return $params['slug'];
        } */
        if ($route === 'page/page/view' && isset($params['slug'])) {
            $url = $params['slug'];
            unset($params['slug']);
            $query = http_build_query($params);
            if (!empty($params) && $query !== ''){
                $url .= '?' . $query;
            }
            return $url;
        }

        return false;
    }

    /**
     * @param \yupe\components\urlManager\LangUrlManager $manager
     * @param CHttpRequest $request
     * @param string $pathInfo
     * @param string $rawPathInfo
     * @return bool|string
     */
    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        // $slugs = Yii::app()->getCache()->get(self::CACHE_KEY);
        // if (false === $slugs) {
            $city = City::model()->published()->findByAttributes(['slug' => Yii::app()->controller->citySlug]);
            $criteria = new CDbCriteria();
            if($city){
                $criteria->condition = 'city_id = :city_id OR (city_id IS NULL)';
                $criteria->params = [
                    ':city_id'=>$city->id
                ];
            }
            $models = Page::model()->published()->findAll($criteria);
            $slugs = CHtml::listData($models, 'id', 'slug');
            // Yii::app()->getCache()->set(self::CACHE_KEY, $slugs, 0);
        // }

        $slug = $manager->removeLangFromUrl($pathInfo);

        if (in_array('/'.$slug, $slugs, true)) {

            return 'page/page/view/slug/' . urlencode($slug);
        }

        return false;
    }
}