<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ServiceModule.service', 'Услуги') => ['/service/serviceBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('ServiceModule.service', 'Услуги - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ServiceModule.service', 'Управление Услугами'), 'url' => ['/service/serviceBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ServiceModule.service', 'Добавить Услугу'), 'url' => ['/service/serviceBackend/create']],
    ['label' => Yii::t('ServiceModule.service', 'Услуга') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('ServiceModule.service', 'Редактирование Услуги'), 'url' => [
        '/service/serviceBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('ServiceModule.service', 'Просмотреть Услугу'), 'url' => [
        '/service/serviceBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('ServiceModule.service', 'Удалить Услугу'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/service/serviceBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('ServiceModule.service', 'Вы уверены, что хотите удалить Услугу?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ServiceModule.service', 'Просмотр') . ' ' . Yii::t('ServiceModule.service', 'Услуги'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_user_id',
        'update_user_id',
        'create_time',
        'update_time',
        'parent_id',
        'page_id',
        'city_id',
        'name_short',
        'name',
        'slug',
        'name_h1',
        'price',
        'price_prefix',
        'image',
        'description_short',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'status',
        'position',
    ],
]); ?>
