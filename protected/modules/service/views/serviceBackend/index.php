<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ServiceModule.service', 'Услуги') => ['/service/serviceBackend/index'],
    Yii::t('ServiceModule.service', 'Управление'),
];

$this->pageTitle = Yii::t('ServiceModule.service', 'Услуги - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ServiceModule.service', 'Управление Услугами'), 'url' => ['/service/serviceBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ServiceModule.service', 'Добавить Услугу'), 'url' => ['/service/serviceBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ServiceModule.service', 'Услуги'); ?>
        <small><?=  Yii::t('ServiceModule.service', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('ServiceModule.service', 'Поиск Услуг');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('service-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('ServiceModule.service', 'В данном разделе представлены средства управления Услугами'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'service-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/service/serviceBackend/sortable',
        'type'         => 'condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            [
                'type' => 'raw',
                'name'   => 'page_id',
                'value'  => function($data){
                    return '<span class="label label-primary">'.strip_tags($data->page->getTitle()).'</span>';
                },
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'page_id',
                    Page::model()->getFormattedList(2),
                    ['encode' => false, 'empty' => '', 'class' => 'form-control']
                )
            ],
            [
                'type' => 'raw',
                'name'    => 'city_id',
                'value'  => function($data){
                    return '<span class="label label-primary">'.$data->city->name.'</span>';
                },
                'filter'  => City::model()->getFormattedList(),
            ],
            'name',
            'slug',
            'price',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/service/serviceBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Service::STATUS_PUBLIC => ['class' => 'label-success'],
                    Service::STATUS_MODERATE => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
