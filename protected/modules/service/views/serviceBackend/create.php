<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ServiceModule.service', 'Услуги') => ['/service/serviceBackend/index'],
    Yii::t('ServiceModule.service', 'Добавление'),
];

$this->pageTitle = Yii::t('ServiceModule.service', 'Услуги - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ServiceModule.service', 'Управление Услугами'), 'url' => ['/service/serviceBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ServiceModule.service', 'Добавить Услугу'), 'url' => ['/service/serviceBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ServiceModule.service', 'Услуги'); ?>
        <small><?=  Yii::t('ServiceModule.service', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>