<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ServiceModule.service', 'Услуги') => ['/service/serviceBackend/index'],
    $model->name => ['/service/serviceBackend/view', 'id' => $model->id],
    Yii::t('ServiceModule.service', 'Редактирование'),
];

$this->pageTitle = Yii::t('ServiceModule.service', 'Услуги - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ServiceModule.service', 'Управление Услугами'), 'url' => ['/service/serviceBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ServiceModule.service', 'Добавить Услугу'), 'url' => ['/service/serviceBackend/create']],
    ['label' => Yii::t('ServiceModule.service', 'Услуга') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('ServiceModule.service', 'Редактирование Услуги'), 'url' => [
        '/service/serviceBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('ServiceModule.service', 'Просмотреть Услугу'), 'url' => [
        '/service/serviceBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('ServiceModule.service', 'Удалить Услугу'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/service/serviceBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('ServiceModule.service', 'Вы уверены, что хотите удалить Услугу?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ServiceModule.service', 'Редактирование') . ' ' . Yii::t('ServiceModule.service', 'Услуги'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>