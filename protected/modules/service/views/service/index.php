<?php
/**
* Отображение для service/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     https://yupe.ru
**/
$this->pageTitle = Yii::t('ServiceModule.service', 'service');
$this->description = Yii::t('ServiceModule.service', 'service');
$this->keywords = Yii::t('ServiceModule.service', 'service');

$this->breadcrumbs = [Yii::t('ServiceModule.service', 'service')];
?>

<h1>
    <small>
        <?php echo Yii::t('ServiceModule.service', 'service'); ?>
    </small>
</h1>