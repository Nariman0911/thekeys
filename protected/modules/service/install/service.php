<?php
/**
 * Файл настроек для модуля service
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2020 amyLabs && Yupe! team
 * @package yupe.modules.service.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.service.ServiceModule',
    ],
    'import'    => [
    	'application.modules.service.listeners.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\ServiceSitemapGeneratorListener', 'onGenerate'],
                ]
            ],
        ],
    ],
    'rules'     => [
        // '/service' => 'service/service/index',
        '/services/<slug>' => 'service/service/view',
    ],
];