<?php
/**
 * ServicesWidget виджет для вывода услуг
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');
Yii::import('application.modules.service.models.*');

/**
 * Class ServicesWidget - Собираем услуги в городе
 */
class ServicesWidget extends yupe\widgets\YWidget
{
    public $id;
    public $page_id;
    public $limit;
    public $paginationLimit = false;
    public $order = 't.position ASC';
    /**
     * @var string
     */
    public $view = 'services-widget';

    protected $city = false;
    protected $page;
    protected $citypage;
    protected $dataProvider;

    public function init()
    {
        $this->page = Page::model()->published()->findByPk($this->page_id);

        /*$citySlug = Yii::app()->controller->citySlug;
        $city = City::model()->published()->findByAttributes(['slug' => $citySlug]);*/
        $city = Yii::app()->controller->geoCity;
        if($city){
            $this->city = $city;
        }

        if($this->page){
            $childPage = [];
            foreach ($this->page->getChildCityPage() as $key => $item) {
                $childPage = $item;
            }
            $this->citypage = ($childPage) ?: $this->page;
            
            $criteria = new CDbCriteria(array(
                'condition'=>'page_id=:page_id',
                'params'=>array(':page_id'=>$this->citypage->id),
            ));
            $criteria->addCondition("t.status = 1");
            $criteria->order = $this->order;

            if($this->limit){
                $criteria->limit = $this->limit;
            }

            if ($this->paginationLimit) {
                $this->paginationLimit = [
                    'pageSize' => $this->paginationLimit,
                    'pageVar' => 'page',
                ];
            }

            $this->dataProvider = new CActiveDataProvider('Service', [
                'criteria' => $criteria,
                'pagination' => $this->paginationLimit,
                'sort' => [
                    'sortVar' => 'sort',
                    'defaultOrder' => $this->order
                ],
            ]);
        }

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        
        $this->render($this->view,[
            'page' => $this->citypage,
            'dataProvider' => $this->dataProvider,
            'city' => $this->city
        ]);
    }
}
