<?php

/**
 * This is the model class for table "{{service}}".
 *
 * The followings are the available columns in table '{{service}}':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $parent_id
 * @property integer $page_id
 * @property integer $city_id
 * @property string $name_short
 * @property string $name
 * @property string $slug
 * @property string $name_h1
 * @property string $price
 * @property string $price_prefix
 * @property string $image
 * @property string $description_short
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $status
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property City $city
 * @property PagePage $page
 */

Yii::import('application.modules.page.models.*');

class Service extends yupe\models\YModel
{
	const STATUS_PUBLIC = 1;
	const STATUS_MODERATE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{service}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			['name_short, name, slug', 'required'],
			['create_user_id, update_user_id, parent_id, page_id, city_id, status, position', 'numerical', 'integerOnly'=>true],
			['name_short, name, slug, name_h1, price, price_prefix, image, back, meta_title', 'length', 'max'=>255],
			['slug', 'yupe\components\validators\YSLugValidator'],
			['slug', 'yupe\components\validators\YUniqueCityValidator'],
			['description_short, description, meta_keywords, meta_description', 'safe'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, create_user_id, update_user_id, create_time, update_time, parent_id, page_id, city_id, name_short, name, slug, name_h1, price, price_prefix, image, back, description_short, description, meta_title, meta_keywords, meta_description, status, position', 'safe', 'on'=>'search'],
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
		);
	}

	public function behaviors()
	{
		$module = Yii::app()->getModule('service');
	    return [
	    	'imageUpload' => [
                'class'         => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize'       => $module->minSize,
                'maxSize'       => $module->maxSize,
                'types'         => $module->allowedExtensions,
                'uploadPath'    => $module->uploadPath,
            ],
            'imageBackUpload' => [
                'class'         => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'back',
                'minSize'       => $module->minSize,
                'maxSize'       => $module->maxSize,
                'types'         => $module->allowedExtensions,
                'uploadPath'    => $module->uploadPath,
                'deleteFileKey' => 'delete-file-2'
            ],
	        'sortable' => [
	            'class' => 'yupe\components\behaviors\SortableBehavior',
	        ],
	    ];
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_user_id' => 'Create User',
			'update_user_id' => 'Update User',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'parent_id' => 'Родитель',
			'page_id' => 'Страница',
			'city_id' => 'Город',
			'name_short' => 'Короткое название',
			'name' => 'Название',
			'slug' => 'Alias',
			'name_h1' => 'Заголовок на странице',
			'price' => 'Цена',
			'price_prefix' => 'Префикс для цены',
			'image' => 'Изображение',
			'description_short' => 'Короткое описание',
			'description' => 'Описание',
			'meta_title' => 'Title (SEO)',
			'meta_keywords' => 'Ключевые слова SEO',
			'meta_description' => 'Описание SEO',
			'status' => 'Статус',
			'position' => 'Сортировка',
			'back' => 'Изображение на фон',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('name_short',$this->name_short,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('name_h1',$this->name_h1,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('price_prefix',$this->price_prefix,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('description_short',$this->description_short,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('position',$this->position);
		$criteria->compare('back',$this->back);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => ['defaultOrder' => 't.position ASC'],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Service the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     * @return bool
     */
     public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->update_user_id = Yii::app()->getUser()->getId();

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->create_user_id = Yii::app()->getUser()->getId();
        }
		return parent::beforeSave();
    }

    public function getStatusList()
	{
		return [
			self::STATUS_PUBLIC   => 'Опубликован',
			self::STATUS_MODERATE => 'На модерации',
		];
	}

	public function getStatusName()
	{
		$data = $this->getStatusList();
		if (isset($data[$this->status])) {
			return $data[$this->status];
		}
		return null;
	}

	public function scopes()
	{
	    return [
	        'published' => [
	            'condition' => 'status  = :status',
	            'params' => [
	                ':status' => self::STATUS_PUBLIC
	            ]
	        ],
	        'roots' => [
                'condition' => 'parent_id IS NULL',
            ],
	    ];
	}

	public function getWrapTitle($title)
    {
        /*$result = '';
        $title = explode(' ', $title);
        foreach ($title as $key => $item) {
           $result .= '<span>' . $item . ' </span>';
        }*/
        return $title;
    }

    public function getMenuList($page_id)
    {
        $data = [];
    	$page = Page::model()->published()->findByPk($page_id);

		$city = Yii::app()->controller->geoCity;
        if($city){
            $city = $city;
        }

		if($page){
			$childPage = [];
            foreach ($page->getChildCityPage($city) as $key => $item) {
                $childPage = $item;
            }
            $citypage = ($childPage) ?: $page;

            $criteria = new CDbCriteria(array(
                'condition'=>'page_id=:page_id',
                'params'=>array(':page_id'=>$citypage->id),
            ));
            $criteria->addCondition("t.status = 1");
            $criteria->order = 't.position ASC';
	    	foreach (self::model()->findAll($criteria) as $key => $item) {
	    		$data[] = [
	    			'label' => $item->name,
	    			'url' => Yii::app()->createUrl('service/service/view', ['slug' => $item->slug])
	    		];
	    	}
	    }

    	return $data;
    }

    public function getTitle()
    {
        return $this->name_h1 ? : $this->name;
    }
}
