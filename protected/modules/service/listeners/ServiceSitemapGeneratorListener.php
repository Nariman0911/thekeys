<?php

use yupe\components\Event;

Yii::import('application.modules.service.models.Service');

/**
 * Class SitemapGeneratorListener
 */
class ServiceSitemapGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();

        $provider = new CActiveDataProvider(Service::model()->published());

        foreach (new CDataProviderIterator($provider) as $item) {
            $url = Yii::app()->createAbsoluteUrl('/service/service/view', ['slug' => $item->slug]);
            $generator->addItem(
                $url,
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }
    }
} 
