<?php
/**
* ServiceController контроллер для service на публичной части сайта
*
* @author yupe team <team@yupe.ru>
* @link https://yupe.ru
* @copyright 2009-2020 amyLabs && Yupe! team
* @package yupe.modules.service.controllers
* @since 0.1
*
*/

class ServiceController extends \yupe\components\controllers\FrontController
{
    /**
     * Действие "по умолчанию"
     *
     * @return void
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionView($slug)
    {
        // $city = City::model()->published()->findByAttributes(['slug' => $this->citySlug]);
        $city = Yii::app()->controller->geoCity;
        if(!$city){
            throw new CHttpException(404, Yii::t('ServiceModule.service', 'Page was not found'));
        }
        $cityId = $city->id;

        $model = Service::model()->find(
            'slug = :slug AND (city_id=:city_id OR (city_id IS NULL))',
            [
                ':city_id' => $cityId,
                ':slug' => $slug,
            ]
        );

        if (null === $model) {
            throw new CHttpException(404, Yii::t('ServiceModule.service', 'Page was not found'));
        }

        $this->render('view', [
            'model' => $model,
            'city'  => $city
        ]);
    }
}