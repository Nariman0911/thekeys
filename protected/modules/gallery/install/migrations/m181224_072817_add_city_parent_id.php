<?php

class m181224_072817_add_city_parent_id extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{gallery_gallery}}', 'parent_id', 'integer');
        $this->addColumn('{{gallery_gallery}}', 'city_id', 'integer');
        
        $this->addForeignKey(
            'fk_{{gallery_gallery}}_gallery_city_id',
            '{{gallery_gallery}}',
            'city_id',
            '{{city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropColumn('{{gallery_gallery}}', 'parent_id');
        $this->dropColumn('{{gallery_gallery}}', 'city_id');
    }
}