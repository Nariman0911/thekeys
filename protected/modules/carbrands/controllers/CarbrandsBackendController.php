<?php
/**
* Класс CarbrandsBackendController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     https://yupe.ru
**/
class CarbrandsBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Carbrands.CarbrandsBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Carbrands.CarbrandsBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Carbrands.CarbrandsBackend.Create']],
            [
                'allow',
                'actions' => ['update', 'inline', 'sortable', 'deleteImage', 'sortablephoto'],
                'roles' => ['Carbrands.CarbrandsBackend.Update'],
            ],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Carbrands.CarbrandsBackend.Delete']],
            ['deny'],
        ];
    }
    
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Carbrands',
                'validAttributes' => [
                    'status', 'name'
                ]
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'Carbrands',
            ]
        ];
    }
    /**
    * Отображает Марку авто по указанному идентификатору
    *
    * @param integer $id Идинтификатор Марку авто для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    /**
    * Создает новую модель Марки авто.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new Carbrands;

        if (Yii::app()->getRequest()->getPost('Carbrands') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Carbrands'));
        
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('CarbrandsModule.carbrands', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }
    
    /**
    * Редактирование Марки авто.
    *
    * @param integer $id Идинтификатор Марку авто для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('Carbrands') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Carbrands'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('CarbrandsModule.carbrands', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }
    
    /**
    * Удаляет модель Марки авто из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Марки авто, который нужно удалить
    *
    * @return void
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('CarbrandsModule.carbrands', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('CarbrandsModule.carbrands', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }
    
    /**
    * Управление Марками авто.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new Carbrands('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Carbrands') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('Carbrands'));
        $this->render('index', ['model' => $model]);
    }
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = Carbrands::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('CarbrandsModule.carbrands', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
