<?php
/**
 * Файл настроек для модуля carbrands
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2020 amyLabs && Yupe! team
 * @package yupe.modules.carbrands.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.carbrands.CarbrandsModule',
    ],
    'import'    => [
        'application.modules.carbrands.models.*',
        'application.modules.carbrands.listeners.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\CarbrandsSitemapGeneratorListener', 'onGenerate'],
                ]
            ],
        ],
    ],
    'rules'     => [
        // '/carbrands' => 'carbrands/carbrands/index',
        '/carbrands/<slug>' => 'carbrands/carbrands/view',
    ],
];