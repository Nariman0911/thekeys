<?php
/**
 * Carbrands install migration
 * Класс миграций для модуля Carbrands:
 *
 * @category YupeMigration
 * @package  yupe.modules.carbrands.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     https://yupe.ru
 **/
class m000000_000001_add_column_back extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->addColumn('{{carbrands}}', 'back', 'string');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropColumn('{{carbrands}}', 'back');
    }
}
