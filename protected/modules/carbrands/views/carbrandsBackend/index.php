<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('CarbrandsModule.carbrands', 'Марки авто') => ['/carbrands/carbrandsBackend/index'],
    Yii::t('CarbrandsModule.carbrands', 'Управление'),
];

$this->pageTitle = Yii::t('CarbrandsModule.carbrands', 'Марки авто - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('CarbrandsModule.carbrands', 'Управление Марками авто'), 'url' => ['/carbrands/carbrandsBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('CarbrandsModule.carbrands', 'Добавить Марку авто'), 'url' => ['/carbrands/carbrandsBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('CarbrandsModule.carbrands', 'Марки авто'); ?>
        <small><?=  Yii::t('CarbrandsModule.carbrands', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('CarbrandsModule.carbrands', 'Поиск Марок авто');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('carbrands-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('CarbrandsModule.carbrands', 'В данном разделе представлены средства управления Марками авто'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'carbrands-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/carbrands/carbrandsBackend/sortable',
        'type'         => 'condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            // 'id',
            /*[
                'type' => 'raw',
                'name'   => 'page_id',
                'value'  => function($data){
                    return '<span class="label label-primary">'.strip_tags($data->page->getTitle()).'</span>';
                },
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'page_id',
                    Page::model()->getFormattedList(4),
                    ['encode' => false, 'empty' => '', 'class' => 'form-control']
                )
            ],*/
            [
                'type' => 'raw',
                'name'    => 'city_id',
                'value'  => function($data){
                    return '<span class="label label-primary">'.$data->city->name.'</span>';
                },
                'filter'  => City::model()->getFormattedList(),
            ],
            'name',
            'slug',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/carbrands/carbrandsBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Carbrands::STATUS_PUBLIC => ['class' => 'label-success'],
                    Carbrands::STATUS_MODERATE => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ]
    ]
); ?>
