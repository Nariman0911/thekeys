<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('CarbrandsModule.carbrands', 'Марки авто') => ['/carbrands/carbrandsBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('CarbrandsModule.carbrands', 'Марки авто - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('CarbrandsModule.carbrands', 'Управление Марками авто'), 'url' => ['/carbrands/carbrandsBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('CarbrandsModule.carbrands', 'Добавить Марку авто'), 'url' => ['/carbrands/carbrandsBackend/create']],
    ['label' => Yii::t('CarbrandsModule.carbrands', 'Марка авто') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('CarbrandsModule.carbrands', 'Редактирование Марки авто'), 'url' => [
        '/carbrands/carbrandsBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('CarbrandsModule.carbrands', 'Просмотреть Марку авто'), 'url' => [
        '/carbrands/carbrandsBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('CarbrandsModule.carbrands', 'Удалить Марку авто'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/carbrands/carbrandsBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('CarbrandsModule.carbrands', 'Вы уверены, что хотите удалить Марку авто?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('CarbrandsModule.carbrands', 'Просмотр') . ' ' . Yii::t('CarbrandsModule.carbrands', 'Марки авто'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_user_id',
        'update_user_id',
        'create_time',
        'update_time',
        'parent_id',
        'page_id',
        'city_id',
        'name_short',
        'name',
        'slug',
        'name_h1',
        'image',
        'description_short',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'status',
        'position',
    ],
]); ?>
