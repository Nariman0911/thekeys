<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('CarbrandsModule.carbrands', 'Марки авто') => ['/carbrands/carbrandsBackend/index'],
    Yii::t('CarbrandsModule.carbrands', 'Добавление'),
];

$this->pageTitle = Yii::t('CarbrandsModule.carbrands', 'Марки авто - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('CarbrandsModule.carbrands', 'Управление Марками авто'), 'url' => ['/carbrands/carbrandsBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('CarbrandsModule.carbrands', 'Добавить Марку авто'), 'url' => ['/carbrands/carbrandsBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('CarbrandsModule.carbrands', 'Марки авто'); ?>
        <small><?=  Yii::t('CarbrandsModule.carbrands', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>