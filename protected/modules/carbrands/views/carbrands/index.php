<?php
/**
* Отображение для carbrands/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     https://yupe.ru
**/
$this->pageTitle = Yii::t('CarbrandsModule.carbrands', 'carbrands');
$this->description = Yii::t('CarbrandsModule.carbrands', 'carbrands');
$this->keywords = Yii::t('CarbrandsModule.carbrands', 'carbrands');

$this->breadcrumbs = [Yii::t('CarbrandsModule.carbrands', 'carbrands')];
?>

<h1>
    <small>
        <?php echo Yii::t('CarbrandsModule.carbrands', 'carbrands'); ?>
    </small>
</h1>