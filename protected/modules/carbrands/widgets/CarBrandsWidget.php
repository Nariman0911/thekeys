<?php
/**
 * CarBrandsWidget виджет для вывода марок авто
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.carbrands.models.*');

/**
 * Class CarBrandsWidget
 */
class CarBrandsWidget extends yupe\widgets\YWidget
{
	public $limit;
	public $order = "t.name ASC";

    public $view = 'carbrands-widget';

    protected $city = false;
    protected $model;

    public function init()
    {
        $city = Yii::app()->controller->geoCity;
        if($city){
            $this->city = $city;
        }

        $cityId = $this->city->id;
        
		$criteria = new CDbCriteria(array(
            'condition'=>'city_id=:city_id OR city_id IS NULL',
            'params'=>array(':city_id'=>$cityId),
        ));
        if($this->limit){
            $criteria->limit = $this->limit;
        }
        $criteria->order = $this->order;
        $criteria->group = 'slug';

        $this->model = Carbrands::model()->published()->findAll($criteria);

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, [
            'model' => $this->model,
            'city' => $this->city
        ]);
    }
}
