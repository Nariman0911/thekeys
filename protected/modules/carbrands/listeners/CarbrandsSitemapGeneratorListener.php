<?php

use yupe\components\Event;

Yii::import('application.modules.carbrands.models.Carbrands');

/**
 * Class SitemapGeneratorListener
 */
class CarbrandsSitemapGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();

        $provider = new CActiveDataProvider(Carbrands::model()->published());

        foreach (new CDataProviderIterator($provider) as $item) {
            $url = Yii::app()->createAbsoluteUrl('/carbrands/carbrands/view', ['slug' => $item->slug]);
            $generator->addItem(
                $url,
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }
    }
} 
