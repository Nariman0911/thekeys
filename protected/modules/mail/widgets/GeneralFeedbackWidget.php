<?php

/**
 * GeneralFeedbackWidget
 */
class GeneralFeedbackWidget extends \yupe\widgets\YWidget
{
    public $id = null;
    public $modalHtmlOptions = [];
    public $formOptions = [];
    public $formClassName = null;
    public $successKey = 'success';
    public $successMessage = 'Ваша заявка успешно отправлена.';
    public $isRefresh = false;
    public $buttonModal = null;
    public $titleModal = 'Заказать звонок';
    public $subTitleModal = 'Оставьте заявку и мы Вам перезвоним!';
    public $showCloseButton = true;
    public $sendButtonText = 'Отправить';
    public $sendButtonId = null;
    public $view = 'general-feedback-widget';
    public $modelAttributes = [];
    public $eventCode = 'zayavki-iz-modalnyh-okon';
    public $isTemplate = true;

    // Для формула трафика
    public $showAttributeName = true;
    public $showAttributePhone = true;
    public $showAttributeEmail = false;
    public $showAttributeComment = false;
    public $showAttributeJson = false;

    public function init()
    {
        if ($this->formClassName===null) {
            throw new CException("Необходимо установить свойство 'formClassName' классе ".get_class($this));
        }

        if ($this->id===null) {
            throw new CException("Необходимо установить свойство 'id' классе ".get_class($this));
        }

        // установка параметров для модального окна
        $this->modalHtmlOptions['id'] = $this->id;
        if (isset($this->modalHtmlOptions['class'])) {
            $this->modalHtmlOptions['class'] = $this->modalHtmlOptions['class'].' modal fade';
        }
        $this->modalHtmlOptions['tabindex'] = "-1";

        if (!isset($this->formOptions['id'])) {
            $this->formOptions['id'] = $this->id.'-form';
        }

        if ($this->sendButtonId===null) {
            $this->sendButtonId = $this->id.'-send-button';
        }

        return parent::init();
    }

    public function run()
    {
        $model = new $this->formClassName;

        if (isset($_POST[$this->formClassName])) {
            $key = isset($_POST[$this->formClassName]['key']) ? $_POST[$this->formClassName]['key'] : '';
            if ($key===$this->id) {
                $model->setAttributes($_POST[$this->formClassName]);
                if ($model->verifyCode == '') {
                    if ($model->validate()) {
                        $this->sendData($model->attributes);
                        Yii::app()->user->setFlash($this->successKey, $this->successMessage);
                        if ($this->isRefresh) {
                            Yii::app()->controller->refresh();
                        }
                    }
                }
            }
        }

        $this->render($this->view, [
            'model' => $model,
        ]);
    }

    public function sendData($data)
    {
        $dataSerialize = [];
        $data = array_merge($data, $this->modelAttributes);

        // Отправка данных на почту
        if($this->isTemplate){
            $mail = Yii::app()->mail;
            $from = Yii::app()->getModule('city')->notifyEmailFrom;
            $theme = "Заявка - {$data['theme']}";

            $body = Yii::app()->controller->renderPartial('//mail/mail/_email', ['model' => $data], true);

            $city = Yii::app()->controller->geoCity;
            if($city->notifyEmailsTo){
                $notifyEmailsTo = explode(',', $city->notifyEmailsTo);
            } else{
                $defaultCity = Yii::app()->getModule('city')->defaultCity;
                $city = City::model()->published()->findByAttributes(['slug' => $defaultCity]);
                $notifyEmailsTo = explode(',', $city->notifyEmailsTo);
            }

            $emailTo = explode(',', Yii::app()->getModule('city')->notifyEmailsTo);
            foreach ($emailTo as $key => $to) {
                $notifyEmailsTo[] = $to;
            }
            
            foreach ($notifyEmailsTo as $key => $to) {
                $mail->send($from, $to, $theme, $body);
            }

        } else {
            if ($this->eventCode) {
                Yii::app()->mailMessage->raiseMailEvent($this->eventCode, $data);
            }
        }
    }
}
