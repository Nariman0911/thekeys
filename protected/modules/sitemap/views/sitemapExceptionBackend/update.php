<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('SitemapModule.sitemap', 'Исключении') => ['/sitemap/sitemapExceptionBackend/index'],
    $model->id => ['/sitemap/sitemapExceptionBackend/view', 'id' => $model->id],
    Yii::t('SitemapModule.sitemap', 'Редактирование'),
];

$this->pageTitle = Yii::t('SitemapModule.sitemap', 'Исключении - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('SitemapModule.sitemap', 'Управление Исключениями'), 'url' => ['/sitemap/sitemapExceptionBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('SitemapModule.sitemap', 'Добавить Исключения'), 'url' => ['/sitemap/sitemapExceptionBackend/create']],
    ['label' => Yii::t('SitemapModule.sitemap', 'Исключения') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('SitemapModule.sitemap', 'Редактирование Исключений'), 'url' => [
        '/sitemap/sitemapExceptionBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('SitemapModule.sitemap', 'Просмотреть Исключения'), 'url' => [
        '/sitemap/sitemapExceptionBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('SitemapModule.sitemap', 'Удалить Исключения'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/sitemap/sitemapExceptionBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('SitemapModule.sitemap', 'Вы уверены, что хотите удалить Исключения?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('SitemapModule.sitemap', 'Редактирование') . ' ' . Yii::t('SitemapModule.sitemap', 'Исключений'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>