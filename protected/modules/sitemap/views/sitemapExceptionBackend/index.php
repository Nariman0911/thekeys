<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('SitemapModule.sitemap', 'Исключении') => ['/sitemap/sitemapExceptionBackend/index'],
    Yii::t('SitemapModule.sitemap', 'Управление'),
];

$this->pageTitle = Yii::t('SitemapModule.sitemap', 'Исключении - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('SitemapModule.sitemap', 'Управление Исключениями'), 'url' => ['/sitemap/sitemapExceptionBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('SitemapModule.sitemap', 'Добавить Исключения'), 'url' => ['/sitemap/sitemapExceptionBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('SitemapModule.sitemap', 'Исключении'); ?>
        <small><?=  Yii::t('SitemapModule.sitemap', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('SitemapModule.sitemap', 'Поиск Исключений');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('sitemap-exception-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('SitemapModule.sitemap', 'В данном разделе представлены средства управления Исключениями'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'sitemap-exception-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            'exception_url',
            // [
            //     'name'  => 'is_child',
            //     'value' => function($data){
            //         if($data->is_child){
            //             $isChild = '<span class="label label-primary">Да</span>';
            //         }

            //         return $isChild;
            //     },
            //     'type' => 'raw',
            //     'filter' => CHtml::activeDropDownList($model, 'is_child', SitemapException::model()->getIsChild(), ['encode' => false, 'empty' => '', 'class' => 'form-control']),
            //     'htmlOptions' => ['width' => '220px'],
            // ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
