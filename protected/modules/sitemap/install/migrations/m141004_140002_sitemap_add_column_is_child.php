<?php

class m141004_140002_sitemap_add_column_is_child extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{sitemap_exception}}', 'is_child', "boolean not null default '0'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{sitemap_exception}}', 'is_child');
    }
}
