<?php

class m141004_140001_sitemap_add_table_exteption extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{sitemap_exception}}",
            [
                "id" => "pk",
                "exception_url" => "varchar(250) not null",
            ],
            $this->getOptions()
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys("{{sitemap_exception}}");
    }
}
