<?php

/**
 * Class BuyOneClickWidget
 */
Yii::import('application.modules.cart.CartModule');

class BuyOneClickWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'buy-one-click';

    /**
     * @throws CException
     */
    public function run()
    {
        $deliveryTypes = Delivery::model()->published()->findAll();
        
    	$model = new Order(Order::SCENARIO_USER);

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Order')) {

            $order = Yii::app()->getRequest()->getPost('Order');
            $order['delivery_id'] = 1;

            $products = Yii::app()->getRequest()->getPost('OrderProduct');
            // $products['quantity'] = 1;

            $coupons = isset($order['couponCodes']) ? $order['couponCodes'] : [];

            // print_r($model->store($order, $products, Yii::app()->getUser()->getId(), (int)Yii::app()->getModule('order')->defaultStatus));
            // exit();
            if ($model->store($order, $products, Yii::app()->getUser()->getId(), (int)Yii::app()->getModule('order')->defaultStatus)) {

                if (!empty($coupons)) {
                    $model->applyCoupons($coupons);
                }

                // Yii::app()->getUser()->setFlash(
                //     yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                //     Yii::t('OrderModule.order', 'The order created')
                // );
                

                Yii::app()->getUser()->setFlash(
                    "buy-one-click-success",
                    Yii::t('OrderModule.order', 'Ваш заказ №'.$model->id. ' от '. date("d.m.Y H:i:s"). ' успешно отправлен. В ближайшее время Вам перезвонит менеджер для уточнения деталей заказа.' )
                );

                Yii::app()->eventManager->fire(OrderEvents::CREATED_HTTP, new OrderEvent($model));
                
                Yii::app()->controller->refresh();
            }
        }

        $this->render($this->view, [
            'order' => $model,
            'deliveryTypes' => $deliveryTypes
        ]);
    }
}
