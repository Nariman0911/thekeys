<?php
/**
 *  SitemappsCommand
 */
class SitemappsCommand extends \yupe\components\ConsoleCommand
{

    public function actionIndex()
    {
        $module = Yii::app()->getModule('sitemap');
        $sitemapFile = $module->getSiteMapPath();
        
        if (\yupe\helpers\YFile::rmIfExists($sitemapFile)) {

            $staticPages = SitemapPage::model()->getData();

            $this->generator->generate($sitemapFile, $staticPages);
        }
    }
}
