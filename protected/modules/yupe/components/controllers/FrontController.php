<?php
/**
 * Базовый класс для всех контроллеров публичной части
 *
 * @category YupeComponents
 * @package  yupe.modules.yupe.components.controllers
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @version  0.6
 * @link     https://yupe.ru
 **/

namespace yupe\components\controllers;

use Yii;
use yupe\events\YupeControllerInitEvent;
use yupe\events\YupeEvents;
use application\components\Controller;

Yii::import('application.components.TinyMinify.TinyMinify');

/**
 * Class FrontController
 * @package yupe\components\controllers
 */
abstract class FrontController extends Controller
{
    public $mainAssets;
    public $geoCity;
    public $citySlug;
    public $storeCountPage = 20;
    /**
     * Вызывается при инициализации FrontController
     * Присваивает значения, необходимым переменным
     */
    public function init()
    {
        
        Yii::app()->eventManager->fire(YupeEvents::BEFORE_FRONT_CONTROLLER_INIT, new YupeControllerInitEvent($this, Yii::app()->getUser()));

        parent::init();

        Yii::app()->theme = $this->yupe->theme ?: 'default';

        $this->mainAssets = Yii::app()->getTheme()->getAssetsUrl();

        $bootstrap = Yii::app()->getTheme()->getBasePath() . DIRECTORY_SEPARATOR . "bootstrap.php";

        if (is_file($bootstrap)) {
            require $bootstrap;
        }

        $this->cityRedirect();

    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'InlineWidgetsBehavior' => [
                'class' => 'yupe.components.behaviors.InlineWidgetsBehavior',
                'classSuffix' => 'Widget',
                'startBlock' => '[[w:',
                'endBlock' => ']]',
                'widgets' => Yii::app()->params['runtimeWidgets'],
            ],
        ];
    }

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }
    
    public function afterRender($view, &$output)
    {
        $output = \TinyMinify::html($output, $options = [
            'collapse_whitespace' => false,
            'disable_comments' => false,
        ]);

        return parent::afterRender($view, $output);
    }

    public function cityRedirect()
    {
        $url = $_SERVER['HTTP_HOST'];
        $domain = Yii::app()->params['url'];
        $defaultCity = Yii::app()->getModule('city')->defaultCity;
        
        if($domain == $url){
            $this->citySlug = $defaultCity;
        } else {
            $this->citySlug = explode(".",$_SERVER['HTTP_HOST'])[0];
        }

        if(isset($_COOKIE["citySlug"])) {
            $this->citySlug = $_COOKIE["citySlug"];
        }

        $this->geoCity = \City::model()->published()->findByAttributes(['slug' => $this->citySlug]);
        

        if(!$this->geoCity){
            $this->geoCity = \City::model()->published()->findByAttributes(['slug' => $defaultCity]);

            $this->citySlug = $this->geoCity->slug;
        }

        $subDomain = explode(".",$_SERVER['HTTP_HOST'])[0];

        if($subDomain != $this->citySlug AND $this->citySlug != ''){
            if($this->citySlug == $defaultCity){
                if($subDomain != explode(".",$domain)[0]){
                    // echo 't1' . '<br>';
                    $uri = $_SERVER['REQUEST_SCHEME'] . '://' . $domain . $_SERVER['REQUEST_URI'];
                    $this->redirect($uri);
                }
            }else{
                // echo 't2' . '<br>';
                $uri = $_SERVER['REQUEST_SCHEME'] . '://'. $this->citySlug . '.' . $domain . $_SERVER['REQUEST_URI'];
                $this->redirect($uri);
            }
        } else {
            /* Для города по-умолчанию */
            if($this->citySlug == $defaultCity){
                // echo 't3' . '<br>';
                $uri = $_SERVER['REQUEST_SCHEME'] . '://' . $domain . $_SERVER['REQUEST_URI'];
                $this->redirect($uri);
            }
        }
    }
}
