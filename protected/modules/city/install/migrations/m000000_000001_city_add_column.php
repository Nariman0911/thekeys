<?php

/**
 * m000000_000001_city_add_column add_column migration
 * Класс миграций для модуля Page:
 *
 */
class m000000_000001_city_add_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{city}}', 'notifyEmailsTo', 'text');
    }

    public function safeDown()
    {
        $this->dropColumn('{{city}}', 'notifyEmailsTo');
    }
}
