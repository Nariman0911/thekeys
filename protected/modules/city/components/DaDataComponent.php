<?php

/**
 * DaDataComponent
 */
class DaDataComponent extends CApplicationComponent
{
    public $token       = null;
    public $cachePrefix = 'dadata_';
    public $cache       = false;
    public $cacheTime   = 86400;

    public function init()
    {
        if ($this->token === null) {
            throw new CException("Необходимо установить токет DaData");
        }
    }

    /**
     * Получить Регион из DaData
     * @return array
     */
    public function getRegion($query)
    {
        $cacheKey = $this->cachePrefix.'region_'.md5($query);
        $result = Yii::app()->cache->get($cacheKey);
        if ($result===false) {
            $dadata = new \Dadata\DadataClient($this->token, null);
            $result = $dadata->suggest(
                "address",
                $query,
                \Dadata\Settings::SUGGESTION_COUNT,
                [
                    'from_bound' => ['value' => 'region'],
                    'to_bound'   => ['value' => 'region'],
                ]
            );
            Yii::app()->cache->set($cacheKey, $result, $this->cacheTime);
        }

        return $result;
    }

    /**
     * Получить город из DaData
     * @return array
     */
    public function getCity($query)
    {
        $cacheKey = $this->cachePrefix.'city_'.md5($query);
        $result = Yii::app()->cache->get($cacheKey);
        if ($result===false) {
            $dadata = new \Dadata\DadataClient($this->token, null);
            $result = $dadata->suggest(
                "address",
                $query,
                \Dadata\Settings::SUGGESTION_COUNT,
                [
                    'from_bound' => ['value' => 'city'],
                    'to_bound'   => ['value' => 'city'],
                ]
            );

            Yii::app()->cache->set($cacheKey, $result, $this->cacheTime);
        }

        return $result;
    }

    public function getAddress($query)
    {
        $cacheKey = $this->cachePrefix.'address_'.md5($query);
        $result = Yii::app()->cache->get($cacheKey);
        if ($result===false) {
            $dadata = new \Dadata\DadataClient($this->token, null);
            $result = $dadata->suggest("address", $query, \Dadata\Settings::SUGGESTION_COUNT);

            Yii::app()->cache->set($cacheKey, $result, $this->cacheTime);
        }

        return $result;
    }

    public function iplocate()
    {
        $ip = CHttpRequest::getUserHostAddress();
        $cacheKey = $this->cachePrefix.md5($ip);
        $result = Yii::app()->cache->get($cacheKey);
        if ($result===false) {
            $dadata = new \Dadata\DadataClient($this->token, null);
            $result = $dadata->iplocate($ip);

            Yii::app()->cache->set($cacheKey, $result, $this->cacheTime);
        }

        return $result;
    }
}
