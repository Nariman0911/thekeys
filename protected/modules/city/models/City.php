<?php

/**
 * This is the model class for table "{{city}}".
 *
 * The followings are the available columns in table '{{city}}':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $parent_id
 * @property string $name_short
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $phone
 * @property string $email
 * @property string $mode
 * @property string $address
 * @property string $code_map
 * @property string $coords
 * @property string $description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $whatsapp
 * @property string $viber
 * @property string $telegram
 * @property string $vk
 * @property string $instagram
 * @property string $facebook
 * @property string $ok
 * @property integer $is_default
 * @property integer $status
 * @property integer $position
 * @property integer $notifyEmailsTo
 */
class City extends yupe\models\YModel
{
	const STATUS_PUBLIC = 1;
	const STATUS_MODERATE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{city}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['name_short, name, slug', 'required'],
			['create_user_id, update_user_id, parent_id, is_default, status, position', 'numerical', 'integerOnly'=>true],
			['slug', 'yupe\components\validators\YSLugValidator'],
            ['slug', 'unique'],
			['name_short, name, slug, image, email, mode, coords, meta_title, whatsapp, viber, telegram, vk, instagram, facebook, ok', 'length', 'max'=>255],
			['phone, address, code_map, description, meta_keywords, meta_description, notifyEmailsTo', 'safe'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, create_user_id, update_user_id, create_time, update_time, parent_id, name_short, name, slug, image, phone, email, mode, address, code_map, coords, description, meta_title, meta_keywords, meta_description, whatsapp, viber, telegram, vk, instagram, facebook, ok, is_default, status, position, notifyEmailsTo', 'safe', 'on'=>'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'parentCity'   => [self::BELONGS_TO, 'City', 'parent_id'],
            'childrenCity' => [self::HAS_MANY, 'City', 'parent_id'],
		];
	}

	public function behaviors()
	{
		$module = Yii::app()->getModule('city');
	    return [
	    	'imageUpload' => [
                'class'         => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize'       => $module->minSize,
                'maxSize'       => $module->maxSize,
                'types'         => $module->allowedExtensions,
                'uploadPath'    => $module->uploadPath,
            ],
	        'sortable' => [
	            'class' => 'yupe\components\behaviors\SortableBehavior',
	        ],
	    ];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_user_id' => 'Create User',
			'update_user_id' => 'Update User',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'parent_id' => 'Родитель',
			'name_short' => 'Короткое название',
			'name' => 'Название',
			'slug' => 'Alias',
			'image' => 'Изображение',
			'phone' => 'Телефон',
			'email' => 'E-mail',
			'mode' => 'График работы',
			'address' => 'Адрес',
			'code_map' => 'Код карты',
			'coords' => 'Координаты на карте',
			'description' => 'Описание',
			'meta_title' => 'Title (SEO)',
			'meta_keywords' => 'Ключевые слова SEO',
			'meta_description' => 'Описание SEO',
			'whatsapp' => 'Whatsapp',
			'viber' => 'Viber',
			'telegram' => 'Telegram',
			'vk' => 'Vkontakte',
			'instagram' => 'Instagram',
			'facebook' => 'Facebook',
			'ok' => 'Odnoklasniki',
			'is_default' => 'Город по-умолчанию',
			'status' => 'Статус',
			'position' => 'Сортировка',
			'notifyEmailsTo' => 'Почта на которую будут отправляться заявки',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('name_short',$this->name_short,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('mode',$this->mode,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('code_map',$this->code_map,true);
		$criteria->compare('coords',$this->coords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('whatsapp',$this->whatsapp,true);
		$criteria->compare('viber',$this->viber,true);
		$criteria->compare('telegram',$this->telegram,true);
		$criteria->compare('vk',$this->vk,true);
		$criteria->compare('instagram',$this->instagram,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('ok',$this->ok,true);
		$criteria->compare('is_default',$this->is_default);
		$criteria->compare('status',$this->status);
		$criteria->compare('position',$this->position);
		$criteria->compare('notifyEmailsTo',$this->notifyEmailsTo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => ['defaultOrder' => 't.position ASC'],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return City the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     * @return bool
     */
     public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->update_user_id = Yii::app()->getUser()->getId();

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->create_user_id = Yii::app()->getUser()->getId();
        }
		return parent::beforeSave();
    }

    public function getStatusList()
	{
		return [
			self::STATUS_PUBLIC   => 'Опубликован',
			self::STATUS_MODERATE => 'На модерации',
		];
	}

	public function getStatusName()
	{
		$data = $this->getStatusList();
		if (isset($data[$this->status])) {
			return $data[$this->status];
		}
		return null;
	}

	public function scopes()
	{
	    return [
	        'published' => [
	            'condition' => 'status  = :status',
	            'params' => [
	                ':status' => self::STATUS_PUBLIC
	            ]
	        ],
	        'roots' => [
                'condition' => 'parent_id IS NULL',
            ],
	    ];
	}

	public function getFormattedList($parentId = null, $level = 0, $criteria = null)
    {
        if (empty($parentId)) {
            $parentId = null;
        }

        $models = $this->findAllByAttributes(['parent_id' => $parentId], $criteria);

        $list = [];

        foreach ($models as $model) {

            $model->name = str_repeat('&emsp;', $level) . $model->name;

            $list[$model->id] = $model->name;

            $list = CMap::mergeArray($list, $this->getFormattedList($model->id, $level + 1, $criteria));
        }

        return $list;
    }

    public function getAddress()
    {
    	$name = trim($this->name);
    	$address = '';
    	if($this->address){
    		$address = ", {$this->address}";
    	}
    	return "г. {$name}{$address}";
    }
}
