<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('CityModule.city', 'Города') => ['/city/cityBackend/index'],
    Yii::t('CityModule.city', 'Управление'),
];

$this->pageTitle = Yii::t('CityModule.city', 'Города - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('CityModule.city', 'Управление Городами'), 'url' => ['/city/cityBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('CityModule.city', 'Добавить Город'), 'url' => ['/city/cityBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('CityModule.city', 'Города'); ?>
        <small><?=  Yii::t('CityModule.city', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('CityModule.city', 'Поиск Городов');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('city-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('CityModule.city', 'В данном разделе представлены средства управления Городами'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'city-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/city/cityBackend/sortable',
        'type'         => 'condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            // 'create_user_id',
            // 'update_user_id',
            // 'create_time',
            // 'update_time',
            'parent_id',
           'name_short',
           'name',
           'slug',
//            'image',
//            'phone',
//            'email',
//            'mode',
//            'address',
//            'code_map',
//            'coords',
//            'description',
//            'meta_title',
//            'meta_keywords',
//            'meta_description',
//            'whatsapp',
//            'viber',
//            'telegram',
//            'vk',
//            'instagram',
//            'facebook',
//            'ok',
//            'is_default',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/city/cityBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    City::STATUS_PUBLIC => ['class' => 'label-success'],
                    City::STATUS_MODERATE => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ]
        ],
    ]
); ?>
