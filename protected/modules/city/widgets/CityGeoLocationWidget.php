<?php
Yii::import('application.modules.city.models.City');
/**
 * CityGeoLocationWidget виджет определения города
 */
class CityGeoLocationWidget extends \yupe\widgets\YWidget
{
    public function run()
    {
        $module = Yii::app()->getModule('city');

        $model = City::model()->findAll();

        $this->_registerScript();

        $this->render('city-widget', [
            'model' => $model,
        ]);
    }

    protected function _registerScript()
    {
        $url = $_SERVER['HTTP_HOST'];
        $domain = Yii::app()->params['url'];
        $defaultCity = Yii::app()->getModule('city')->defaultCity;

        Yii::app()->clientScript->registerScript(__FILE__, "
            var options = {
                'path': '/'
            };

            var options_sub = {
                'domain': '{$domain}',
                'path': '/'
            };

            if (getCookie('jsPoll') === undefined) {
                $('.js-poll').show();
            }

            // При подтверждении что выбран мой город
            $('.js-poll-yes').on('click', function() {
                if('{$url}' == '{$domain}'){
                    setCookie('jsPoll', '1', options);
                } else {
                    setCookie('jsPoll', '1', options_sub);
                }
                $('.js-poll').hide();

                return false;
            });

            $('.js-poll-no').on('click', function() {
                var modal = $('.js-city').data('target');
                $(modal).modal('show');
                $('.js-poll').hide();
                return false;
            });

            $('.js-city-link').on('click', function() {
                if('{$url}' == '{$domain}'){
                    setCookie('jsPoll', '1', options);
                } else {
                    setCookie('jsPoll', '1', options_sub);
                }
            });

        ");
    }
}
