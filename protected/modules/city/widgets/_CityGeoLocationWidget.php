<?php
Yii::import('application.modules.city.models.City');
/**
 * CityGeoLocationWidget виджет определения города
 */
class CityGeoLocationWidget extends \yupe\widgets\YWidget
{
    public $cityJson = '';
    public $dadataCity = '';
    public $city = '';
    public $userAddress = '';
    
    public function run()
    {
        $module = Yii::app()->getModule('city');

        $model = City::model()->findAll();

        $json = [];
        foreach ($model as $key => $city) {

            $phone = explode("<br>", $city->phone);

            $json[] = [
                'id'        => $city->id,
                'name'      => $city->name,
                'slug'      => $city->slug,
                'phone'     => $phone[0],
                'email'     => $city->email,
                'mode'      => $city->mode,
                'location'  => $city->getAddress(),
                'code_map'  => $city->code_map,
                'vk'        => $city->vk,
                'instagram' => $city->instagram,
                'facebook'  => $city->facebook,
            ];
        }

        $this->cityJson = CJavaScript::encode($json);

        $this->city = Yii::app()->controller->geoCity;
        if($this->city){
            $this->userAddress = $this->city->name;
        }

        /*if(empty($_COOKIE['citySlug'])){
            $result = Yii::app()->dadata->iplocate();

            if($result['data']['city'] != null){
                $this->dadataCity = $result['data']['city'];
            }
        }*/

        $this->_registerScript();

        $this->render('city-widget', [
            'model' => $model,
        ]);
    }

    protected function _registerScript()
    {
        $metrika = Yii::app()->params['yandexMetrika'];

        $url = $_SERVER['HTTP_HOST'];
        $domain = Yii::app()->params['url'];
        $defaultCity = Yii::app()->getModule('city')->defaultCity;

        Yii::app()->clientScript->registerScript(__FILE__, "
            
            var cityJson = {$this->cityJson};
            //console.log(getCookie('citySlug'));
            var options = {
                'path': '/'
            };

            var options_sub = {
                'domain': '{$domain}',
                'path': '/'
            };
            
            if (getCookie('citySlug') === undefined) {
                // Получение местоположения пользователя.
                var userAddress = '{$this->userAddress}';
                // var userAddress = '{$this->dadataCity}';
                // console.log(result.geoObjects.get(0).properties);
                // userAddress = 'Оренбург';
                var c = cityJson.map(function(item, i) {
                    if(item.name == userAddress){
                        $('.js-city span').text(item.location);
                        $('.js-city span').attr('data-city', userAddress);
                    } 
                    console.log(userAddress);
                    $('.js-poll').show();
                });
            } else {
                var cityKey = getCookie('cityKey');
                $('.js-city span').text(cityJson[cityKey].location);
                $('.js-city span').attr('data-city', cityJson[cityKey].name);
                // $('.js-poll').show();
            }

            // При подтверждении что выбран мой город
            $('.js-poll-yes').on('click', function() {
                var cityName = $('.js-city span').data('city');

                cityJson.map(function(item, i) {
                    if (item.name == cityName.trim()) {
                        if('{$url}' == '{$domain}'){
                            createCookie(cityName, i, item.id, item.slug);
                        } else {
                            createCookieSubDomain(cityName, i, item.id, item.slug);
                        }
                        $('.js-poll').hide();
                    }
                });

                return false;
            });

            $('.js-poll-no').on('click', function() {
                var modal = $('.js-city').data('target');
                $(modal).modal('show');
                $('.js-poll').hide();
                return false;
            });
            
            $('.js-city-link').on('click', function() {
                var elem = $(this);
                var cityName = elem.data('city');
                var cityKey = elem.data('key');
                var cityId = elem.data('id');
                var citySlug = elem.data('city-slug');
                
                if('{$url}' != '{$domain}' && citySlug == '{$defaultCity}'){
                    createCookie(cityName, cityKey, cityId, citySlug);
                } else {
                    createCookieSubDomain(cityName, cityKey, cityId, citySlug);
                }
                window.location.reload();

                return false;
            });

            function createCookie(cityName, cityKey, cityId, citySlug){
                if(getCookie('citySlug')){
                    setCookie('cityName', '', {'max-age': -1});
                    setCookie('cityKey', '', {'max-age': -1});
                    setCookie('cityId', '', {'max-age': -1});
                    setCookie('citySlug', '', {'max-age': -1});
                }

                setCookie('cityName', cityName, options);
                setCookie('cityKey', cityKey, options);
                setCookie('cityId', cityId, options);
                setCookie('citySlug', citySlug, options);
            }

            function createCookieSubDomain(cityName, cityKey, cityId, citySlug){
                if(getCookie('citySlug')){
                    setCookie('cityName', '', {
                        'max-age': -1,
                        'path': '/',
                        'domain': '{$url}'
                    });
                    setCookie('cityKey', '', {
                        'max-age': -1,
                        'path': '/',
                        'domain': '{$url}'
                    });
                    setCookie('cityId', '', {
                        'max-age': -1,
                        'path': '/',
                        'domain': '{$url}'
                    });
                    setCookie('citySlug', '', {
                        'max-age': -1,
                        'path': '/',
                        'domain': '{$url}'
                    });
                }

                setCookie('cityName', cityName, options_sub);
                setCookie('cityKey', cityKey, options_sub);
                setCookie('cityId', cityId, options_sub);
                setCookie('citySlug', citySlug, options_sub);
            }
        ");
    }
}
