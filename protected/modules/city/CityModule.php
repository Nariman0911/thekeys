<?php
/**
 * CityModule основной класс модуля city
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2020 amyLabs && Yupe! team
 * @package yupe.modules.city
 * @since 0.1
 */
use yupe\components\WebModule;
use yupe\models\Settings;

/**
 * Class OrderModule
 */
// class CityModule extends WebModule
class CityModule  extends yupe\components\WebModule
{
    const VERSION = '0.9.8';

    public $uploadPath = 'city';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;
    /**
     * @var int
     */
    public $maxFiles = 1;
    public $defaultCity = 'orenburg';
    
    public $notifyEmailFrom;
    public $notifyEmailsTo;

    public $templateProp = 'city_';
    public $notifyEmailsToCity = [];

    /*public function __construct($params1, $params2)
    {
        foreach ($this->getCityes() as $key => $value) {
            $this->notifyEmailsToCity[$this->templateProp.$key] = null;
        }
        return parent::__construct($params1, $params2);
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->notifyEmailsToCity)) {
            return $this->notifyEmailsToCity[$name];
        }

        return parent::__get($name);
    }

    public function getSettings($needReset = false)
    {
        if ($needReset) {
            Yii::app()->getCache()->clear($this->getId());
        }
        try {
            $settingsRows = Yii::app()->db
                ->cache($this->coreCacheTime, new \TagsCache($this->getId(), 'settings'))
                ->createCommand(
                    '
                    SELECT param_name, param_value
                        FROM {{yupe_settings}}
                        WHERE module_id = :module_id AND type = :type
                    '
                )
                ->bindValue(':module_id', $this->getId())
                ->bindValue(':type', Settings::TYPE_CORE)
                ->queryAll();


            if (!empty($settingsRows)) {
                foreach ($settingsRows as $sRow) {
                    if (property_exists($this, $sRow['param_name'])) {
                        $this->{$sRow['param_name']} = $sRow['param_value'];
                    } else if (array_key_exists($sRow['param_name'], $this->notifyEmailsToCity)) {
                        $this->notifyEmailsToCity[$sRow['param_name']] = $sRow['param_value'];
                    }
                }
            }

            return true;
        } catch (CDbException $e) {
            return false;
        }
    }*/

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return parent::getDependencies();
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array или false
     */
    public function checkSelf()
    {
        return parent::checkSelf();
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('CityModule.city', 'Контент');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        $labels = [
            'defaultCity' => 'Город по умолчанию',
            'notifyEmailFrom' => 'Email, от имени которого отправлять оповещения',
            'notifyEmailsTo' => 'Получатели оповещений (через запятую) общие для всех городов',
        ];

        /*foreach ($this->getCityes() as $key => $value) {
            $labels[$this->templateProp.$key] = 'Получатели оповещений (через запятую) ('.$value.')';
        }
*/
        return $labels;

        // return parent::getParamsLabels();
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        $editablePrams = [
            'defaultCity' => $this->getCityes(),
            'notifyEmailFrom',
            'notifyEmailsTo',
        ];

        /*foreach ($this->getCityes() as $key => $value) {
           $editablePrams[] = $this->templateProp.$key;
        }*/
        
        return $editablePrams;

        // return parent::getEditableParams();
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        $groups = [
            '0.main' => [
                'label' => 'Настройки',
                'items' => [
                    'defaultCity',
                ],
            ],
            '1.notify' => [
                'label' => 'Оповещения',
                'items' => [
                    'notifyEmailFrom',
                    'notifyEmailsTo',
                ],
            ],
        ];

        // $groups['1.notify']['items'] += array_keys($this->notifyEmailsToCity);

        return $groups;
        // return parent::getEditableParamsGroups();
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('CityModule.city', 'Города')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('CityModule.city', 'Города'),
                'url' => ['/city/cityBackend/index']
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('CityModule.city', self::VERSION);
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('CityModule.city', 'https://yupe.ru');
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('CityModule.city', 'Города');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('CityModule.city', 'Описание модуля "Города"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('CityModule.city', 'yupe team');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('CityModule.city', 'team@yupe.ru');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/city/cityBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-globe";
    }

    /**
      * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
      *
      * @return bool
      **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'city.models.*',
                'city.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'City.CityManager',
                'description' => Yii::t('CityModule.city', 'Manage city'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'City.CityBackend.Index',
                        'description' => Yii::t('CityModule.city', 'Index')
                    ],
                ]
            ]
        ];
    }

    public function getCityes()
    {
        return CHtml::listData(City::model()->roots()->findAll(), 'slug', 'name');
    }
}
