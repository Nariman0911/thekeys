<?php
/**
* Класс StoreModelMarkaBackendController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     https://yupe.ru
**/
class StoreModelMarkaBackendController extends \yupe\components\controllers\BackController
{
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'StoreModelMarka',
                'validAttributes' => [
                    'status', 'name'
                ]
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'StoreModelMarka',
            ]
        ];
    }
    /**
    * Отображает Модель марки по указанному идентификатору
    *
    * @param integer $id Идинтификатор Модель марки для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    /**
    * Создает новую модель Модели марки.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new StoreModelMarka;

        if (Yii::app()->getRequest()->getPost('StoreModelMarka') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('StoreModelMarka'));
        
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('StoreModule.store', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }
    
    /**
    * Редактирование Модели марки.
    *
    * @param integer $id Идинтификатор Модель марки для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('StoreModelMarka') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('StoreModelMarka'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('StoreModule.store', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }
    
    /**
    * Удаляет модель Модели марки из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Модели марки, который нужно удалить
    *
    * @return void
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('StoreModule.store', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('StoreModule.store', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }
    
    /**
    * Управление Моделями марки.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new StoreModelMarka('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('StoreModelMarka') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('StoreModelMarka'));
        $this->render('index', ['model' => $model]);
    }
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = StoreModelMarka::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Запрошенная страница не найдена.'));

        return $model;
    }

    public function actionAjaxLoadMarkaModel()
    {
        if (!Yii::app()->getRequest()->getQuery('ids')) {
            throw new CHttpException(404);
        }

        $ids = Yii::app()->getRequest()->getQuery('ids');
        // $models = StoreModelMarka::model()->published()->findAllByPk($ids);

        $criteria = new CDbCriteria();

        $criteria->addInCondition('t.marka_id', array_unique($ids));
        
        $criteria->order = 't.position ASC';

        $models = StoreModelMarka::model()->findAll($criteria);

        $options = "";
        foreach($models as $key => $data)
        {
            $options .= "<label class='marka-models__item'>";
            $options .= CHtml::checkBox('Product[markaModels][]', '', [
                'id' => 'Product_markaModels_'.$data->id,
                'value' => $data->id,
                'data-mark-id' => $data->marka_id,
            ]);
            $options .= ' ' . $data->name;
            $options .= " </label>|";
        }

        echo $options;
        // $this->renderPartial('_load_model_marka', ['models' => $models]);
    }
}
