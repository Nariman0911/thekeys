<?php

/**
 * Class StorageBackendController
 */
class StorageBackendController extends yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin'],],
            ['allow', 'actions' => ['index'], 'roles' => ['Store.StorageBackend.Index'],],
            ['deny',],
        ];
    }

    /**
     *
     */
    public function actionCreate()
    {

    }

    /**
     *
     */
    public function actionIndex()
    {
        $model = new Storage('search');
        $model->unsetAttributes(); // clear any default values

        if (isset($_GET['Storage'])) {
            $model->attributes = $_GET['Storage'];
        }

        $this->render('index', ['model' => $model]);
    }

}
