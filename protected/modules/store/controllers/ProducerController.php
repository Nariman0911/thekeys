<?php
use yupe\components\controllers\FrontController;

/**
 * Class ProducerController
 */
class ProducerController extends FrontController
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var ProducerRepository
     */
    protected $producerRepository;

    /**
     * @var AttributeFilter
     */
    protected $attributeFilter;
    
    /**
     *
     */
    public function init()
    {
        $this->productRepository = Yii::app()->getComponent('productRepository');

        $this->producerRepository = Yii::app()->getComponent('producerRepository');

        $this->attributeFilter = Yii::app()->getComponent('attributesFilter');

        parent::init();
    }

    /**
     *
     */
    public function actionIndex()
    {
        $this->render(
            'index',
            [
                'brands' => $this->producerRepository->getAllDataProvider(),
            ]
        );
    }

    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionView($slug)
    {
        $producer = $this->producerRepository->getBySlug($slug);

        if (null === $producer) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }

        $typesSearchParam = $this->attributeFilter->getTypeAttributesForSearchFromQuery(Yii::app()->getRequest());

        $mainSearchParam = $this->attributeFilter->getMainAttributesForSearchFromQuery(
            Yii::app()->getRequest(),
            [
                AttributeFilter::MAIN_SEARCH_PARAM_NAME => Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)
            ]
        );

        if (!empty($mainSearchParam) || !empty($typesSearchParam)) {
            $data = $this->productRepository->getByBrandProvider($producer, $mainSearchParam, $typesSearchParam);
        } else {
            $data = $this->productRepository->getByBrandProvider($producer);
        }

        $this->render(
            $producer->view ?: 'view',
            [
                'brand' => $producer,
                'products' => $data,
            ]
        );
    }
}
