<?php

use yupe\components\controllers\FrontController;

/**
 * Class ProductController
 */
class ProductController extends FrontController
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var AttributeFilter
     */
    protected $attributeFilter;

    /**
     *
     */
    public function init()
    {
        $this->productRepository = Yii::app()->getComponent('productRepository');
        $this->attributeFilter = Yii::app()->getComponent('attributesFilter');

        parent::init();
    }

    /**
     *
     */
    public function actionIndex()
    {
        $typesSearchParam = $this->attributeFilter->getTypeAttributesForSearchFromQuery(Yii::app()->getRequest());

        $mainSearchParam = $this->attributeFilter->getMainAttributesForSearchFromQuery(
            Yii::app()->getRequest(),
            [
                AttributeFilter::MAIN_SEARCH_PARAM_NAME => Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME),
                AttributeFilter::MAIN_SEARCH_PARAM_MARKI => Yii::app()->getRequest()->getQuery('marki'),
                AttributeFilter::MAIN_SEARCH_PARAM_MARKMODELS => Yii::app()->getRequest()->getQuery('markaModels'),
                AttributeFilter::MAIN_SEARCH_PARAM_MODELSYEARS => Yii::app()->getRequest()->getQuery('markaModelsYears'),
            ]
        );

        

        if (!empty($mainSearchParam) || !empty($typesSearchParam)) {
            $data = $this->productRepository->getByFilter($mainSearchParam, $typesSearchParam);
        } else {
            $data = $this->productRepository->getListForIndexPage();
        }

        $this->render(
            'index',
            [
                'dataProvider' => $data,
            ]
        );
    }

    /**
     * @param string $name Product slug
     * @param string $category Product category path
     * @throws CHttpException
     */
    public function actionView($name, $category = null)
    {
        $product = $this->productRepository->getBySlug(
            $name,
            [
                'type.typeAttributes',
                'category',
                'variants',
                'attributesValues',
            ]
        );

        if (
            null === $product ||
            (isset($product->category) && $product->category->path !== $category) ||
            (!isset($product->category) && !is_null($category))
        ) {
            throw new CHttpException(404, Yii::t('StoreModule.catalog', 'Product was not found!'));
        }

        Yii::app()->eventManager->fire(StoreEvents::PRODUCT_OPEN, new ProductOpenEvent($product));

        $this->render($product->view ?:'view', ['product' => $product]);
    }

    /**
     *
     */
    public function actionSearch()
    {
        $typesSearchParam = $this->attributeFilter->getTypeAttributesForSearchFromQuery(Yii::app()->getRequest());

        $mainSearchParam = $this->attributeFilter->getMainAttributesForSearchFromQuery(
            Yii::app()->getRequest(),
            [
                AttributeFilter::MAIN_SEARCH_PARAM_NAME => Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)
            ]
        );

        if (!empty($mainSearchParam) || !empty($typesSearchParam)) {
            $data = $this->productRepository->getByFilter($mainSearchParam, $typesSearchParam);
        } else {
            $data = $this->productRepository->getListForIndexPage();
        }

        $this->render(
            'search',
            [
                'dataProvider' => $data,
            ]
        );
    }

    public function actionProductDiscount()
    {
        $criteria = new CDbCriteria();
        
        $id = Yii::app()->getRequest()->getPost('id') ;

        if($id){
            $category = StoreCategory::model()->published()->findByPk($id);
            if ($category) {
                $criteria->addCondition("t.category_id={$category->id}");
            }
        }

        $criteria->addCondition("t.discount_price IS NOT NULL OR t.discount IS NOT NULL");
        
        $dataProvider = new CActiveDataProvider(
            'Product',
            [
                'criteria' => $criteria,
                'pagination' => false,
            ]
        );
        $this->renderPartial('productDiscount', ['dataProvider' => $dataProvider]);
    }

    public function actionAjaxLoadMarkaModel()
    {
        if($marki = $_POST['marki']) {
            $criteria = new CDbCriteria(array(
                'condition'=>'marka_id=:marka_id',
                'params'=>array(':marka_id'=>(int) $marki),
            ));
            
            $criteria->order = 'position ASC';

            $models = StoreModelMarka::model()->published()->findAll($criteria);

            $options = $this->optionsList($models);
            echo $options;
        }

        if($markaModels = $_POST['markaModels']) {
            $models = StoreMarkaModelModelYear::model()->findAllByAttributes(['marka_model_id' => $markaModels]);
            if($models){
                $newModels = [];
                foreach ($models as $key => $data) {
                    $newModels[] = $data->model_year_id;
                }

                $years = StoreModelMarkaYear::model()->published()->findAllByAttributes(['id' => $newModels]);

                $options = $this->optionsList($years);
                echo $options;
            } else {
                $models = StoreModelMarkaYear::model()->published()->findAll();

                $options = $this->optionsList($models);
                echo $options;
            }
        }
    }

    public function optionsList($models)
    {
        $options = CHtml::tag('option', ['value' => 0], '-- выберите --', true);
        foreach(CHtml::listData($models, 'id', 'name') as $key => $data) {
            $options .= CHtml::tag('option', ['value' => $key], CHtml::encode($data), true);
        }

        return $options;
    }
}