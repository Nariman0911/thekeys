<?php

class m181218_121829_store_add_table_producer_city extends yupe\components\DbMigration
{
    public function safeUp()
    {
    	$this->addColumn('{{store_producer}}', 'city_id', 'integer default NULL');
        $this->createTable(
            "{{store_producer_city}}",
            [
                "id" => "pk",
                "name_short" => "varchar(150) not null",
                "name" => "varchar(250) not null",
                "slug" => "varchar(150) not null",
                "image" => "varchar(250) default null",
                "description" => "text",
                "meta_title" => "varchar(250) default null",
                "meta_keywords" => "varchar(250) default null",
                "meta_description" => "varchar(250) default null",
                "status" => "integer not null default '1'",
                "order" => "integer not null default '0'",
            ],
            $this->getOptions()
        );

        $this->addForeignKey("fk_{{store_producer}}_city", "{{store_producer}}", "city_id", "{{store_producer_city}}", "id", "CASCADE", "CASCADE");

    }

    public function safeDown()
    {
    }
}