<?php

Yii::import('application.modules.store.models.Product');

class m181218_121826_store_model_to_year extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable('{{store_model_marka_model_year}}', [
            'id'      => 'pk',
            'marka_model_id' => 'integer',
            'model_year_id' => 'integer',
        ], $this->getOptions());
        
        $this->addForeignKey(
            "fk_{{store_model_marka_model_year}}_marka_model_id",
            '{{store_model_marka_model_year}}',
            'marka_model_id',
            '{{store_model_marka}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            "fk_{{store_model_marka_model_year}}_model_year_id",
            '{{store_model_marka_model_year}}',
            'model_year_id',
            '{{store_model_year}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{store_model_marka_model_year}}');
    }
}