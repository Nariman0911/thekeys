<?php

Yii::import('application.modules.store.models.Product');

class m181218_121826_store_product_product extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable('{{store_product_product}}', [
            'id'      => 'pk',
            'parent_product_id' => 'integer',
            'product_id' => 'integer',
        ], $this->getOptions());
        
        $this->addForeignKey(
            "fk_{{store_product_product}}_product_id",
            '{{store_product_product}}',
            'product_id',
            '{{store_product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{store_product_product}}');
    }
}