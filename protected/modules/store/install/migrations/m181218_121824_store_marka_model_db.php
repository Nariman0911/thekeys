<?php

Yii::import('application.modules.store.models.Product');

class m181218_121824_store_marka_model_db extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $languages = file_get_contents(__DIR__.'/marka.sql');
        $languages = str_replace('{{prefix}}', Yii::app()->getDb()->tablePrefix, $languages);
        $this->getDbConnection()->createCommand($languages)->execute();
    }
}