<?php

class m161122_093737_store_storage_base extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            "{{store_storage}}",
            [
                "id" => "pk",
                "name" => "varchar(250) not null",
                "external_id" => "varchar(250) not null",
            ],
            $this->getOptions()
        );

        $this->createTable(
            "{{store_storage_count}}",
            [
                "id" => "pk",
                "storage_id" => "int(11) not null",
                "product_id" => "int(11) not null",
            ],
            $this->getOptions()
        );

    }

    public function safeDown()
    {
        $this->dropTable("{{store_storage}}");
        $this->dropTable("{{store_storage_count}}");
    }
}
