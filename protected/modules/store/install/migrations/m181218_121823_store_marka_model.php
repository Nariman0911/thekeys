<?php

Yii::import('application.modules.store.models.Product');

class m181218_121823_store_marka_model extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{store_marka}}',
            [
                'id'             => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_user_id' => "integer NOT NULL",
                'update_user_id' => "integer NOT NULL",
                'create_time'    => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
                'name'              => 'string COMMENT "Название"',
                'status'            => 'integer COMMENT "Статус"',
                'position'          => 'integer COMMENT "Сортировка"',
            ],
            $this->getOptions()
        );

        $this->createTable(
            '{{store_model_marka}}',
            [
                'id'             => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_user_id' => "integer NOT NULL",
                'update_user_id' => "integer NOT NULL",
                'create_time'    => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
                'marka_id'          => 'integer COMMENT "Марка авто"',
                'name'              => 'string COMMENT "Название"',
                'status'            => 'integer COMMENT "Статус"',
                'position'          => 'integer COMMENT "Сортировка"',
            ],
            $this->getOptions()
        );

        $this->addForeignKey(
            "fk_{{store_model_marka}}_marka_id",
            '{{store_model_marka}}',
            'marka_id',
            '{{store_marka}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{store_marka}}');
        $this->dropTableWithForeignKeys('{{store_model_marka}}');
    }
}