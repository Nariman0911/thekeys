<?php

Yii::import('application.modules.store.models.Product');

class m181218_121827_store_product_marka_model_year extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable('{{store_product_marka}}', [
            'id'      => 'pk',
            'product_id' => 'integer',
            'marka_id' => 'integer',
        ], $this->getOptions());

        $this->addForeignKey(
            "fk_{{store_product_marka}}_product_id",
            '{{store_product_marka}}',
            'product_id',
            '{{store_product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            "fk_{{store_product_marka}}_marka_id",
            '{{store_product_marka}}',
            'marka_id',
            '{{store_marka}}',
            'id',
            'CASCADE',
            'CASCADE'
        );


        $this->createTable('{{store_product_marka_model}}', [
            'id'      => 'pk',
            'product_id' => 'integer',
            'marka_model_id' => 'integer',
        ], $this->getOptions());
        
        $this->addForeignKey(
            "fk_{{store_product_marka_model}}_product_id",
            '{{store_product_marka_model}}',
            'product_id',
            '{{store_product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            "fk_{{store_product_marka_model}}_marka_model_id",
            '{{store_product_marka_model}}',
            'marka_model_id',
            '{{store_model_marka}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('{{store_product_marka_model_year}}', [
            'id'      => 'pk',
            'product_id' => 'integer',
            'model_year_id' => 'integer',
        ], $this->getOptions());
        
        $this->addForeignKey(
            "fk_{{store_product_marka_model_year}}_product_id",
            '{{store_product_marka_model_year}}',
            'product_id',
            '{{store_product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            "fk_{{store_product_marka_model_year}}_model_year_id",
            '{{store_product_marka_model_year}}',
            'model_year_id',
            '{{store_model_year}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{store_product_marka}}');
        $this->dropTableWithForeignKeys('{{store_product_marka_model}}');
        $this->dropTableWithForeignKeys('{{store_product_marka_model_year}}');
    }
}