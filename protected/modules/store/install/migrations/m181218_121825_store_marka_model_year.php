<?php

Yii::import('application.modules.store.models.Product');

class m181218_121825_store_marka_model_year extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{store_model_year}}',
            [
                'id'             => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_user_id' => "integer NOT NULL",
                'update_user_id' => "integer NOT NULL",
                'create_time'    => 'datetime NOT NULL',
                'update_time'    => 'datetime NOT NULL',
                'name'              => 'string COMMENT "Название"',
                'status'            => 'integer COMMENT "Статус"',
                'position'          => 'integer COMMENT "Сортировка"',
            ],
            $this->getOptions()
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{store_model_year}}');
    }
}