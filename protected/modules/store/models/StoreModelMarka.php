<?php

/**
 * This is the model class for table "{{store_model_marka}}".
 *
 * The followings are the available columns in table '{{store_model_marka}}':
 * @property integer $id
 * @property integer $marka_id
 * @property string $name
 * @property string $status
 * @property string $position
 */
class StoreModelMarka extends \yupe\models\YModel
{
	const STATUS_PUBLIC = 1;
	const STATUS_MODERATE = 0;

	public $years;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_model_marka}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['marka_id, status, position', 'numerical', 'integerOnly'=>true],
			['name', 'length', 'max'=>255],
			['name', 'unique'],
			['years', 'safe'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, marka_id, name, status, position', 'safe', 'on'=>'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'marka' => [self::BELONGS_TO, 'StoreMarka', 'marka_id'],
			'markaModelModelYear' => [self::HAS_MANY, 'StoreMarkaModelModelYear', 'marka_model_id'],
		);
	}

	public function behaviors()
	{
		$module = Yii::app()->getModule('store');
	    return [
	        'sortable' => [
	            'class' => 'yupe\components\behaviors\SortableBehavior',
	        ],
	    ];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'marka_id' => 'Марка авто',
			'name' => 'Модель марки',
			'status' => 'Статус',
			'position' => 'Сортировка',
			'years' => 'Года выпусков',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('marka_id',$this->marka_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => ['defaultOrder' => 't.position ASC'],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StoreModelMarka the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterFind()
	{
		$years = StoreMarkaModelModelYear::model()->findAllByAttributes(['marka_model_id' => $this->id]);
		foreach ($years as $key => $year) {
			$this->years[] = $year->model_year_id;
		}
	}

	public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->update_user_id = Yii::app()->getUser()->getId();

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->create_user_id = Yii::app()->getUser()->getId();
        }
		return parent::beforeSave();
    }

    public function afterSave()
	{
		if (!empty($this->years)) {
			$this->saveYears($this->years);
		} else {
			$this->saveYears([]);
		}
		return parent::afterSave();
	}

    public function getStatusList()
	{
		return [
			self::STATUS_PUBLIC   => 'Опубликован',
			self::STATUS_MODERATE => 'На модерации',
		];
	}

	public function getStatusName()
	{
		$data = $this->getStatusList();
		if (isset($data[$this->status])) {
			return $data[$this->status];
		}
		return null;
	}

	public function scopes()
	{
	    return [
	        'published' => [
	            'condition' => 'status  = :status',
	            'params' => [
	                ':status' => self::STATUS_PUBLIC
	            ]
	        ],
	    ];
	}

	public function saveYears(array $yearsId)
    {
        StoreMarkaModelModelYear::model()->deleteAllByAttributes(['marka_model_id' => $this->id]);
        foreach ($yearsId as $key => $year) {
        	$model = new StoreMarkaModelModelYear;
        	$model->model_year_id = $year;
        	$model->marka_model_id = $this->id;
        	$model->save();
        }
    }


	public function getMarkaModelsList()
	{
		return CHtml::listData(self::model()->published()->findAll(), 'id', 'name');
	}

	public function getModelListQuery($request)
	{
		$marka_id = $request->getQuery('marki', false);
		if($marka_id){
			return CHtml::listData(self::model()->published()->findAllByAttributes(['marka_id' => $marka_id]), 'id', 'name');
		}

		return [];
	}
}
