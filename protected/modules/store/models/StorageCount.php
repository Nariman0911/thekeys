<?php

/**
 *
 * @property integer $id
 * @property integer $storage_id
 * @property integer $product_id
 * @property string $quantity
 *
 */
class StorageCount extends \yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{store_storage_count}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Type the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['storage_id, product_id, quantity', 'numerical', 'integerOnly' => true],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'storage' => [self::BELONGS_TO, 'Storage', 'storage_id'],
            'product' => [self::BELONGS_TO, 'Product', 'product_id'],
        ];
    }

}