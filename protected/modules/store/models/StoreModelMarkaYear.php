<?php

/**
 * This is the model class for table "{{store_model_year}}".
 *
 * The followings are the available columns in table '{{store_model_year}}':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property string $create_time
 * @property string $update_time
 * @property string $name
 * @property integer $status
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property StoreModelMarka $modelMarka
 */
class StoreModelMarkaYear extends \yupe\models\YModel
{
	const STATUS_PUBLIC = 1;
	const STATUS_MODERATE = 0;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_model_year}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('create_user_id, update_user_id, status, position', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			['name', 'unique'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, create_user_id, update_user_id, create_time, update_time, name, status, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'modelMarka' => array(self::BELONGS_TO, 'StoreModelMarka', 'model_marka_id'),
			'markaModelModelYear' => [self::HAS_MANY, 'StoreMarkaModelModelYear', 'model_year_id'],
		);
	}

	public function behaviors()
	{
		$module = Yii::app()->getModule('store');
	    return [
	        'sortable' => [
	            'class' => 'yupe\components\behaviors\SortableBehavior',
	        ],
	    ];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_user_id' => 'Create User',
			'update_user_id' => 'Update User',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'name' => 'Название',
			'status' => 'Статус',
			'position' => 'Сортировка',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => ['defaultOrder' => 't.position DESC'],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StoreModelMarkaYear the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->update_user_id = Yii::app()->getUser()->getId();

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->create_user_id = Yii::app()->getUser()->getId();
        }
		return parent::beforeSave();
    }

    public function getStatusList()
	{
		return [
			self::STATUS_PUBLIC   => 'Опубликован',
			self::STATUS_MODERATE => 'На модерации',
		];
	}

	public function getStatusName()
	{
		$data = $this->getStatusList();
		if (isset($data[$this->status])) {
			return $data[$this->status];
		}
		return null;
	}

	public function scopes()
	{
	    return [
	        'published' => [
	            'condition' => 'status  = :status',
	            'params' => [
	                ':status' => self::STATUS_PUBLIC
	            ]
	        ],
	    ];
	}

	public function getYearList()
	{
		return CHtml::listData(self::model()->published()->findAll(['order' => 'position DESC']), 'id', 'name');
	}

	public function getYearModelListQuery($request)
	{
		$models_id = $request->getQuery('markaModels', false);
		if($models_id){
			$years = StoreMarkaModelModelYear::model()->findAllByAttributes(['marka_model_id' => $models_id]);

			if($years){
				$newYear = [];

				foreach ($years as $key => $data) {
		            $newYear[] = $data->model_year_id;
		        }

				return CHtml::listData(StoreModelMarkaYear::model()->published()->findAllByPk($newYear), 'id', 'name');
			}

			return CHtml::listData(self::model()->published()->findAll(), 'id', 'name');
		}

		return [];
	}
}
