<?php

/**
 * This is the model class for table "{{store_model_marka_model_year}}".
 *
 * The followings are the available columns in table '{{store_model_marka_model_year}}':
 * @property integer $id
 * @property integer $marka_model_id
 * @property integer $model_year_id
 *
 * The followings are the available model relations:
 * @property StoreModelMarka $markaModel
 * @property StoreModelYear $modelYear
 */
class StoreMarkaModelModelYear extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{store_model_marka_model_year}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('marka_model_id, model_year_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, marka_model_id, model_year_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'markaModel' => array(self::BELONGS_TO, 'StoreModelMarka', 'marka_model_id'),
			'modelYear' => array(self::BELONGS_TO, 'StoreModelMarkaYear', 'model_year_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'marka_model_id' => 'Marka Model',
			'model_year_id' => 'Model Year',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('marka_model_id',$this->marka_model_id);
		$criteria->compare('model_year_id',$this->model_year_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StoreMarkaModelModelYear the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
