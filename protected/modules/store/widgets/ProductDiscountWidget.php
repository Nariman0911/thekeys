<?php
Yii::import('application.modules.store.models.Product');

/**
 * Class ProductDiscountWidget
 *
 */
class ProductDiscountWidget extends \yupe\widgets\YWidget
{
	public $view = 'view-product-discount';

	public function run()
	{
		$ids_category = Yii::app()->getDb()->createCommand()
		    ->select('sc.id')
		    ->from('{{store_product}} as pr')
		    ->leftJoin('{{store_category}} as sc', 'pr.category_id = sc.id')
		    ->where('pr.discount_price IS NOT NULL OR pr.discount IS NOT NULL')
		    ->queryAll();

	   $ids = [];
		foreach ($ids_category as $key => $item) {
			$ids[] = $item['id'];
		}

		$category = StoreCategory::model()->findAllByPk($ids);
		
		$this->render($this->view, [
			'category' => $category
		]);
	}
}