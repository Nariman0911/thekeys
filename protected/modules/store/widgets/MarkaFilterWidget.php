<?php
/**
 * MarkaFilterWidget виджет для фильтров
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.review.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.store.models.*');

class MarkaFilterWidget extends yupe\widgets\YWidget
{
	public $view = 'marka-form-widget';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new Product;

        $this->render($this->view, [
        	'model' => $model 
        ]);
     
    }
}
