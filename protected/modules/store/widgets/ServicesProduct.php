<?php
/**
 * ServicesProduct виджет для вывода доп. услуг
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.review.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.store.models.*');

class ServicesProduct extends yupe\widgets\YWidget
{
	public $product;
	public $code = null;

	public $limit = false;
	public $is_service = true;

	public $view = 'services-form-widget';

	protected $productRepository;
	protected $model;
	protected $dataProvider;

    public function init()
    {
    	$this->productRepository = new ProductRepository();

        parent::init();
    }

    public function run()
    {
    	$this->dataProvider = $this->productRepository->getLinkedProductsDataProvider($this->product, $this->code);

		 if ($this->dataProvider->itemCount) {
		 	$this->render($this->view, [
	        	'dataProvider' => $this->dataProvider 
	        ]);
		 } else {
	    	$criteria = new CDbCriteria();

	        if($this->limit){
	            $criteria->limit = $this->limit;
	        }
	        
	        $criteria->order = 't.position ASC';
	        $criteria->compare('is_special', true);

	        $this->dataProvider = new CActiveDataProvider('Product', [
	            'criteria' => $criteria,
	            'pagination' => [
	                'pageSize' => $this->limit,
	            ],
	        ]);

	        $this->render($this->view, [
	        	'dataProvider' => $this->dataProvider,
	        	'id' => $this->product->id,
	        ]);
		 }
    }
}
