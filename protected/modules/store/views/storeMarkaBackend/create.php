<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('StoreModule.store', 'Марки авто') => ['/store/storeMarkaBackend/index'],
    Yii::t('StoreModule.store', 'Добавление'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Марки авто - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StoreModule.store', 'Управление Марками авто'), 'url' => ['/store/storeMarkaBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StoreModule.store', 'Добавить Марку авто'), 'url' => ['/store/storeMarkaBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('StoreModule.store', 'Марки авто'); ?>
        <small><?=  Yii::t('StoreModule.store', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>