<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('StoreModule.store', 'Марки авто') => ['/store/storeMarkaBackend/index'],
    $model->id => ['/store/storeMarkaBackend/view', 'id' => $model->id],
    Yii::t('StoreModule.store', 'Редактирование'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Марки авто - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StoreModule.store', 'Управление Марками авто'), 'url' => ['/store/storeMarkaBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StoreModule.store', 'Добавить Марку авто'), 'url' => ['/store/storeMarkaBackend/create']],
    ['label' => Yii::t('StoreModule.store', 'Марка авто') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('StoreModule.store', 'Редактирование Марки авто'), 'url' => [
        '/store/storeMarkaBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('StoreModule.store', 'Просмотреть Марку авто'), 'url' => [
        '/store/storeMarkaBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('StoreModule.store', 'Удалить Марку авто'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/store/storeMarkaBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('StoreModule.store', 'Вы уверены, что хотите удалить Марку авто?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('StoreModule.store', 'Редактирование') . ' ' . Yii::t('StoreModule.store', 'Марки авто'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>