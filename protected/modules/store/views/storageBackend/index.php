<?php
$this->breadcrumbs = [
    Yii::t('StoreModule.store', 'Product storages') => ['/store/storageBackend/index'],
    Yii::t('StoreModule.store', 'Manage'),
];

$this->pageTitle = Yii::t('StoreModule.store', 'Product storages - manage');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StoreModule.store', 'Storage manage'), 'url' => ['/store/storageBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StoreModule.store', 'Create storage'), 'url' => ['/store/storageBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('StoreModule.store', 'Product storages'); ?>
        <small><?= Yii::t('StoreModule.store', 'administration'); ?></small>
    </h1>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'storage-grid',
        'type' => 'condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, array("/store/storageBackend/update", "id" => $data->id))',
            ],

            [
                'name' => 'external_id',
                'type' => 'raw',
                'value' => 'CHtml::link($data->name, array("/store/storageBackend/update", "id" => $data->id))',
            ],

            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]
); ?>
