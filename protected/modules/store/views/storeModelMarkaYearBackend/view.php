<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('StoreModule.store', 'Года выпусков') => ['/store/storeModelMarkaYearBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('StoreModule.store', 'Года выпусков - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StoreModule.store', 'Управление Годами выпусков'), 'url' => ['/store/storeModelMarkaYearBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StoreModule.store', 'Добавить Год выпуска'), 'url' => ['/store/storeModelMarkaYearBackend/create']],
    ['label' => Yii::t('StoreModule.store', 'Год выпуска') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('StoreModule.store', 'Редактирование Года выпуска'), 'url' => [
        '/store/storeModelMarkaYearBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('StoreModule.store', 'Просмотреть Год выпуска'), 'url' => [
        '/store/storeModelMarkaYearBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('StoreModule.store', 'Удалить Год выпуска'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/store/storeModelMarkaYearBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('StoreModule.store', 'Вы уверены, что хотите удалить Год выпуска?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('StoreModule.store', 'Просмотр') . ' ' . Yii::t('StoreModule.store', 'Года выпуска'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_user_id',
        'update_user_id',
        'create_time',
        'update_time',
        'name',
        'status',
        'position',
    ],
]); ?>
