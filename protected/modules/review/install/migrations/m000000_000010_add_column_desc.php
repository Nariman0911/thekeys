<?php

/**
 * Comment install migration
 * Класс миграций для модуля Comment:
 *
 * @category YupeMigration
 * @package  yupe.modules.comment.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000010_add_column_desc extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{review}}', 'short_desc', 'text');
        $this->addColumn('{{review}}', 'audio', 'string');
        $this->addColumn('{{review}}', 'video', 'string');
        $this->addColumn('{{review}}', 'code', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn('{{review}}', 'short_desc');
        $this->dropColumn('{{review}}', 'audio');
        $this->dropColumn('{{review}}', 'video');
        $this->dropColumn('{{review}}', 'code');
    }
}
