<?php
/**
 * ReviewNewWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.review.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.review.models.*');
Yii::import('application.modules.page.models.*');

class ReviewNewWidget extends yupe\widgets\YWidget
{
    public $carbrands_id;
    public $page_id;
	public $limit;
    public $paginationLimit = false;
    public $order = 't.position DESC';
	public $view = 'reviews-widget';

    protected $city = false;
    protected $page;
    protected $citypage;
    protected $dataProvider;

    public function init()
    {
        $criteria = new CDbCriteria();

        $criteria->addCondition("t.moderation = 1");
        $criteria->order = $this->order;
        
        if($this->limit){
            $criteria->limit = $this->limit;
        }

        $criteria->compare("t.carbrands_id", $this->carbrands_id);

        $city = Yii::app()->controller->geoCity;
        if($city){
            // $criteria->addCondition("t.city_id = {$city->id}");
            $criteria->addCondition("t.city_id IS NULL OR t.city_id = {$city->id}");
            $this->city = $city;
        } else {
            $criteria->addCondition("t.city_id IS NULL");
        }

        if ($this->paginationLimit) {
            $this->paginationLimit = [
                'pageSize' => $this->paginationLimit,
                'pageVar' => 'page',
            ];
        }

        if($this->page_id){
            $this->page = Page::model()->published()->findByPk($this->page_id);

            if($this->page){
                $childPage = [];
                foreach ($this->page->getChildCityPage($this->city) as $key => $item) {
                    $childPage = $item;
                }
                $this->citypage = ($childPage) ?: $this->page;
            }
        }

        $this->dataProvider = new CActiveDataProvider('Review', [
            'criteria' => $criteria,
            'pagination' => $this->paginationLimit,
            'sort' => [
                'sortVar' => 'sort',
                'defaultOrder' => $this->order,
            ],
        ]);

        parent::init();
    }


    public function run()
    {
        $this->render($this->view, [
            'page' => $this->citypage,
        	'dataProvider' => $this->dataProvider,
            'city' => $this->city
        ]);
    }
}
