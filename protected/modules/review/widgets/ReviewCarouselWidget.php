<?php
/**
 * ReviewNewWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.review.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.review.models.*');

class ReviewCarouselWidget extends yupe\widgets\YWidget
{
    public $limit;
    public $page_id;
    public $marka_id;
    public $view = 'review-carousel';

    public function run()
    {
        $criteria = new CDbCriteria();

        $criteria->addCondition("t.moderation = 1");
        $criteria->order = 't.position DESC';
        
        if($this->limit){
            $criteria->limit = $this->limit;
        }

        /*$citySlug = Yii::app()->controller->citySlug;
        $city = City::model()->published()->findByAttributes(['slug' => $citySlug]);*/
        $city = Yii::app()->controller->geoCity;
        if($city){
            $criteria->addCondition("t.city_id = {$city->id}");
        }

        $model = Review::model()->findAll($criteria);

        $this->render($this->view, [
            'model' => $model
        ]);
    }
}
