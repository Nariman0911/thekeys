<?php
/**
 * Файл настроек для модуля city
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2020 amyLabs && Yupe! team
 * @package yupe.modules.city.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.city.CityModule',
    ],
    'import'    => [
		'application.modules.city.models.*',
    ],
    'component' => [
        'dadata' => [
            'class' => 'application.modules.city.components.DaDataComponent',
            'token' => 'c6d8a84c0bc2300815f94ef489c5e4e53bf65614',
        ],
    ],
    'rules'     => [
        '/city' => 'city/city/index',
        '/city/<slug:[\w_\/-]+>' => 'city/city/view',
    ],
];