<?php 
	/*
		* Вывод Услуг на главной странице по городу
	*/
 ?>
<?php if ($dataProvider->itemCount): ?>
	<?php if($page): ?>
		<div class="services-home">
			<div class="content">
				<div class="box-style">
					<div class="box-style__header">
						<div class="box-style__heading">
							<?= $page->title_short; ?>
						</div>
					</div>
				</div>
				
				<div class="services-box services-box-carousel carousel-arrow-t slick-slider">
					<?php foreach ($dataProvider->getData() as $key => $data) : ?>
						<div>
							<?php $this->render('../../service/_item', ['data' => $data]) ?>
						</div>
					<?php endforeach; ?>
				</div>

				<div class="services-home__but fl fl-ju-co-c">
					<div class="but-shadow">
						<a class="but but-animate-transform" href="<?= Yii::app()->createUrl('page/page/view', ['slug' => $page->parentPage->slug]) ?>">Смотреть все услуги</a>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>