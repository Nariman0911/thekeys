<?php 
	/*
		* Вывод Услуг на главной странице по городу
	*/
 ?>
<?php if ($dataProvider->itemCount): ?>
	<div class="footer__item footer__item_services">
		<div class="footer-heading"><span>Услуги</span></div>
	    <ul class="menu-footer footer-services-menu">
	    	<?php foreach ($dataProvider->getData() as $key => $data) : ?>
	            <li>
	            	<a href="<?= Yii::app()->createUrl('service/service/view', ['slug' => $data->slug]) ?>"><?= $data->name_short; ?></a>
	            </li>
	    	<?php endforeach; ?>
	    </ul>
	</div>
<?php endif; ?>