<?php
/* @var $model Carbrands */
/* @var $this CarbrandsController */

$this->title = $model->meta_title ?: $model->name;
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = array_merge(
    ['Услуги' => ['/services']],
    [$model->name]
);
?>

<div class="page-content page-content-services">
    <?php if($model->back) : ?>
		<div class="page-header-main" style="background: url(<?= $model->getImageNewUrl(0, 0, false, null, 'back'); ?>) center no-repeat; ">
	<?php else : ?>
		<div class="page-header-main">
	<?php endif; ?>
		<div class="content">
			<?php $this->widget('application.components.MyTbBreadcrumbs', [
		        'links' => $this->breadcrumbs,
		    ]); ?>
			<h1><?= $model->getTitle(); ?></h1>
		</div>
	</div>
	<div class="content">
		<?= $model->description; ?>
    </div>
</div>

<div class="content-home">	
	<?php /* Отызвы клиентов */ ?>
	<?php $this->widget('application.modules.review.widgets.ReviewNewWidget', [
		'page_id' => 6,
		'view' => 'reviews-home'
	]); ?>

	<?php $this->widget('application.modules.carbrands.widgets.CarBrandsWidget', [
		'view' => 'carbrands-home'
	]); ?>

	<?php /* Остались вопросы */ ?>
	<?php $this->widget('application.modules.page.widgets.PagesCityWidget', [
		'page_id' => 21,
		'view' => 'question-home'
	]); ?>
</div>