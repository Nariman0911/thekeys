<div class="services-box__item fl">
	<div class="services-box__info fl fl-di-c fl-ju-co-sp-b">
		<div>
			<div class="services-box__name">
				<a href="<?= Yii::app()->createUrl('service/service/view', ['slug' => $data->slug]) ?>"><?= $data->getWrapTitle($data->name_short); ?></a>
			</div>
			<div class="services-box__desc t-stl">
				<?= $data->description_short; ?>
			</div>
		</div>
		<div class="services-box__price">
			<?= $data->price_prefix; ?> <?= $data->price; ?> <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/icon-ruble.svg'); ?>
		</div>
	</div>
	<div class="services-box__img fl fl-al-it-c">
		<picture>
            <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(0, 0, false, null, 'image'); ?>" type="image/webp">
			<source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(0, 0, false, null, 'image'); ?>">

            <img src="<?= $data->getImageNewUrl(0, 0, false, null, 'image'); ?>" alt="">
        </picture>
	</div>
</div>