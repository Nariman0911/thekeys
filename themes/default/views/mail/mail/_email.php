<table style="width: 100%;">
	<tbody>
		<?php if($model['name']) : ?>
			<tr style="background: #ececec;">
				<td style="padding: 7px 10px;"><strong>Имя:</strong></td>
				<td style="padding: 7px 10px;"><?= $model['name']; ?></td>
			</tr>
		<?php endif; ?>
		
		<?php if($model['phone']) : ?>
			<tr>
				<td style="padding: 7px 10px;"><strong>Телефон:</strong></td>
				<td style="padding: 7px 10px;"><?= $model['phone']; ?></td>
			</tr>
		<?php endif; ?>
		
		<?php if($model['email']) : ?>
			<tr>
				<td style="padding: 7px 10px;"><strong>E-mail:</strong></td>
				<td style="padding: 7px 10px;"><?= $model['email']; ?></td>
			</tr>
		<?php endif; ?>
		
		<?php if($model['body']) : ?>
			<tr style="background: #ececec;">
				<td style="padding: 7px 10px;"><strong>Сообщение:</strong></td>
				<td style="padding: 7px 10px;"><?= $model['body']; ?></td>
			</tr>
		<?php endif; ?>
		
	</tbody>
</table>
