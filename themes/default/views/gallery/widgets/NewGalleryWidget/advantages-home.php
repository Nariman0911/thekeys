<?php if($models) : ?>
	<?php 
		$childGal = [];
		foreach ($models->getChildCityGallery() as $key => $item) {
			$childGal = $item->images(['order' => 'position ASC']);
		}
	 ?>
	<?php $images = ($childGal) ?: $models->images(['order' => 'position ASC']); ?>
<?php endif; ?>

<?php if($images) : ?>
	<div class="advantages-home">
		<div class="content">
			<div class="advantages-box fl fl-wr-w">
				<?php foreach ($images as $key => $data): ?>
					<div class="advantages-box__item">
						<div class="advantages-box__img">
							<picture>
				                <source srcset="<?= $data->getImageUrlWebp(0, 0, false,null, 'file'); ?>" type="image/webp">
				                <img src="<?= $data->getImageNewUrl(0, 0, false,null, 'file'); ?>" alt="">
				            </picture>
						</div>
						<div class="advantages-box__info">
							<div class="advantages-box__name t-stl">
								<?= $data->name; ?>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
<?php endif; ?>