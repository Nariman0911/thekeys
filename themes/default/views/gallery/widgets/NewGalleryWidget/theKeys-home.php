<?php if($models->images) : ?>
	<div class="theKeys-home">
		<!-- <div class="content"> -->
			<div class="theKeys-box theKeys-box-carousel slick-slider">
				<?php foreach ($models->images(['order' => 'position ASC']) as $key => $data): ?>
					<div class="theKeys-box__item">
						<?php if($data->description): ?>
							<a href="<?= $data->description; ?>">
						<?php endif; ?>

						<picture>
			                <source srcset="<?= $data->getImageUrlWebp(0, 0, false,null, 'file'); ?>" type="image/webp">
			                <img src="<?= $data->getImageNewUrl(0, 0, false,null, 'file'); ?>" alt="">
			            </picture>
			            
			            <?php if($data->alt): ?>
				            <div class="theKeys-box__name">
				            	<?= $data->alt; ?> <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/icon-ruble.svg'); ?>
				            </div>
						<?php endif; ?>

						<?php if($data->description): ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endforeach ?>
			</div>
		<!-- </div> -->
	</div>
<?php endif; ?>