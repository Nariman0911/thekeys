<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>
    
    <link rel="preconnect" href="https://mc.yandex.ru" />
    <link rel="preconnect" href="https://connect.facebook.net" />
    <link rel="preconnect" href="https://www.googletagmanager.com" />
    <link rel="preconnect" href="https://www.googleadservices.com" />
    <link rel="preconnect" href="https://www.google-analytics.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link rel="preconnect" href="https://www.gstatic.com" />
    <link rel="preconnect" href="https://www.google.com" />
    <link rel="preconnect" href="https://stackpath.bootstrapcdn.com" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php else : ?>
        <link rel="canonical" href="<?= Yii::app()->request->hostInfo . '/' . Yii::app()->request->pathInfo; ?>">
    <?php endif; ?>

    <?php
    
    if(Yii::app()->request->queryString) {
        Yii::app()->clientScript->registerMetaTag('noindex, nofollow', 'robots');
    } else {
        Yii::app()->clientScript->registerMetaTag('index, follow', 'robots');
    }
    
    $indexCss = $this->mainAssets . "/css/index.css";
    $indexCss = $indexCss . "?v-" . filectime(Yii::getPathOfAlias('public') . $indexCss);
    Yii::app()->getClientScript()->registerCssFile($indexCss);

    $mainJs = $this->mainAssets . "/js/main.js";
    $mainJs = $mainJs . "?v-" . filectime(Yii::getPathOfAlias('public') . $mainJs);
    Yii::app()->getClientScript()->registerScriptFile($mainJs, CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('https://use.fontawesome.com/289bda1ff9.js', CClientScript::POS_END);
    
    // Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js');
    // Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.easing.min.js');
    Yii::app()->clientScript->registerMetaTag('telephone=no', 'format-detection');

    /* 
     * Шрифты 
    */
    // Yii::app()->getClientScript()->registerLinkTag('preload stylesheet', 'text/css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', NULL, ['as'=> 'style']);
    Yii::app()->getClientScript()->registerLinkTag('preload stylesheet', 'text/css', 'https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap', NULL, ['as'=> 'style']);
    Yii::app()->getClientScript()->registerLinkTag('preload stylesheet', 'text/css', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap', NULL, ['as'=> 'style']);

    ?>
    <script type="text/javascript">
        var yupeTokenName = "<?= Yii::app()->getRequest()->csrfTokenName;?>";
        var yupeToken = "<?= Yii::app()->getRequest()->getCsrfToken();?>";
        var yupeCartDeleteProductUrl = "<?= Yii::app()->createUrl('/cart/cart/delete/')?>";
        var yupeCartDeleteProductUrlNew = "<?= Yii::app()->createUrl('/cart/cart/deleteNew/')?>";
        var yupeCartUpdateUrl = "<?= Yii::app()->createUrl('/cart/cart/update/')?>";
        var yupeCartUpdateUrlNew = "<?= Yii::app()->createUrl('/cart/cart/updateNew/')?>";
        var yupeCartАddProductUrlNew = "<?= Yii::app()->createUrl('/cart/cart/addNew/')?>";
        var yupeCartWidgetUrl = "<?= Yii::app()->createUrl('/cart/cart/widget/')?>";
        var phoneMaskTemplate = "<?= Yii::app()->getModule('user')->phoneMask; ?>";
    </script>

    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
        'id' => 5
    ]); ?>


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- <link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script> -->
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>

<div class="wrapper">
    <div class="wrap1">
        
        <?php $this->renderPartial('//layouts/_header', [
            'city' => Yii::app()->controller->geoCity
        ]); ?>

        <?= $this->decodeWidgets($content); ?>
    </div>
    <div class="wrap2">
        <?php $this->renderPartial('//layouts/_footer', [
            'city' => Yii::app()->controller->geoCity
        ]); ?>
    </div>
</div>

<div class="mobileNav">
    <div class="mobileNav__icon-close"><div></div></div>
    <div class="mobileNav-box">
        <div class="mobileNav-box-2">
            <ul class="menu-mobile">
                <div class="mobileNav-box__btn">
                    <a class="but but-buy but-animate-transform" href="<?= Yii::app()->createUrl('/store/product/index'); ?>">Купить ключ</a>
                </div>
            </ul>
        </div>
    </div>
</div>

<div class="menu-fix">
    <div class="menu-fix__icon-close"><div></div></div>
    <div class="menu-fix__box">
        <div class="menu-fix__box2">
            <div class="menu-fix__box3">
                <ul></ul>
            </div>
        </div>
    </div>
</div>

<?php $fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox', [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
); ?>

<div id="messageModal" class="modal modal-my modal-my-xs fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Уведомление
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="message-success">
                    Ваша заявка успешно отправлена!
                </div>
            </div>
        </div>
    </div>
</div>

<div id="reviewsModal" class="js-reviewsModal modal modal-my modal-my-sm fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
            </div>
            <div class="modal-body">
                <div class="reviews-box reviews-box-modal">
                </div>
            </div>
        </div>
    </div>
</div>

<?php //Список городов ?>
<?php $this->widget('application.modules.city.widgets.CityGeoLocationWidget'); ?>

<?php //Модалка для поиска ?>
<?php /*$this->widget('application.modules.store.widgets.SearchProductWidget', [
    'view' => 'search-product-form-modal'
]);*/ ?>

 <!-- Заказать звонок -->
<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
    'id' => 'requestCallModal',
    'formClassName' => 'StandartForm',
    'buttonModal' => false,
    'titleModal' => 'Остались вопросы?',
    'showCloseButton' => false,
    'isRefresh' => true,
    'eventCode' => 'zakazat-zvonok',
    'successKey' => 'zakazat-zvonok',
    'modalHtmlOptions' => [
        'class' => 'modal-my',
    ],
    'formOptions' => [
        'htmlOptions' => [
            'class' => 'form-my',
        ]
    ],
    'modelAttributes' => [
        'theme' => 'Заказать звонок',
    ],
]) ?>

<!-- Вызвать мастера -->
<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
    'id' => 'callTheMasterModal',
    'formClassName' => 'StandartForm',
    'buttonModal' => false,
    'titleModal' => 'Вызвать мастера?',
    // 'subTitleModal' => 'Оставьте заявку ',
    'showCloseButton' => false,
    'isRefresh' => true,
    'eventCode' => 'zakazat-zvonok',
    'successKey' => 'zakazat-zvonok',
    'modalHtmlOptions' => [
        'class' => 'modal-my',
    ],
    'formOptions' => [
        'htmlOptions' => [
            'class' => 'form-my',
        ]
    ],
    'modelAttributes' => [
        'theme' => 'Вызвать мастера',
    ],
]) ?>

<!-- Вызвать мастера -->
<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
    'id' => 'questionModal',
    'formClassName' => 'StandartForm',
    'buttonModal' => false,
    'titleModal' => 'Задать вопрос',
    // 'subTitleModal' => 'Оставьте заявку ',
    'showCloseButton' => false,
    'isRefresh' => true,
    'eventCode' => 'zakazat-zvonok',
    'successKey' => 'zakazat-zvonok',
    'modalHtmlOptions' => [
        'class' => 'modal-my',
    ],
    'formOptions' => [
        'htmlOptions' => [
            'class' => 'form-my',
        ]
    ],
    'modelAttributes' => [
        'theme' => 'Задать вопрос',
    ],
]) ?>

<?php $this->widget('application.modules.order.widgets.BuyOneClickWidget'); ?>

<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
        'id' => 6
    ]); ?>
<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

<div class='notifications top-right' id="notifications"></div>
<div class="ajax-loading"></div>

<div class="preloader">
    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>

</body>
</html>
