<header>
    <div class="header-top">
        <div class="content">
            <div class="header-contact fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
                <div class="header-contact__item header-contact__mode">
                    <i class="icon icon-hours"></i>
                    <span><?= $city->mode; ?></span>
                </div>
                <div class="header-contact__item header-contact__city">
                    <!-- Оренбург, Шарлыкское шоссе 1/2 -->
                    <div class="header-contact__geo header-contact-geo">
                        <div class="header-contact-geo__city fl">
                            <a data-toggle="modal" data-target="#citymodal" href="#" class="city-link js-city">
                                <span data-city="<?= $city->name; ?>">Мастерская в г. <?= $city->name; ?></span>
                                <?php if(!$city->address) : ?>
                                    <i class="fa fa-angle-down"></i>
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="city-interface fl">
                            <?php if($city->address) : ?>
                                <div class="header-contact-geo__address fl">
                                    <a data-toggle="modal" data-target="#citymodal" href="#" class="city-link js-city">
                                        <span data-city="<?= $city->name; ?>"><?= $city->address; ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <div class="poll js-poll" style="display: none;">
                                <div class="poll-box">
                                    <div class="poll-text">Выбран ваш город?</div>
                                    <button class="btn poll-yes js-poll-yes">Да</button>
                                    <button class="btn poll-no js-poll-no">Нет</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-contact__item header-contact__email">
                    <div class="header-contact__heading">Всегда на связи:</div>
                    <?= $city->email; ?>
                </div>
                <div class="header-contact__item header-contact__phone">
                    <?= $city->phone; ?>
                </div>
                <div class="header-contact__item header-contact__soc">
                    <div class="soc-box fl fl-ju-co-fl-e">
                        <?php if($city->whatsapp) : ?>
                            <a href="https://wa.me/<?= $city->whatsapp; ?>"><i class="icon icon-whatsapp"></i></a>
                        <?php endif; ?>
                        <?php if($city->viber) : ?>
                            <a href="viber://chat?number=<?= $city->viber; ?>"><i class="icon icon-viber"></i></a>
                        <?php endif; ?>
                        <?php if($city->telegram) : ?>
                            <a href="https://telegram.im/<?= $city->telegram; ?>"><i class="icon icon-telegram"></i></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="content fl fl-wr-w fl-al-it-c">
            <div class="header__logo">
                <a href="/">
                    <?= CHtml::image($this->mainAssets . '/images/logo-mobile.svg', ''); ?>
                </a>
            </div>
            <div class="header__section">
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'main', 
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div> 
    <div class="header-fix">
        <div class="content fl fl-al-it-c">
            <div class="header-fix__logo">
                <a href="/">
                    <?= CHtml::image($this->mainAssets . '/images/logo.svg', ''); ?>
                </a>
            </div>
            <div class="header-fix__section fl fl-al-it-c">
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'main', 
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
                <div class="header-fix__contact header-fix-contact fl fl-al-it-c">
                    <div class="header-fix-contact__item header-fix-contact__phone">
                        <?= $city->phone; ?>
                    </div>
                    <div class="header-fix-contact__item header-fix-contact__soc">
                        <div class="soc-box fl fl-ju-co-fl-e">
                            <?php if($city->whatsapp) : ?>
                                <a href="https://wa.me/<?= $city->whatsapp; ?>"><i class="icon icon-whatsapp"></i></a>
                            <?php endif; ?>
                            <?php if($city->viber) : ?>
                                <a href="viber://chat?number=<?= $city->viber; ?>"><i class="icon icon-viber"></i></a>
                            <?php endif; ?>
                            <?php if($city->telegram) : ?>
                                <a href="https://telegram.im/<?= $city->telegram; ?>"><i class="icon icon-telegram"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                                        
</header>