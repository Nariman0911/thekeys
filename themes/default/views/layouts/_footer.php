<footer>
    <div class="footer">
        <div class="content fl fl-wr-w">
            <?php $this->widget('application.modules.service.widgets.ServicesWidget', [
                'page_id' => 2,
                'view' => 'menu-footer-services'
            ]); ?>
            <div class="footer__item footer__item_menu">
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'footermenu', 
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="footer__item footer__item_contact">
                <div class="footer-contact">
                    <div class="footer-contact__phone">
                        <?= $city->phone; ?>
                    </div>
                    <div class="footer-contact__email">
                        <?= $city->email; ?>
                    </div>
                    <div class="footer-contact__location">
                        <?= $city->getAddress(); ?>
                    </div>
                    <div class="footer-contact__soc">
                        <div class="soc-box fl fl-ju-co-fl-s">
                            <?php if($city->whatsapp) : ?>
                                <a href="https://wa.me/<?= $city->whatsapp; ?>"><i class="icon icon-whatsapp"></i></a>
                            <?php endif; ?>
                            <?php if($city->viber) : ?>
                                <a href="viber://chat?number=<?= $city->viber; ?>"><i class="icon icon-viber"></i></a>
                            <?php endif; ?>
                            <?php if($city->telegram) : ?>
                                <a href="<?= $city->telegram; ?>"><i class="icon icon-telegram"></i></a>
                            <?php endif; ?>
                            <?php /*if($city->vk) : ?>
                                <a href="<?= $city->vk; ?>"><i class="icon icon-vk"></i></a>
                            <?php endif; ?>
                            <?php if($city->instagram) : ?>
                                <a href="<?= $city->instagram; ?>"><i class="icon icon-instagram"></i></a>
                            <?php endif; ?>
                            <?php if($city->ok) : ?>
                                <a href="<?= $city->ok; ?>"><i class="icon icon-ok"></i></a>
                            <?php endif; ?>
                            <?php if($city->facebook) : ?>
                                <a href="<?= $city->facebook; ?>"><i class="icon icon-facebook"></i></a>
                            <?php endif;*/ ?>
                        </div>
                    </div>
                    <div class="footer-contact__menu">
                        <?php if(Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                                'view' => 'footermenu-2', 
                                'name' => 'bottom-menu'
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="footer-bottom">
            <div class="footer__copy">
                &copy; <?= date("Y"); ?> 
                <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                    'id' => 7
                ]); ?>
            </div>
        </div>
    </div>
</footer>