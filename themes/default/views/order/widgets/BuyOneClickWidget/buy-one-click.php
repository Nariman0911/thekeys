<div id="buyOneClickModal" class="modal modal-my modal-my-sm buyOneClickModal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Заказать
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <?php $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    [
                        // 'action' => ['/order/order/create'],
                        'id' => 'order-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => false,
                            'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
                            'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
                        ],
                        'htmlOptions' => [
                            'hideErrorMessage' => false,
                            'class' => 'order-form form-my',
                            'data-type' => "ajax-form"
                        ]
                    ]
                );
                ?>               
                <div class="buyOneClick-section">
                    <div class="buyOneClick-section__product buyOneClick-product js-addCartModal-body"></div>

                    <div class="buyOneClick-section__form js-buyOneClick-form">
                        <?php if (Yii::app()->user->hasFlash('buy-one-click-success')): ?>
                            <div class="modal-success-message">
                                <?= Yii::app()->user->getFlash('buy-one-click-success') ?>
                            </div>
                            <script>
                                $('.js-addCartModal-body').hide();
                                $('.js-addCartModal-form').hide();

                                setTimeout(function(){
                                    $('#buyOneClickModal').modal('hide');
                                    $('.js-addCartModal-body').show();
                                    $('.js-addCartModal-form').show();
                                    $('.js-buyOneClick-form .modal-success-message').hide();
                                }, 7000);
                            </script>
                        <?php endif ?>
                        <div class="js-addCartModal-form">
                            <div class="buyOneClick-section__header">Личные данные</div>
                            <div class="buyOneClick-section__desc">Оставьте свои контактные данные для уточнения деталей по заказу</div>
                            <?= $form->textFieldGroup($order, 'name'); ?>

                            <div class="form-group form-group-filled <?= $order->hasErrors('phone') ? 'has-error' : ''; ?>">
                                <?= $form->telField($order, 'phone', [
                                    'autocomplete' => 'off',
                                    'class' => 'form-control phone-mask',
                                    'placeholder' => 'Телефон',
                                    'data-phoneMask' => 'phone',
                                ]); ?>
                                <?= $form->labelEx($order, 'phone') ?>
                                <?= $form->error($order, 'phone') ?>
                            </div>
                            <?php //= $form->textFieldGroup($order, 'email'); ?>
                            <?= $form->textAreaGroup($order, 'comment'); ?>

                            <code><?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 10]); ?></code>

                            <div class="buyOneClick-section-but fl fl-al-it-c">
                                <button type="submit" data-send="ajax" class="but but-orange-gradient but-animation">
                                    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 9]); ?><
                                </button>

                                <div class="terms_of_use">
                                    * Оставляя заявку Вы соглашаетесь <br>с
                                    <a target="_blank" href="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 4]); ?>">Условиями обработки персональных данных</a>
                                </div>
                            </div>

                            <code><?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 8]); ?></code>


                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>