<div class="slide-box slide-box-carousel slick-slider">
	<?php foreach ($models as $key => $slide): ?>
	    <div class="slide-box__item">
			<div class="slide-box__img">
                <picture class="js-load-img js-load-picture">
                    <source 
                    	srcset="" 
						data-img-big="<?= $slide->getImageUrlWebp(1320,400,false,null,'image', $slide->uploadPath); ?>"
						data-img-md="<?= $slide->getImageUrlWebp(1320,400,false,null,'image', $slide->uploadPath); ?>"
						data-img-sm="<?= $slide->getImageUrlWebp(640,400,false,null,'image_xs', $slide->uploadPath . '/mobile'); ?>"
						data-img-xs="<?= $slide->getImageUrlWebp(480,400,false,null,'image_xs', $slide->uploadPath . '/mobile'); ?>"
                    	type="image/webp">
                    <img 
                    	src="" 
                    	data-img-big="<?= $slide->getImageNewUrl(1320,400,false,null,'image', $slide->uploadPath); ?>"
						data-img-md="<?= $slide->getImageNewUrl(1320,400,false,null,'image', $slide->uploadPath); ?>"
						data-img-sm="<?= $slide->getImageNewUrl(640,400,false,null,'image_xs', $slide->uploadPath . '/mobile'); ?>"
						data-img-xs="<?= $slide->getImageNewUrl(480,400,false,null,'image_xs', $slide->uploadPath . '/mobile'); ?>"
                    	alt="">
                </picture>
                <?php  ?>
			</div>
	    </div>
	<?php endforeach ?>
</div>