<?php if(!empty($pages)): ?>
	<?php 
		$childPage = [];
		foreach ($pages->getChildCityPage() as $key => $item) {
			$childPage = $item;
		}
	 ?>
	<?php $citypages = ($childPage) ?: $pages; ?>
<?php endif; ?>

<?php if($citypages): ?>
	<?= $citypages->title_short; ?>
	<?= $citypages->body_short; ?>
<?php endif; ?>
