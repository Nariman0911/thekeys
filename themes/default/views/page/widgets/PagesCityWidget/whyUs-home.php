<?php if($page): ?>
	<?php $childCityPages = $page->childPages(['order' => 'childPages.order ASC']); ?>
	<?php if($childCityPages): ?>
		<div class="whyUs-home">
			<div class="content">
				<div class="box-style box-style-but">
					<div class="box-style__header fl fl-wr-w">
						<div class="box-style__heading">
							<?= $page->title_short; ?>
						</div>
						<div class="box-style__desc t-stl">
							<?= $page->body_short; ?>
						</div>
					</div>
				</div>
				<div class="whyUs-box-section js-whyUs-box">
					<div class="whyUs-box-section__item">
						<div class="whyUs-box whyUs-box-carousel carousel-arrow-t slick-slider">
							<?php foreach ($childCityPages as $key => $data) : ?>
								<div>
									<div class="whyUs-box__item fl">
										<div class="whyUs-box__info">
											<div class="whyUs-box__name"><?= $data->title_short; ?></div>
											<div class="whyUs-box__desc t-stl"><?= $data->body_short; ?></div>
										</div>
										<div class="whyUs-box__img fl fl-al-it-c">
											<picture>
									            <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(0, 0, false, null, 'image'); ?>" type="image/webp">
						            			<source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(0, 0, false, null, 'image'); ?>">

									            <img src="<?= $data->getImageNewUrl(0, 0, false, null, 'image'); ?>" alt="">
									        </picture>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="whyUs-box-section__arrows whyUs-box-arrows carousel-arrow-t carousel-arrow-t-2"></div>
				</div>

				<div class="whyUs-home__but fl">
					<div class="but-shadow">
						<a class="but but-animate-transform" data-toggle="modal" data-target="#callTheMasterModal" href="#">Вызвать мастера</a>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>
