<?php if($page): ?>
	<div class="question-home bck-stl">
		<div class="content fl fl-wr-w">
			<div class="question-home__header">
				<?= $page->body_short; ?>
			</div>
			<div class="question-home__body question-contact">
				<div class="question-contact__phone">
					<?= $city->phone; ?>
				</div>
				<div class="question-contact__email">
					<?= $city->email; ?>
				</div>
				<div class="question-contact__but fl">
					<div class="but-shadow">
						<a class="but but-white but-animate-transform" data-toggle="modal" data-target="#requestCallModal" href="#">Оставить заявку</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>