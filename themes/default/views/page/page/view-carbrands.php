<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content page-content-carbrands">
    <div class="content">
    	<?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>

		<h1><?= $model->title; ?></h1>
		<?php //= $model->body; ?>

		<?php $this->widget('application.modules.carbrands.widgets.CarBrandsWidget'); ?>
    </div>
</div>

<?php /* Остались вопросы */ ?>
<?php $this->widget('application.modules.page.widgets.PagesCityWidget', [
	'page_id' => 21,
	'view' => 'question-home'
]); ?>
