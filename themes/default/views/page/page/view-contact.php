<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content">
    <div class="content">
        <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
        ]); ?>
        <div class="contact-box">
			<div class="contact-box__info">
				<h1><?= $model->title; ?></h1>
				<?php //= $model->body; ?>

				<div class="page-contact-box">
					<?php if($city->address) : ?>
						<div class="page-contact-box__item page-contact-box__location">
							<div class="page-contact-box__header">Адрес:</div>
	                        <?= $city->getAddress(); ?>
	                    </div>
					<?php endif; ?>
					<?php if($city->phone) : ?>
						<div class="page-contact-box__item page-contact-box__phone fl fl-wr-w">
							<div class="page-contact-box__header">Телефон:</div>
							<?= $city->phone; ?>
						</div>
					<?php endif; ?>
					<?php if($city->email) : ?>
						<div class="page-contact-box__item page-contact-box__email fl fl-wr-w">
							<div class="page-contact-box__header">E-mail:</div>
							<?= $city->email; ?>
						</div>
					<?php endif; ?>
                    <div class="page-contact-box__item page-contact-box__soc">
						<div class="page-contact-box__header">Мы в социальных сетях:</div>
                        <div class="soc-box fl fl-ju-co-fl-s">
                            <?php if($city->whatsapp) : ?>
                                <a href="https://wa.me/<?= $city->whatsapp; ?>"><i class="icon icon-whatsapp"></i></a>
                            <?php endif; ?>
                            <?php if($city->viber) : ?>
                                <a href="viber://chat?number=<?= $city->viber; ?>"><i class="icon icon-viber"></i></a>
                            <?php endif; ?>
                            <?php if($city->telegram) : ?>
                                <a href="<?= $city->telegram; ?>"><i class="icon icon-telegram"></i></a>
                            <?php endif; ?>
                            <?php if($city->vk) : ?>
                                <a target="_blank" href="<?= $city->vk; ?>"><i class="icon icon-vk"></i></a>
                            <?php endif; ?>
                            <?php if($city->instagram) : ?>
                                <a target="_blank" href="<?= $city->instagram; ?>"><i class="icon icon-instagram"></i></a>
                            <?php endif; ?>
                            <?php if($city->ok) : ?>
                                <a target="_blank" href="<?= $city->ok; ?>"><i class="icon icon-ok"></i></a>
                            <?php endif; ?>
                            <?php if($city->facebook) : ?>
                                <a target="_blank" href="<?= $city->facebook; ?>"><i class="icon icon-facebook"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>
				</div>

			</div>
			<div class="contact-box__map">
				<?php if($city->code_map) : ?>	
	                <iframe src="<?= $city->code_map; ?>" frameborder="0"></iframe>
            	<?php endif; ?>
			</div>
        </div>
	</div>
</div>

<?php /* Остались вопросы */ ?>
<?php $this->widget('application.modules.page.widgets.PagesCityWidget', [
	'page_id' => 21,
	'view' => 'question-home'
]); ?>
