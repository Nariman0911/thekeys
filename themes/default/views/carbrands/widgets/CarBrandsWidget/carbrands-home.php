<?php if($model) : ?>
	<div class="carBrand-home">
		<div class="content">
			<div class="box-style">
				<div class="box-style__header">
					<div class="box-style__heading">
						<?= Yii::app()->getModule('carbrands')->titleHome; ?>
					</div>
				</div>
			</div>
			<div class="carBrand-box carBrand-box-carousel carousel-arrow-t slick-slider">
				<?php foreach ($model as $key => $data) : ?>
					<div>
						<div class="carBrand-box__item">
							<div class="carBrand-box__img fl fl-al-it-c fl-ju-co-c">
								<a class="fl fl-al-it-c fl-ju-co-c"  href="<?= Yii::app()->createUrl('carbrands/carbrands/view', ['slug' => $data->slug]); ?>">
									<picture class="fl fl-al-it-c fl-ju-co-c" >
										<source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(128, 86, false, null,'image'); ?>" type="image/webp">
										<source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(128, 86, false, null,'image'); ?>" >

										<source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(107, 60, false, null,'image'); ?>" type="image/webp">
										<source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(107, 60, false, null,'image'); ?>" >

						                <img src="<?= $data->getImageNewUrl(0, 0, false,null, 'image'); ?>" alt="">
						            </picture>
					            </a>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
<?php endif; ?>