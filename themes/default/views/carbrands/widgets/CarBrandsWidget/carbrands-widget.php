<?php if($model) : ?>
	<div class="carBrand-box carBrand-page-box fl fl-wr-w">
		<?php foreach ($model as $key => $data) : ?>
			<div class="carBrand-box__item">
				<div class="carBrand-box__img fl fl-al-it-c fl-ju-co-c">
					<a class="fl fl-al-it-c fl-ju-co-c" href="<?= Yii::app()->createUrl('carbrands/carbrands/view', ['slug' => $data->slug]); ?>">
						<picture class="fl fl-al-it-c fl-ju-co-c" >
			                <source srcset="<?= $data->getImageUrlWebp(0, 0, false,null, 'image'); ?>" type="image/webp">
			                <img src="<?= $data->getImageNewUrl(0, 0, false,null, 'image'); ?>" alt="">
			            </picture>
		            </a>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php endif; ?>