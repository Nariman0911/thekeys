<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}


$this->title = $page->meta_title ?: $page->title;
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];

?>

<div class="content-home">
	<div class="content">
		<div class="main-screen fl fl-wr-w">
			<div class="main-screen__info">
				<h1 class="main-screen__h1"><?= $page->getTitle(); ?><i class="main-screen__lock icon icon-lock"></i></h1>
				<div class="main-screen__desc">
					<div class="hidden-sm"><?= $page->body_short; ?></div>
					<div class="hidden-xs"><?= $page->body; ?></div>
				</div>
				<div class="main-screen__but fl">
					<div class="but-shadow">
						<?php if(Yii::app()->controller->geoCity->phone) : ?>
							<?php 
								$phone = Yii::app()->controller->geoCity->phone;
								$pattern = '/^<a.*?href=(["\'])(.*?)\1.*$/';
								// $pattern = '/<a.*href="(.*)".*>/is';
								preg_match($pattern, $phone, $href);
 							?>
 							<a class="hidden-sm but but-animate-transform main-screen__phone" href="<?= $href[2]; ?>">Позвонить</a>
							<a class="hidden-xs but but-animate-transform" data-toggle="modal" data-target="#requestCallModal" href="#">Оставить заявку</a>
						<?php else : ?>
							<a class="but but-animate-transform" data-toggle="modal" data-target="#requestCallModal" href="#">Оставить заявку</a>
						<?php endif; ?>
					</div>
					<div class="but-shadow">
						<a class="but but-buy bt-animation2" href="<?= Yii::app()->createUrl('/store/product/index'); ?>">Купить ключ</a>
					</div>
				</div>
			</div>
			<div class="main-screen__media">
				<?php $photos = $page->photos(['order' => 'photos.position DESC']); ?>
				<?php if($photos) : ?>
					<div class="main-screen-carousel slick-slider">
						<?php foreach ($photos as $key => $data) : ?>
							<div class="main-screen-carousel__item">
								<picture>
									<source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(600, 400, false, null,'image'); ?>" type="image/webp">
									<source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(600, 400, false, null,'image'); ?>" >
									<source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(360, 240, false, null,'image'); ?>" type="image/webp">
									<source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(360, 240, false, null,'image'); ?>" >

									<img src="<?= $data->getImageNewUrl(600, 400, false, null,'image'); ?>" alt="" />
								</picture>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>

	<?php /* Галерея ключей */ ?>
	<?php $this->widget('application.modules.gallery.widgets.NewGalleryWidget', [
		'id' => 2,
		'view' => 'theKeys-home'
	]); ?>
	
	<?php /* Преимущества */ ?>
	<?php $this->widget('application.modules.gallery.widgets.NewGalleryWidget', [
		'id' => 3,
		'view' => 'advantages-home'
	]); ?>


	<?php /* Услуги и цены */ ?>
	<?php $this->widget('application.modules.service.widgets.ServicesWidget', [
		'page_id' => 2,
	]); ?>

	<?php /* Отызвы клиентов */ ?>
	<?php $this->widget('application.modules.review.widgets.ReviewNewWidget', [
		'page_id' => 6,
		'view' => 'reviews-home'
	]); ?>
	
	<?php /* Почему мы */ ?>
	<?php $this->widget('application.modules.page.widgets.PagesCityWidget', [
		'page_id' => 17,
		'view' => 'whyUs-home'
	]); ?>

	<div class="calculation-home bck-stl hidden">
		<div class="content">
			<div class="box-style">
				<div class="box-style__header fl fl-wr-w">
					<div class="box-style__heading">
						Расчет времени <br>прибытия мастера
					</div>
				</div>
			</div>

			<div class="calc-box fl fl-wr-w">
				<div class="calc-box__item">
					<div class="calc-box__heading">Выберите нужную услугу</div>
					<div class="calc-services fl fl-wr-w">
						<div class="calc-services__item js-calc-services fl fl-di-c fl-al-it-c fl-ju-co-c">
							<div class="calc-services__icon">
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/calc/1.svg'); ?>
							</div>
							<div class="calc-services__name">Вскрыть замок</div>
						</div>
						<div class="calc-services__item js-calc-services fl fl-di-c fl-al-it-c fl-ju-co-c">
							<div class="calc-services__icon">
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/calc/2.svg'); ?>
							</div>
							<div class="calc-services__name">Вскрыть дверь</div>
						</div>
						<div class="calc-services__item js-calc-services fl fl-di-c fl-al-it-c fl-ju-co-c">
							<div class="calc-services__icon">
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/calc/3.svg'); ?>
							</div>
							<div class="calc-services__name">Вскрыть сейф</div>
						</div>
						<div class="calc-services__item js-calc-services fl fl-di-c fl-al-it-c fl-ju-co-c">
							<div class="calc-services__icon">
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/calc/4.svg'); ?>
							</div>
							<div class="calc-services__name">Вскрыть машину</div>
						</div>
					</div>
				</div>
				<div class="calc-box__item">
					<div class="calc-box__heading">Выберите ваш район</div>

					<div class="wrapper-dropdown js-wrapper-dropdown">
						<div class="wrapper-dropdown__header js-wrapper-dropdown-header fl fl-al-it-c">
							<span>Оренбург</span>
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/icon-arrow-wrapperdrop.svg'); ?>
						</div>
						<div class="wrapper-dropdown__body">
							<div class="wrapper-dropdown__content calc-city-list">
								<div class="calc-city-list__item js-calc-city">Оренбург</div>
								<div class="calc-city-list__item js-calc-city">Санкт-Петербург</div>
								<div class="calc-city-list__item js-calc-city">Москва</div>
								<div class="calc-city-list__item js-calc-city">Самара</div>
								<div class="calc-city-list__item js-calc-city">Екатеринбург</div>
								<div class="calc-city-list__item js-calc-city">Казань</div>
								<div class="calc-city-list__item js-calc-city">Краснодар</div>
							</div>
						</div>
					</div>

					<div class="city-district fl fl-wr-w">
						<div class="city-district__item js-calc-district">Центральный</div>
						<div class="city-district__item js-calc-district">Промышленный</div>
						<div class="city-district__item js-calc-district">Ленинский</div>
						<div class="city-district__item js-calc-district">Южный</div>
						<div class="city-district__item js-calc-district">Дзержинский</div>
					</div>
				</div>
				<div class="calc-box__item">
					<div class="calc-box__heading">Время прибытия</div>
					<div class="calc-result fl fl-di-c fl-al-it-c fl-ju-co-c">
						<div class="calc-result-time fl">
							<div class="calc-result-time__day fl fl-di-c fl-al-it-c">
								<span>00</span>
								Часы
							</div>
							<span class="span_hr">:</span>
							<div class="calc-result-time__minutes fl fl-di-c fl-al-it-c">
								<span>00</span>
								Минуты
							</div>
						</div>
						<div class="calc-result__but fl ju-co-c">
							<div class="but-shadow">
								<a class="but but-animate-transform" data-toggle="modal" data-target="#requestCallModal" href="#">Заполнить заявку онлайн</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->widget('application.modules.carbrands.widgets.CarBrandsWidget', [
		'view' => 'carbrands-home'
	]); ?>

	<?php /* Остались вопросы */ ?>
	<?php $this->widget('application.modules.page.widgets.PagesCityWidget', [
		'page_id' => 21,
		'view' => 'question-home'
	]); ?>

</div>
