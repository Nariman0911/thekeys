<?php if($models) : ?>
    <div class="news-home">
        <div class="content">
            <div class="box-style">
                <div class="box-style__header">
                    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                        'id' => 7
                    ]); ?>
                    <div class="box-style__but">
                        <a class="but-link-svg but-link-svg-red" href="<?= Yii::app()->createUrl('/news/news/index'); ?>">
                            <span>Все новости</span>
                            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right-category.svg'); ?>
                        </a>
                    </div>
                </div>
                <div class="box-style__content">
                    <div class="news-box fl fl-wr-w">
                        <?php foreach ($models as $key => $data) : ?>
                            <div class="news-box__item news-box__item_<?= $key; ?>">
                                <div class="news-box__img">
                                    <?php if ($data->image): ?>
                                        <?= CHtml::image($data->getImageUrl(), $data->title); ?>
                                    <?php else : ?>
                                        <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.jpg',''); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="news-box__info">
                                    <div class="news-box__date"><?= date("d.m.Y", strtotime($data->date)); ?></div>
                                    <a class="but-link-svg" href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
                                        <div class="news-box__name"><?= $data->title; ?></div>
                                    </a>
                                    <div class="news-box__but">
                                        <a class="but-link-svg" href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
                                            <span>Читать новость</span>
                                            <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right-category.svg'); ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>