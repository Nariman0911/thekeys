<?php $domain = Yii::app()->params['url']; ?>

<div class="modal fade" id="citymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header box-style">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Выберите ваш город
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <ul class="city-list">
                    <?php foreach ($model as $key => $item) : ?>
                        <?php if($item->slug == Yii::app()->getModule('city')->defaultCity){
                            $url = $_SERVER['REQUEST_SCHEME'] . '://' . $domain . $_SERVER['REQUEST_URI'];
                        }else{
                            $url = $_SERVER['REQUEST_SCHEME'] . '://'. $item->slug . '.' . $domain . $_SERVER['REQUEST_URI'];
                        } ?>
                        <li class="<?= ($item->slug == Yii::app()->controller->citySlug) ? 'active' : ''; ?>" >
                            <a class="js-city-link" href="<?= $url; ?>">
                                <?= $item->name; ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
</div>
