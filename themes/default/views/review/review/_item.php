<div class="reviews-box__item  js-reviews-item">
    <div class="reviews-box__header fl fl-wr-w fl-al-it-c">
        <div class="reviews-box__img fl fl-wr-w fl-al-it-c fl-ju-co-c">
            <picture>
                <source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(65, 65, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>" type="image/webp">
                <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(65, 65, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>">
                
                <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(56, 56, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>" type="image/webp">
                <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(56, 56, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>">

                <img src="<?= $data->getImageNewUrl(65, 65, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>" alt="">
            </picture>
        </div>
        <div class="reviews-box__info">
            <div class="reviews-box__name t-stl">
                <div>
                    <span><?= CHtml::encode($data->username); ?></span>
                    <?php if($data->links) : ?>
                        <a class="but-link" target="_blank" rel="nofollow" href="<?= $data->links ?>">[Подробнее об отзыве]</a>
                    <?php endif; ?>
                </div>
                <?= CHtml::encode( $data->short_desc); ?>
            </div>
        </div>
    </div>
    <div class="reviews-box__description js-reviews-desc">
        <div class="reviews-box__text js-reviews-text">
            <?= $data->text; ?>
        </div>
    </div>
    <?php if($is_more) : ?>
        <div class="reviews-box__more js-reviews-more">
            <a class="reviews-box__link js-reviews-link" href="#">
                <span>Читать весь отзыв</span>
            </a>
        </div>
    <?php endif; ?>

    <?php if($data->audioreview) : ?>
        <div class="review-box__play">
            <div class="review-box-play fl fl-al-it-c js-review-audio-play" data-src="<?= $data->getPathAudio(); ?>" >
                <div class="review-box-play__icon review-audio-play fl fl-al-it-c fl-ju-co-c">
                    <i class="icon-play"></i>
                </div>
                <div class="review-box-play__text">
                    <span>Прослушать </span><span>аудиоотзыв</span>
                </div>                
            </div>
        </div>
    <?php endif; ?>
</div>
