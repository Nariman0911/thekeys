<?php 
	/*
		* Вывод Отзывов на главной странице по городу
	*/
 ?>
<?php if ($dataProvider->itemCount): ?>
	<?php if($page): ?>
		<div class="reviews-home">
			<div class="content fl fl-wr-w">
				<div class="reviews-home__header">
					<div class="box-style">
						<div class="box-style__header">
							<div class="box-style__heading">
								<?= $page->title_short; ?>
							</div>
						</div>
					</div>
					<div class="reviews-home__desc t-stl">
						<?= $page->body_short; ?>
					</div>
					<div class="reviews-home__but fl">
						<div class="but-shadow">
							<a class="but but-animate-transform" href="<?= Yii::app()->createUrl('page/page/view', ['slug' => $page->parentPage->slug]) ?>">Все отзывы</a>
						</div>
					</div>
				</div>
				<div class="reviews-home__body">
					<div class="reviews-nav fl fl-al-it-c fl-ju-co-sp-b">
						<div class="reviews-nav__counter reviews-nav-counter"></div>
						<div class="reviews-nav__arrows reviews-nav-arrows nav-arrows fl fl-al-it-c"></div>
					</div>
					<div class="reviews-box reviews-box-carousel slick-slider">
						<?php foreach ($dataProvider->getData() as $key => $data) : ?>
							<div>
								<?php $this->render('../../review/_item', [
									'data' => $data,
									'is_more' => true
								]) ?>
							</div>
						<?php endforeach; ?>
					</div>
					<audio id="review-hidden-player" loop src=""></audio>
				</div>
			</div>
			</div>	
	<?php endif; ?>
<?php endif; ?>
