<?php if($model) : ?>
	<div class="reviews-nav fl fl-al-it-c fl-ju-co-sp-b">
		<div class="reviews-nav__counter reviews-nav-counter"></div>
		<div class="reviews-nav__arrows reviews-nav-arrows nav-arrows fl fl-al-it-c"></div>
	</div>
	<div class="reviews-box reviews-box-carousel slick-slider">
		<?php foreach ($model as $key => $item) : ?>
			<div>
				<?php $this->render('../../review/_item', [
					'data' => $item,
					'is_more' => true
				]) ?>
			</div>
		<?php endforeach; ?>
	</div>
	<audio id="review-hidden-player" loop src=""></audio>
<?php endif; ?>