<?php 
/**
 * Шаблон в модальном окне, добавленного товара в корзину.
**/  
?>
<div class="cart-list js-product-item">
	<?php foreach ($positions as $position): ?>
	    <div class="cart-list__item">
	        <?php $productUrl = ProductHelper::getUrl($position->getProductModel()); ?>
	        <div class="cart-item js-cart__item js-cart-list-item fl fl-ju-co-sp-b">
	            <div class="cart-item__thumbnail">
                    <img src="<?= $position->getProductModel()->getImageUrl(90, 90, false); ?>" class="cart-item-info__img"/>
	            </div>
	            <div class="cart-item__section fl fl-ju-co-sp-b fl-al-it-c">
	                <div class="cart-item__content">
	                    <?php if ($position->getProductModel()->getCategoryId()): ?>
	                        <div class="cart-item__category"><?= $position->getProductModel(
	                            )->category->name ?></div>
	                    <?php endif; ?>
	                    <div class="cart-item__title">
	                        <a href="<?= $productUrl; ?>" class="cart-item__link"><?= CHtml::encode(
	                                $position->name
	                            ); ?></a>
	                    </div>
	                    <?php foreach ($position->selectedVariants as $variant): ?>
	                        <h6><?= $variant->attribute->title; ?>: <?= $variant->getOptionValue(); ?></h6>
	                        <?= CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant->id); ?>
	                    <?php endforeach; ?>
	                </div>
		            <div class="cart-item__price cart-price <?= ($position->hasDiscount()) ? 'cart-price-new' : ''; ?> fl fl-di-c fl-al-it-fl-e">
		            	<?php if ($position->hasDiscount()) : ?>
			                <span class="cart-price__old">
			                	<span class="strikethrough">
				                    <span class="price-old js-price-old">
				                        <?= round($position->getBasePrice(), 2); ?>
				                    </span>
				                    <span class="ruble">р.</span>
				                </span>
			                </span>
			            <?php endif; ?>
			            <span class="cart-price__res">
			                <span class="price-result js-price-result">
			                    <?= round($position->getPrice(), 2); ?>
			                </span>
			                <span class="ruble">р.</span>
			            </span>
			            <div class="cart-item__action">
		                    <a class="js-cart__delete cart-delete-product"
		                       data-position-id="<?= $positionId; ?>">
		                        <i class="fa fa-fw fa-trash-o"></i>
		                    </a>
		                </div>
		            </div>
	            </div>
	        </div>
	    </div>


	    <?php $sevices = $position->linkedProducts; ?>
    	<div class="cart-services">
	    	<?php if($sevices) : ?>
    			<div class="cart-services__header">Дополнительные услуги: </div>
	    		<div class="cart-services-list fl fl-di-c">
			    	<?php foreach ($sevices as $sevice): ?>
			    		<label class="cart-services-list__item js-service-add">
		                    <?= CHtml::checkBox('Product[servives][]', '', [
		                        'id' => 'Product_servives_'.$sevice->id,
		                        'data-position-id' => $sevice->id,
		                        'data-sum' => $sevice->getResultPrice(),
		                        'value' => $sevice->id,
		                    ]); ?>
		                    <?= $sevice->name; ?>
		                </label>
		    		<?php endforeach; ?>
	    		</div>
	    	<?php else : ?>
	    		<div class="cart-services__header">Дополнительные услуги: </div>
	    		<div class="cart-services-list fl fl-di-c">
			    	<?php foreach ($servicesList as $sevice): ?>
			    		<label class="cart-services-list__item js-service-add">
		                    <?= CHtml::checkBox('Product[servives][]', '', [
		                        'id' => 'Product_servives_'.$sevice->id,
		                        'data-position-id' => $sevice->id,
		                        'data-sum' => $sevice->getResultPrice(),
		                        'value' => $sevice->id,
		                    ]); ?>
		                    <?= $sevice->name; ?>
		                </label>
		    		<?php endforeach; ?>
	    		</div>
	    	<?php endif; ?>
    	</div>

	<?php endforeach; ?>
</div>

<div class="js-orderProduct-list">
	<?php foreach ($positions as $position): ?>
	    <div>
	        <?php $positionId = $position->getId(); ?>
	        <?= CHtml::hiddenField('OrderProduct['.$positionId.'][product_id]', $position->id, [
	        	'class' => 'js-position-product',
	        	'data-id' => $position->id
	        ]); ?>
	    </div>
	<?php endforeach; ?>
</div>
