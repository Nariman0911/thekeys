<?php foreach ($positions as $position): ?>
    <div>
        <?php $positionId = $position->getId(); ?>
        <?= CHtml::hiddenField('OrderProduct['.$positionId.'][product_id]', $position->id, [
        	'class' => 'js-position-product',
        	'data-id' => $position->id
        ]); ?>
    </div>
<?php endforeach; ?>
