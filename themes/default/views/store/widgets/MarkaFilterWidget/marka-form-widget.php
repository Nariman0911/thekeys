<form 
    class="well marka-filter-form" 
    id="marka-filter-form" 
    name="marka-filter-form" 
    action="<?= Yii::app()->createUrl('/store/product/index'); ?>" 
    method="get"
>

    <div class="row">
        <div class="col-sm-12 jsMarkaSelect">
            <label for="">Марка автомобиля: </label>
            <?php print_r(StoreMarka::model()->getFieldValue('marki', Yii::app()->getRequest()))?>
            <div class="form-group">
                <?php $this->widget('booster.widgets.TbSelect2',[
                    'asDropDownList' => true,
                    'name' => 'marki',
                    'data' => StoreMarka::model()->getMarkaList(),
                    // 'value' => StoreMarka::model()->getFieldValue('marki', Yii::app()->getRequest()),
                    'value' => Yii::app()->getRequest()->getQuery('marki', false) ? Yii::app()->getRequest()->getQuery('marki', false) : '',
                    'htmlOptions' => [
                        'class' => 'form-control',
                        'action' => Yii::app()->createUrl('/store/product/ajaxLoadMarkaModel/'),
                        'empty' => '-- выберите марку --',
                        'encode' => false              
                    ]
                ]); ?>
            </div>
        </div>
        <div class="col-sm-12 jsMarkaModelSelect <?= Yii::app()->getRequest()->getQuery('marki', false) ? '' : 'hidden'?>">
            <label for="">Модель автомобиля: </label>
            <div class="form-group">
                <?php $this->widget('booster.widgets.TbSelect2',[
                    'asDropDownList' => true,
                    'name' => 'markaModels',
                    'data' => StoreModelMarka::model()->getModelListQuery(Yii::app()->getRequest()),
                    'value' => Yii::app()->getRequest()->getQuery('markaModels', false) ? Yii::app()->getRequest()->getQuery('markaModels', false) : '',
                    'htmlOptions' => [
                        'class' => 'form-control',
                        'action' => Yii::app()->createUrl('/store/product/ajaxLoadMarkaModel/'),
                        'empty' => '-- выберите модель --',
                        'encode' => false              
                    ]
                ]); ?>
            </div>
        </div>

        <div class="col-sm-12 jsMarkaModelYearSelect <?= Yii::app()->getRequest()->getQuery('markaModels', false) ? '' : 'hidden'?>">
            <label for="">Год выпускa: </label>
            <div class="form-group">
                <?php $this->widget('booster.widgets.TbSelect2',[
                    'asDropDownList' => true,
                    'name' => 'markaModelsYears',
                    'data' => StoreModelMarkaYear::model()->getYearModelListQuery(Yii::app()->getRequest()),
                    'value' => Yii::app()->getRequest()->getQuery('markaModelsYears', false) ? Yii::app()->getRequest()->getQuery('markaModelsYears', false) : '',
                    'htmlOptions' => [
                        'class' => 'form-control',
                        'empty' => '-- выберите год --',
                        'encode' => false              
                    ]
                ]); ?>
            </div>
        </div>
    </div>

    <div class="row jsMarkaModelButton <?= Yii::app()->getRequest()->getQuery('markaModels', false) ? '' : 'hidden'?>">
        <div class="col-sm-12">
            <button class="but but-filter" type="submit">Найти ключ</button>
        </div>
    </div>

</form>

<?php Yii::app()->getClientScript()->registerScript("js-Marka-Select", "
    
"); ?>


