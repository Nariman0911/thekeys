<?php if ($this->dataProvider->itemCount) : ?>
    <div class="product-box__services product-services">
        <div class="product-services__header">Дополнительные услуги: </div>
        <!-- <ul>
            <?php foreach ($dataProvider->getData() as $data): ?>
                <li><?= $data->name; ?></li>
            <?php endforeach; ?>
        </ul> -->
        <div class="cart-services-list fl fl-di-c">
            <?php foreach ($dataProvider->getData() as $data): ?>
                <label 
                    for="ProductServives_<?= $data->id; ?>_<?= $id; ?>"
                    class="cart-services-list__item js-service-product-add" 
                >
                    <?= CHtml::checkBox('ProductServives[]', '', [
                        'id' => 'ProductServives_'.$data->id.'_'.$id,
                        'data-sum' => $data->getResultPrice(),
                        'value' => $data->id,
                    ]); ?>
                    <?= $data->name?> 
                    <span class="cart-services-list__cost">(<?= $data->getResultPrice()?> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?>)</span>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>