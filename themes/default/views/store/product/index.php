<?php

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

/* @var $category StoreCategory */

$this->title = Yii::app()->getModule('store')->metaTitle ?: Yii::t('StoreModule.store', 'Catalog');
$this->description = Yii::app()->getModule('store')->metaDescription;
$this->keywords = Yii::app()->getModule('store')->metaKeyWords;

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog")];
?>

<?php $class = Yii::app()->getRequest()->getQuery('markaModels', false) ? '' : 'hidden'; ?>

<div class="page-content">
    <div class="content">
        <h1><?= Yii::t("StoreModule.store", "Catalog"); ?></h1>

        <div class="marka-form marka-form-loading">
            <?php $this->widget('application.modules.store.widgets.MarkaFilterWidget'); ?>
        </div>

        <div class="js-listView-section">
            <?php $this->widget(
                'bootstrap.widgets.TbListView',
                [
                    'dataProvider' => $dataProvider,
                    'id' => 'product-box',
                    'itemView' => '_item',
                    'summaryText' => '',
                    'cssFile' => false,
                    'emptyText'=>'По данным параметрам нет товаров.',
                    'template'=>'{items} {pager}',
                    'sortableAttributes' => [
                        'name',
                        'price',
                    ],
                    'itemsCssClass' => "product-box product-list fl fl-wr-w",
                    'htmlOptions' => [
                        'class' => 'product-box-listView js-product-listView '.$class
                    ],
                    'ajaxUpdate'=>true,
                    'enableHistory' => false,
                     'pagerCssClass' => 'pagination-box pagination-product-box',
                        'pager' => [
                        'header' => '',
                        'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                        'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                        // 'lastPageLabel'  => false,
                        // 'firstPageLabel' => false,
                        'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                        'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                        'maxButtonCount' => 5,
                        'htmlOptions' => [
                            'class' => 'pagination'
                        ],
                    ]
                ]
            ); ?>
        </div>
    </div>
</div>

<div class="preloader-noop active" style="z-index: 99999">
    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>

<?php Yii::app()->getClientScript()->registerScript("localStorage-clear", "
    localStorage.clear();

   $(window).load(function(){
        $('.select2-drop-mask').remove();
        $('.select2-drop-active').remove();
        setTimeout(function() {
            $('.preloader-noop').removeClass('active').css('z-index', '');
        }, 1000);
    });
"); ?>