<div class="product-box__item js-product-item">
    <div class="product-box__img">
        <a href="<?= ProductHelper::getUrl($data); ?>">
            <picture class="">
                <source 
                    srcset="<?= $data->getImageUrlWebp(268, 200, false, null); ?>" 
                    type="image/webp">
                <img 
                    src="<?= $data->getImageNewUrl(268, 200, false, null); ?>" 
                    alt="<?= CHtml::encode($data->getImageAlt()); ?>"
                    title="<?= CHtml::encode($data->getImageTitle()); ?>">
            </picture>
        </a>
    </div>
    <div class="product-box__info fl fl-di-c fl-ju-co-sp-b">
        <div>
            <div class="product-box__name">
                <a class="product-box__link product-name" href="<?= ProductHelper::getUrl($data); ?>">
                    <span><?= $data->name; ?></span>
                </a>
            </div>
            <?php if($data->sku) : ?>
                <div class="product-box__sku">
                    Артикул: <span><?= $data->sku; ?></span>
                </div>
            <?php endif; ?>
            <?php /* ?>
            <div class="product-box__stock product-stock">
                <?php if($data->in_stock): ?>
                    <span class="product-stock__icon product-stock__icon-inStock"></span>
                    <span class="product-stock__name">В наличии</span>
                <?php else: ?>
                    <span class="product-stock__icon product-stock__icon-noStock"></span>
                    <span class="product-stock__name">Нет в наличии</span>
                <?php endif; ?>
            </div>
            <?php */ ?>

            <div class="product-box__price product-box__price-base product-price <?= ($data->hasDiscount()) ? 'product-price-new' : ''; ?> fl fl-wr-w fl-al-it-fl-e fl-ju-co-sp-b">
                <span class="product-price__res">
                    <span 
                        class="price-result" 
                        id="result-price<?= $data->id?>" 
                    >
                        <?= round($data->getResultPrice(), 2); ?>
                    </span>
                    <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?>
                </span>
                <span class="product-price__old">
                    <?php if ($data->hasDiscount()) : ?>
                        <span 
                            class="price-old" 
                        >
                            <?= round($data->getBasePrice(), 2); ?>
                        </span>
                        <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?>
                    <?php endif; ?>
                </span>
            </div>

            <?php $this->widget('application.modules.store.widgets.ServicesProduct', [
                'product' => $data,
            ]);?>
        </div>

        <div class="product-box__bottom">
            <div class="product-box__price js-product-price-total hidden">
                <div class="product-box__price-header">Итого: </div>
                <div class="product-price <?= ($data->hasDiscount()) ? 'product-price-new' : ''; ?> fl fl-wr-w fl-al-it-fl-e fl-ju-co-sp-b">
                    <span class="product-price__res">
                        <span 
                            class="price-result js-price-result" 
                            id="result-price<?= $data->id?>" 
                            data-sum="<?= round($data->getBasePrice(), 2); ?>"
                        >
                            <?= round($data->getResultPrice(), 2); ?>
                        </span>
                        <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?>
                    </span>
                    <span class="product-price__old">
                        <?php if ($data->hasDiscount()) : ?>
                            <span 
                                class="price-old js-price-old" 
                                data-sum="<?= round($data->getBasePrice(), 2); ?>"
                            >
                                <?= round($data->getBasePrice(), 2); ?>
                            </span>
                            <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?>
                        <?php endif; ?>
                    </span>
                </div>
            </div>
            <div class="product-box__button product-button">
                <a 
                    href="#" 
                    class="but but-orange js-product-but quick-add-product-to-cart" 
                    data-product-id="<?= $data->id; ?>" 
                    data-cart-add-url="<?= Yii::app()->createUrl('/cart/cart/add');?>">
                    <span>Заказать</span>
                </a>
            </div>
        </div>
    </div>
</div>
