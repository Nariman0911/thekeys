<?php
Yii::import('application.modules.menu.components.YMenu'); ?>
<div class='menu menu-btn-wrapper'>
    <?php $this->widget(
         'zii.widgets.CMenu',
         [
             'encodeLabel' => false,
             'items' => $this->params['items'],
             'htmlOptions' => [
                'class' => 'menu-main',
             ],
         ]
     ); ?>

    <div class="menu-btn-wrapper__btn but-shadow">
        <a class="but but-buy but-animate-transform" href="<?= Yii::app()->createUrl('/store/product/index'); ?>">Купить ключ</a>
    </div>
</div>
<div class="menu-fix-icon js-menu-mobileNav fl fl-ju-co-c fl-al-it-c">
    <div class="icon-bars fl fl-di-c fl-ju-co-c">
        <div class="icon-bars__item"></div>
        <div class="icon-bars__item"></div>
        <div class="icon-bars__item"></div>
    </div>
</div>