$(document).ready(function() {
    /**********************************************/
    /*
     * Слайдеры
    */
    /* Карусель услуг */
    if($('div').hasClass('main-screen-carousel')){
        $('.main-screen-carousel').slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots: false,
            arrows: false,
        });
    }

    /* Карусель услуг */
    if($('div').hasClass('theKeys-box-carousel')){
        $('.theKeys-box-carousel').slick({
            fade: false,
            infinite: true,
            variableWidth: true,
            autoplay: true,
            autoplaySpeed: 2000,
            dots: false,
            arrows: false,
        });
    }

    /* Карусель услуг */
    if($('div').hasClass('services-box-carousel')){
        $('.services-box-carousel').slick({
            fade: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
    }

    /* Карусель на главной ( Отзывы ) */
    if($('div').hasClass('reviews-box-carousel')){
        var reviewsGallery = $('.reviews-box-carousel');

        reviewsGallery.on('init reInit beforeChange', function(event, slick, currentSlide, nextSlide){
            var i = (currentSlide ? currentSlide : 0) + 1;
            // $('.slide-nav-counter').html('<span>' + ('0'+i).slice(-2) + '</span> / ' + ('0'+slick.slideCount).slice(-2));
            $('.reviews-nav-counter').html('<span>' + ('0'+i).slice(-2) + '</span> ' + ('0'+slick.slideCount).slice(-2));
        });

        reviewsGallery.slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            appendArrows: $('.reviews-nav-arrows'),
        });
    }

     /* Карусель на главное ( Почему мы ) */
    if($('div').hasClass('whyUs-box-carousel')){
        var whyUsGallery = $('.whyUs-box-carousel');

        whyUsGallery.slick({
            fade: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            variableWidth: true,
            dots: false,
            arrows: true,
            appendArrows: whyUsGallery.parents('.js-whyUs-box').find('.whyUs-box-arrows'),
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        infinite: true,
                    }
                },
            ]
        });
    }

    /* Карусель с логотипами авто */
    if($('div').hasClass('carBrand-box-carousel')){
        $('.carBrand-box-carousel').slick({
            fade: false,
            infinite: true,
            slidesToShow: 8,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1005,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
    }
});