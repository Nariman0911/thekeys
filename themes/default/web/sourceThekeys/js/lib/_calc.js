$(document).ready(function() {
	/**********************************************/
    /* 
        *** Выпадающий список *** 
    */
    /**********************************************/
    /* Открыть список фильтра */
    $(document).delegate('.wrapper-dropdown__header', 'click', function(){
        if(!$(this).parent().hasClass('active')){
            $('.wrapper-dropdown').removeClass('active');
        }
        $(this).parent().toggleClass('active');
    });

    /* Клик по любому месту, чтобы закрыть все выпадающие списки */
    $(document).delegate('body', 'click', function(e){
        if($(e.target).parents('.wrapper-dropdown').length < 1){
            $('.wrapper-dropdown').removeClass('active');
            // return false;
        }
    });

    /* Выбор Услуги */
    $('.js-calc-services').on('click', function() {
        $('.js-calc-services').removeClass('active');
        $(this).addClass('active');
    });
    /* Выбор города */
    $('.js-calc-city').on('click', function() {
        var parent = $(this).parents('.js-wrapper-dropdown');
        parent.find('.js-wrapper-dropdown-header span').text($(this).text());
        $('.js-calc-city').removeClass('active');
        $(this).addClass('active');
        parent.removeClass('active');
        /* 
         * Какой-то ajax запрос наверное надо будет написать 
        */
    });
    /* Выбор района */
    $('.js-calc-district').on('click', function() {
        $('.js-calc-district').removeClass('active');
        $(this).addClass('active');
    });
    /*************** END *************************/

});