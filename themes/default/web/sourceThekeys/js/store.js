$(document).ready(function() {
    /* Клик по марке */
    $('.jsMarkaSelect select').on('change', function() {
        var id = $(this).val();
        var action = $(this).attr('action');

        if(id != 0) {
            var data = {'marki': id};

            $('.preloader').addClass('active');

            data[yupeTokenName] = yupeToken;
             $.ajax({
                url: action,
                type: 'POST',
                data: data,
                dataType: 'html',
                success: function(html) {
                    $('#markaModels').html(html);
                },
                complete: function(html){
                    $('.jsMarkaModelSelect .select2-chosen').html('-- выберите модель --');

                    // Если есть модели, то показываем фильтр с моделями
                    if($('#markaModels option').length > 0){
                        $('.jsMarkaModelSelect').removeClass('hidden');
                    }

                    // Скрываем год и кнопку
                    hideYearAndBut();

                    setTimeout(function(){
                        $('.preloader').removeClass('active');
                    }, 300);
                }
            });
         } else {
            // Если ничего не выбираем, то скрываем все
            hideMarkaModel();
            hideYearAndBut();
        }

        return false;
    });

    /* Клик по модели */
    $('.jsMarkaModelSelect select').on('change', function() {
        var id = $(this).val();
        var action = $(this).attr('action');

        if(id != 0) {
            var data = {'markaModels': id};
            
            $('.preloader').addClass('active');

            data[yupeTokenName] = yupeToken;
             $.ajax({
                url: action,
                type: 'POST',
                data: data,
                dataType: 'html',
                success: function(html) {
                    $('#markaModelsYears').html(html);
                },
                complete: function(html){
                    $('.jsMarkaModelYearSelect .select2-chosen').html('-- выберите год --');
                    $('.jsMarkaModelYearSelect').removeClass('hidden');
                    $('.jsMarkaModelButton').removeClass('hidden');
                    
                    $('.js-product-listView').addClass('hidden');

                    setTimeout(function(){
                        $('.preloader').removeClass('active');
                    }, 300);
                }
            });
        } else {
            // Если ничего не выбираем, то скрываем все
            hideYearAndBut();
        }

        return false;
    });

    function hideMarkaModel() {
        $('.jsMarkaModelSelect .select2-chosen').html('-- выберите модель --');
        $('.jsMarkaModelSelect').addClass('hidden');
    }
    function hideYearAndBut() {
        $('.jsMarkaModelYearSelect .select2-chosen').html('-- выберите год --');
        $('.jsMarkaModelYearSelect').addClass('hidden');
        $('.jsMarkaModelButton').addClass('hidden');
        $('.js-product-listView').addClass('hidden');
    }
    /* Клик по кнопке применить фильтры */
    $(document).delegate('.but-filter', 'click', function(e){
        filterUpdate(true);
        return false;
    });

    $(document).delegate('.js-service-product-add input', 'click', function(e){
        var sum = parseFloat($(this).attr('data-sum'));
        var parent = $(this).parents('.js-product-item');
        var resultSumWrapper = parent.find('.js-price-result')
        var oldSumWrapper = parent.find('.js-price-old')

        if($(this).prop('checked')) {
            // localStorage.setItem($(this).attr('id'), 'checked');
            increaseSumProduct(sum, parent, resultSumWrapper, oldSumWrapper);
        } else {
            // localStorage.removeItem($(this).attr('id'));
            deacreaseSumProduct(sum, parent, resultSumWrapper, oldSumWrapper);
        }

        visibleTotalPrice(parent);
    });

    function visibleTotalPrice(parent) {
        if(parent.find('input:checked').length > 0) {
            parent.find('.js-product-price-total').removeClass('hidden');
        } else {
            parent.find('.js-product-price-total').addClass('hidden');
        }
    }

    function increaseSumProduct(sum, parent, resultSumWrapper, oldSumWrapper) {
        var resultSum = parseFloat(resultSumWrapper.html());
        var oldSum = parseFloat(oldSumWrapper.html());

        resultSumWrapper.html((resultSum + sum).toFixed(2)*1);
        oldSumWrapper.html((oldSum + sum).toFixed(2)*1);
    }

    function deacreaseSumProduct(sum, parent, resultSumWrapper, oldSumWrapper) {
        var resultSum = parseFloat(resultSumWrapper.html());
        var oldSum = parseFloat(oldSumWrapper.html());

        resultSumWrapper.html((resultSum - sum).toFixed(2)*1);
        oldSumWrapper.html((oldSum - sum).toFixed(2)*1);
    }

    $(document).delegate('.js-service-add input', 'click', function(e){
        var sum = parseFloat($(this).attr('data-sum'));
        var parent = $(this).parents('.js-product-item');
        var resultSumWrapper = parent.find('.js-price-result')
        var oldSumWrapper = parent.find('.js-price-old')

        var el = $(this);
        var id = el.attr('data-position-id');
        var data = {'id': id};
        data[yupeTokenName] = yupeToken;

        $('.preloader').addClass('active');
        
        if($(this).prop('checked')) {
            increaseSumProduct(sum, parent, resultSumWrapper, oldSumWrapper);
            $.ajax({
                url: yupeCartАddProductUrlNew,
                type: 'post',
                data: data,
                dataType: 'html',
                success: function (html) {
                    $('.js-orderProduct-list').html(html);
                },
                complete: function() {
                    el.parent().addClass('active');

                    setTimeout(function(){
                        $('.preloader').removeClass('active');
                    }, 300);
                }
            });
        } else {
            deacreaseSumProduct(sum, parent, resultSumWrapper, oldSumWrapper);
            $.ajax({
                url: yupeCartDeleteProductUrlNew,
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.result) {
                        updateCartListProduct(id);
                    }
                },
                complete: function() {
                    el.parent().removeClass('active');

                    setTimeout(function(){
                        $('.preloader').removeClass('active');
                    }, 300);
                }
            });
        }
    });

    function updateCartListProduct(id) {
        var data = {};
        $('.js-orderProduct-list').find('.js-position-product[data-id="'+id+'"]').remove();
        // data[yupeTokenName] = yupeToken;
        // $.ajax({
        //     url: yupeCartUpdateUrlNew,
        //     type: 'post',
        //     data: data,
        //     dataType: 'html',
        //     success: function (html) {
        //         $('.js-orderProduct-list').html(html);
        //     }
        // });
    }

    function filterUpdate(type) {
        var form = $('#marka-filter-form'),
            data = form.serialize();
            action = form.attr('action');
        if (data == '') {
            data={};
        }

        var historyurl = action+'?'+data;
        if(type == false){
            data={};
            historyurl = action;
        }
        window.history.pushState(null, document.title, historyurl);

        $('.preloader').addClass('active');
        $('.ajax-loading').fadeIn(500);
        var top = $('.js-listView-section').offset().top - 100;
        
        $('body,html').animate({
            scrollTop: top + 'px'
        }, 400);

        $.fn.yiiListView.update('product-box', {
            'data': data,
            'url': '',
            complete:function() {
                $('select').blur();
                $('.js-product-listView').removeClass('hidden');
                setTimeout(function(){
                    $('.preloader').removeClass('active');
                }, 300);
            }
        });

        $('.ajax-loading').delay(100).fadeOut(500);
        return false;
    }; 
});

$(document).ready(function () {
    var cartWidgetSelector = '.js-shopping-cart-widget';

    /*страница продукта*/
    var priceElement = $('#result-price'); //итоговая цена на странице продукта
    var basePrice = parseFloat($('#base-price').val()); //базовая цена на странице продукта
    var quantityElement = $('#product-quantity');
    var quantityInputElement = $('#product-quantity-input');

    /*корзина*/
    var shippingCostElement = $('#cart-shipping-cost'); // Сумма доставки
    var cartFullCostElement = $('.js-cart-full-cost');  // сумма заказа без доставки со скидкой
    var cartFullCostWithShippingElement = $('.js-cart-full-cost-with-shipping'); //Итого сумма с доставкой

    /* Сумма заказа без доставки без скидки */
    var cartFullCostElement2 = $('#cart-full-cost2');
    var cartDiscountCost = $('.js-cart-discount-cost');


    miniCartListeners();
    refreshDeliveryTypes();
    // checkFirstAvailableDeliveryType();
    updateAllCosts();

    // Галерея дополнительных изображений в карточке товара
    // $('.js-product-gallery').productGallery();

    // Табы в карточке товара
    // $('.js-tabs').tabs();

    // $(".js-select2").select2();

    $('#start-payment').on('click', function () {
        $('.payment-method-radio:checked').parents('.payment-method').find('form').submit();
    });

    $('body').on('click', '.clear-cart', function (e) {
        e.preventDefault();
        var data = {};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: '/coupon/clear',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
            }
        });
    });

    $('#add-coupon-code').click(function (e) {
        e.preventDefault();
        var code = $('#coupon-code').val();
        var button = $(this);
        if (code) {
            var data = {'code': code};
            data[yupeTokenName] = yupeToken;
            $.ajax({
                url: '/coupon/add',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.result) {
                        window.location.reload();
                    }
                    showNotify(button, data.result ? 'success' : 'danger', data.data.join('; '));
                }
            });
            $('#coupon-code').val('');
        }
    });

    $('.coupon .close').click(function (e) {
        e.preventDefault();
        var code = $(this).siblings('input[type="hidden"]').data('code');
        var data = {'code': code};
        var el = $(this).closest('.coupon');
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: '/coupon/remove',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                showNotify(this, data.result ? 'success' : 'danger', data.data);
                if (data.result) {
                    el.remove();
                    updateAllCosts();
                }
            }
        });
    });

    $('#coupon-code').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $('#add-coupon-code').click();
        }
    });

    $('.order-form').submit(function () {
        $(this).find("button[type='submit']").prop('disabled', true);
    });

    $('select[name="ProductVariant[]"]').change(function () {
        updatePrice();
    });

    $('.product-quantity-increase').on('click', function () {
        quantityInputElement.val(parseInt(quantityInputElement.val()) + 1).trigger('change');
    });

    $('.product-quantity-decrease').on('click', function () {
        if (parseInt(quantityInputElement.val()) > 1) {
            quantityInputElement.val(parseInt(quantityInputElement.val()) - 1).trigger('change');
        }
    });

    $('#product-quantity-input').change(function (event) {
        var el = $(this);
        quantity = parseInt(el.val());

        if (quantity <= 0 || isNaN(quantity)) {
            quantity = 1;
        }

        var quantityLimiterEl = el.parents('.spinput'),
            minQuantity = parseInt(quantityLimiterEl.data('min-value')),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'));

        if (quantity < minQuantity) {
            quantity = minQuantity;
        }
        else if (quantity > maxQuantity) {
            quantity = maxQuantity;
        }

        el.val(quantity);
        quantityElement.text(quantity);
        $('#product-total-price').text(parseFloat($('#result-price').text()) * quantity);
    });

    $('body').on('click', '#add-product-to-cart', function (e) {
        e.preventDefault();
        var button = $(this);
        var form = $(this).parents('form');
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            url: form.attr('action'),
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                showNotify(button, data.result ? 'success' : 'danger', data.data);
            }
        });
    });

    $('body').on('click', '.quick-add-product-to-cart', function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'Product[id]': el.data('product-id')};
        el.addClass('active');
        el.parents('.js-product-item').addClass('active');
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: el.data('cart-add-url'),
            type: 'post',
            data: data,
            dataType: 'html',
            success: function (data) {
                $('.js-addCartModal-body').html(data);
                $('#buyOneClickModal').modal('show');
                // if (data.result) {
                    updateCartWidget();
                    // setTimeout(function(){
                    //     el.removeClass('active');
                    //     el.parents('.js-product-item').removeClass('active');
                    //     el.removeClass('quick-add-product-to-cart but-svg but-svg-left but-animation').addClass('but-go-cart').attr('href', '/cart').find('span').html("В корзине");
                    // }, 600);
                    // el.off('click','.quick-add-product-to-cart');
                    // el.removeClass('btn_cart')
                    //     .addClass('btn_success')
                    //     .html('Оформить заказ')
                    //     .attr('href', '/cart');
                // }
                // showNotify(el, data.result ? 'success' : 'danger', data.data);
            }
        });
    });

    /* 
     * количество у продукта
    */
    // $('.product-box-quantity-increase').on('click', function () {
    $(document).delegate('.product-box-quantity-increase', 'click', function(){
        var quantityLimiterEl = $(this).parents('.spinput'),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'))
        var input = quantityLimiterEl.find('input');
        if(parseInt(input.val()) < maxQuantity){
            input.val(parseInt(input.val()) + 1).trigger('change');
        }
        // showNotify(input, 'success', "Успешно добавлено");
    });

    // $('.product-box-quantity-decrease').on('click', function () {
    $(document).delegate('.product-box-quantity-decrease', 'click', function(){
        var quantityLimiterEl = $(this).parents('.spinput');
        var input = quantityLimiterEl.find('input');
        if (parseInt(input.val()) > 1) {
            input.val(parseInt(input.val()) - 1).trigger('change');
            // showNotify(input, 'success', "Успешно удален");
        }
    });

    $('.product-box-quantity-input').bind("keyup", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }

        if (this.value <= 0 || isNaN(this.value)) {
            this.value = 1;
        }

        var quantityLimiterEl = $(this).parents('.spinput'),
            minQuantity = parseInt(quantityLimiterEl.data('min-value')),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'));

        if (this.value < minQuantity) {
            this.value = minQuantity;
        }
        else if (this.value > maxQuantity) {
            this.value = maxQuantity;
        }
    });


    $('.cart-quantity-increase').on('click', function () {
        var target = $($(this).data('target'));
        target.val(parseInt(target.val()) + 1).trigger('change');
    });

    $('.cart-quantity-decrease').on('click', function () {
        var target = $($(this).data('target'));
        if (parseInt(target.val()) > 1) {
            target.val(parseInt(target.val()) - 1).trigger('change');
        }
    });

    $(document).on('change', 'input[name="Order[delivery_id]"], input[name="Order[sub_delivery_id]"]', function () {
        updateShippingCost();
    });

    function miniCartListeners() {
        $('.mini-cart-delete-product').click(function (e) {
            e.preventDefault();
            var el = $(this);
            var data = {'id': el.data('position-id')};
            data[yupeTokenName] = yupeToken;
            $.ajax({
                url: yupeCartDeleteProductUrl,
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.result) {
                        updateCartWidget();
                    }
                }
            });
        });

        $('#cart-toggle-link').click(function (e) {
            e.preventDefault();
            $('#cart-mini').toggle();
        });
    }

    function refreshDeliveryTypes() {
        var cartTotalCost = getCartTotalCost();
        $.each($('input[name="Order[delivery_id]"]'), function (index, el) {
            var elem = $(el);
            var availableFrom = elem.data('available-from');
            if (availableFrom.length && parseFloat(availableFrom) >= cartTotalCost) {
                if (elem.prop('checked')) {
                    checkFirstAvailableDeliveryType();
                }
                elem.prop('disabled', true);
            } else {
                elem.prop('disabled', false);
            }
        });
    }

    function checkFirstAvailableDeliveryType() {
        $('input[name="Order[delivery_id]"]:not(:disabled):first').prop('checked', true);
    }

    function getShippingCost() {
        var cartTotalCost = getCartTotalCost();
        var coupons = getCoupons();
        var freeShipping = false;
        $.each(coupons, function (index, el) {
            if (el.free_shipping && cartTotalCost >= el.min_order_price) {
                freeShipping = true;
            }
        });
        if (freeShipping) {
            return 0;
        }
        var selectedDeliveryType = $('input[name="Order[delivery_id]"]:checked');
        var selectedSubDeliveryType = $('input[name="Order[sub_delivery_id]"]:checked');
        if (!selectedDeliveryType[0] && !selectedSubDeliveryType[0]) {
            return 0;
        }

        if (selectedSubDeliveryType.length > 0) {
            if (parseInt(selectedSubDeliveryType.data('separate-payment')) || parseFloat(selectedSubDeliveryType.data('free-from')) <= cartTotalCost) {
                return 0;
            } else {
                return parseFloat(selectedSubDeliveryType.data('price'));
            }
        } else if(selectedDeliveryType.length > 0) {
            if (parseInt(selectedDeliveryType.data('separate-payment')) || parseFloat(selectedDeliveryType.data('free-from')) <= cartTotalCost) {
                return 0;
            } else {
                return parseFloat(selectedDeliveryType.data('price'));
            }
        }
    }

    function updateShippingCost() {
        if (getShippingCost() > 0) {
            shippingCostElement
                .html(getShippingCost())
                .parents('.cart-total')
                .removeClass('hidden');
        } else {
            shippingCostElement
                .parents('.cart-total')
                .addClass('hidden');
        }
        updateFullCostWithShipping();
    }

    function updateFullCostWithShipping() {
        var costRes = getShippingCost() + getCartTotalCost();
        if (costRes > 0) {
            cartFullCostWithShippingElement.html(number_format(costRes, 2, '.', ' '));
        }
    }

    function updateAllCosts() {
        updateCartTotalCost();
    }

    function updatePrice() {
        var _basePrice = basePrice;
        var variants = [];
        var varElements = $('select[name="ProductVariant[]"]');
        /* выбираем вариант, меняющий базовую цену максимально*/
        var hasBasePriceVariant = false;
        $.each(varElements, function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                switch (variant.type) {
                    case 2: // base price
                        // еще не было варианта
                        if (!hasBasePriceVariant) {
                            _basePrice = variant.amount;
                            hasBasePriceVariant = true;
                        }
                        else {
                            if (_basePrice < variant.amount) {
                                _basePrice = variant.amount;
                            }
                        }
                        break;
                }
            }
        });
        var newPrice = _basePrice;
        $.each(varElements, function (index, elem) {
            var varId = elem.value;
            if (varId) {
                var option = $(elem).find('option[value="' + varId + '"]');
                var variant = {amount: option.data('amount'), type: option.data('type')};
                variants.push(variant);
                switch (variant.type) {
                    case 0: // sum
                        newPrice += variant.amount;
                        break;
                    case 1: // percent
                        newPrice += _basePrice * ( variant.amount / 100);
                        break;
                }
            }
        });

        var price = parseFloat(newPrice.toFixed(2));
        priceElement.html(price);
        $('#product-result-price').text(price);
        $('#product-total-price').text(price * parseInt($('#product-quantity').text()));
    }

    function updateCartWidget() {
        // $(cartWidgetSelector).load($('.js-cart-widget').data('cart-widget-url'), function () {
        //     miniCartListeners();
        // });
    }

    function getCoupons() {
        var coupons = [];
        $.each($('.coupon-input'), function (index, elem) {
            var $elem = $(elem);
            coupons.push({
                code: $elem.data('code'),
                name: $elem.data('name'),
                value: $elem.data('value'),
                type: $elem.data('type'),
                min_order_price: $elem.data('min-order-price'),
                free_shipping: $elem.data('free-shipping')
            })
        });
        return coupons;
    }

    function changePositionQuantity(productId, quantity) {
        var data = {'quantity': quantity, 'id': productId};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: yupeCartUpdateUrl,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                else {
                    showNotify(this, 'danger', data.data);
                }
            }
        });
    }

    /* анимация перемещения товара  в корзину */
    function moveCartProduct(elem){
        var product = elem.parents('.js-product-item').find(".js-product-image");
        var leftCart = $(".header-bot .js-shopping-cart-widget").offset().left;
        var topCart = $(".header-bot .js-shopping-cart-widget").offset().top;

        if($(".header-fix-content").hasClass('active')){
            leftCart = $(".header-fix .js-shopping-cart-widget").offset().left;
            topCart = $(".header-fix .js-shopping-cart-widget").offset().top;
        }

        $(product).clone().css({
            'position' : 'absolute',
            'z-index' : '9999',
            'opacity' : '0.5',
            top: $(product).offset().top,
            left:$(product).offset().left,
            width: 240
        }).appendTo("body").animate({
            opacity: 0.1,
            left: leftCart,
            top: topCart,
            width: 20}, 1000, function() {
            $(this).remove();
        });
    }
    
    /*
    ******************************************************
    ******************************************************

    ======================================================
    // Все, что между комментарий
    // ["START NEW CART" и "END NEW CART"]
    // НЕОБХОДИМО УДАЛИТЬ (ЗАКОММЕНТИРОВАТЬ) из store.js
    ======================================================
    
    ======================================================
    // Необходимо удалить закрывающие скобки в store.js, для того, 
    чтобы текущий код вставился $(document).ready(function() store.js
    ======================================================

    ======================================================
    // Закоментировать вызов функции сверху
       checkFirstAvailableDeliveryType();
    ======================================================

    // Заменить переменные вначале в store.js 
       var shippingCostElement = $('#cart-shipping-cost'); // Сумма доставки
       var cartFullCostElement = $('.js-cart-full-cost');  // сумма заказа без доставки со скидкой
       var cartFullCostWithShippingElement = $('.js-cart-full-cost-with-shipping'); //Итого сумма с доставкой

       var cartFullCostElement2 = $('#cart-full-cost2');
       var cartDiscountCost = $('.js-cart-discount-cost');

    ******************************************************
    ******************************************************
    */

    // $(document).ready(function() {
    
    updatePositionListDiscount();

    /*============ START NEW CART========================*/
    $(document).delegate('.cart-delete-product', 'click', function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'id': el.data('position-id')};
        data[yupeTokenName] = yupeToken;
        $.ajax({
            url: yupeCartDeleteProductUrl,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    el.closest('.js-cart-item').remove();

                    if ($('.cart-list .js-cart-item').length == 0) {
                        $('.cart-section').remove();
                        $('.empty-cart').css({'opacity': 1, 'display': 'block'});
                    }
                    // $('#cart-total-product-count').text($('.cart-list .cart-item').length);
                    positionCountPr();
                    updateCartTotalCost();
                    updateCartWidget();
                }
            }
        });
    });

    $(document).delegate('.position-count', 'change', function () {
        var el = $(this).parents('.js-cart-item'),
            positionCountEl = el.find('.position-count');

        var quantity = parseInt(positionCountEl.val());
        var productId = el.find('.position-id').val();

        if (quantity <= 0 || isNaN(quantity)) {
            quantity = 1;
        }

        var quantityLimiterEl = el.find('.spinput'),
            minQuantity = parseInt(quantityLimiterEl.data('min-value')),
            maxQuantity = parseInt(quantityLimiterEl.data('max-value'));

        if (quantity < minQuantity) {
            quantity = minQuantity;
        }
        else if (quantity > maxQuantity) {
            quantity = maxQuantity;
        }

        positionCountEl.val(quantity);

        updatePositionSumPrice(el);
        changePositionQuantity(productId, quantity);
        positionCountPr();
    });

    function positionCountPr(){
        var quantityPr = 0;
        $(".cart-list .js-cart-item").each(function(){
            quantityPr = parseInt(quantityPr) + parseInt($(this).find('.position-count').val());
        });
        $('#cart-total-product-count').text(quantityPr);
    }

    function getPositionsCost() {
        var cost = 0;
        $.each($('.position-sum-price'), function (index, elem) {
            cost += parseFloat($(elem).text());
        });

        return cost;
    }

    /* Общая сумма со скидкой */
    function getCartTotalCost() {
        var cost = getPositionsCost();
        var delta = 0;
        var coupons = getCoupons();
        $.each(coupons, function (index, el) {
            if (cost >= el.min_order_price) {
                switch (el.type) {
                    case 0: // руб
                        delta += parseFloat(el.value);
                        break;
                    case 1: // %
                        delta += (parseFloat(el.value) / 100) * cost;
                        break;
                }
            }
        });

        return delta > cost ? 0 : cost - delta;
    }

    /* Общая сумма без скидки у товаров */
    function getCartTotalNoDiscountCost() {
        var cost = 0;
        $.each($('.position-full-sum-price'), function (index, elem) {
            cost += parseFloat($(elem).text());
        });
        var delta = 0;
        var coupons = getCoupons();
        $.each(coupons, function (index, el) {
            if (cost >= el.min_order_price) {
                switch (el.type) {
                    case 0: // руб
                        delta += parseFloat(el.value);
                        break;
                    case 1: // %
                        delta += (parseFloat(el.value) / 100) * cost;
                        break;
                }
            }
        });

        return delta > cost ? 0 : cost - delta;
    }
    /* Общая сумма скидки */
    function getCartDiscountCost() {
        var cost = 0;
        $.each($('.position-discount-sum-price'), function (index, elem) {
            cost += parseFloat($(elem).text());
        });

        return cost;
    }

    function updateCartTotalCost() {
        // cartFullCostElement.html(getCartTotalCost().toFixed(2));
        cartFullCostElement.html(number_format(getCartTotalNoDiscountCost(), 2, '.', ' '));
        refreshDeliveryTypes();
        updateShippingCost();
        updateFullCostWithShipping();
        checkMinAmountFunc();
        var discountTotal = getCartDiscountCost();
        if (discountTotal > 0) {
            cartDiscountCost.html('- ' + number_format(getCartDiscountCost(), 2, '.', ' '));
            cartDiscountCost.parents('.cart-total').removeClass('hidden');
        } else{
            cartDiscountCost.parents('.cart-total').addClass('hidden');
        }
    }

    function updatePositionSumPrice(tr) {
        /*var count = parseInt(tr.find('.position-count').val());
        var price = parseFloat(tr.data('price'));
        var possum = (price * count).toFixed(2);
        tr.find('.position-sum-price').html(possum);*/

        updatePositionDiscount(tr);

        updateCartTotalCost();
    }

    function updatePositionListDiscount() {
        $(".cart-list .js-cart-item").each(function(){
            updatePositionDiscount($(this));
        });
        updateCartTotalCost();
    }
    function updatePositionDiscount(elem) {
        var count = parseInt(elem.find('.position-count').val());
        var price = parseFloat(elem.data('price'));
        var baseprice = parseFloat(elem.data('base-price'));
        var discount = parseFloat(elem.data('discount-price'));

        price = (price * count).toFixed(2);
        baseprice = (baseprice * count).toFixed(2);
        discount = (discount * count).toFixed(2);

        elem.find('.position-sum-price').html(price);
        elem.find('.position-full-sum-price').html(baseprice);
        elem.find('.position-discount-sum-price').html(discount);

        elem.find('.js-cartPrice-with-discount').html(number_format(price, 2, '.', ' '));
        elem.find('.js-cartPrice-without-discount').html(number_format(baseprice, 2, '.', ' '));
        elem.find('.js-cartPrice-benefit').html(number_format(discount, 2, '.', ' '));
    }

    /*============ END NEW CART========================*/

    // Ограничения на покупку
    function checkMinAmountFunc() {
        if (typeof minAmount === 'undefined') {
            return ;
        }

        if (minAmount > getPositionsCost()) {
            $('.js-check-min-amount').removeClass('hide');
            $('.js-next-button').addClass('hide');
            $('.js-return-url').removeClass('hide');
        } else {
            $('.js-check-min-amount').addClass('hide');
            $('.js-next-button').removeClass('hide');
            $('.js-return-url').addClass('hide');
        }
    }

    /*Пказать/скрыть статичную таблицу с товарами в корзине*/
    $('.js-static-table-toggle').on('click', function() {
        var link = $(this),
        toggle = link.data('target'),
        table = $(toggle);

        link.toggleClass('active');

        table.toggle();

        return false;
    });

    /**
     * Авторизация, регистрация, модальное окно
     */
    $(document).on('submit', '#ajax-login, #ajax-registration', function(e) {
        var form = $(this),
        id = form.attr('id'),
        action = form.attr('action'),
        method = form.attr('method'),
        data = form.serialize();

        $.ajax({
            url: action,
            type: method,
            data: data,
            dataType: 'html',
            success: function(data) {
                $('#'+id).html(data);
            },
        })
        return false;
    });

    /**
     * Модуль доставка - новыый
     */

    // Вывод тарифов и спопосов оплаты
    $(document).on('change', 'input[name=\'Order[delivery_id]\']', function() {
        var input = $(this);
        $('.js-items-delivery_id').removeClass('active');
        input.parents('.js-items-delivery_id').addClass('active');
        $(".preloader").addClass("active");

        $.ajax({
            url: deliveryMethodUrl,
            type: 'post',
            data: input.parents('form').serialize(),
            success: function(data) {
                $('.js-delivery-method')
                    .fadeOut({
                        done: function() {
                            $(this)
                                .html(data)
                                .fadeIn();
                        }
                    });
            },
            complete: function () {
                $(".preloader").removeClass("active");
            }
        });

        return false;
    });

    /**
     * Самовывоз
     */
    var modalMap = null;
    /*$('a[href="#pickup-modal"]')
        .fancybox({
            'animationEffect': 'fade',
            'buttons': ['zoom','close'],
            'beforeClose': function() {
                if (modalMap) {
                    modalMap.destroy();
                }
            }
        });*/
    $(document)
        .on('click', 'a[href="#pickup-modal"]', function(e) {
            var elem = $(this);
            var modal = elem.attr('href');
            var item = elem.parents('.js-pickup-item');
            var data = item.data();
            $.each(data, function(key, value) {
                var selector = '.js-pickup-modal-'+key;
                if ($(selector).length > 0 ) {
                    $(selector).html(value);
                }
            });

            if (data.latitude && data.longitude) {
                ymaps.ready(function() {
                    modalMap = new ymaps.Map("map", {
                        center: [data.latitude, data.longitude],
                        controls: [],
                        zoom: 17
                    });
                    var point = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: [data.latitude, data.longitude],
                        },
                    }, {
                        preset: "islands#blackDotIconWithCaption",
                        iconColor: "#0074FF",
                    });
                    modalMap.geoObjects.add(point);
                });
                $(modal).modal('show');
            }
            return false
        });

    var pickupItemMap;
    $(document).on('change', 'input[name="Order[pickup][]"], input[name="Order[cdek_pvz][]"]', function() {
        var elem = $(this);
        var itemContainer = elem.parents('.js-pickup-item');
        var mapContainer = itemContainer.find('.js-pickup-item__map');
        var buttonSpan = itemContainer.find('.pickup-checkbox span');
        var data = itemContainer.data();

        if (elem.prop('checked')) {
            $('input[name="Order[pickup][]"]').each(function(i, e) {
                if(elem.val() != $(e).val()) {
                    $(e)
                        .parents('.js-pickup-item')
                        .hide()
                        .find('.pickup-checkbox span')
                        .text('Забрать отсюда');
                }
            });

            buttonSpan.text('Выбрать другой');

            if (data.latitude && data.longitude) {
                mapContainer.css({'height': '150px', 'width': '100%'}).show();
                ymaps.ready(function() {
                    pickupItemMap = new ymaps.Map(mapContainer.get(0), {
                        center: [data.latitude, data.longitude],
                        controls: ['geolocationControl'],

                        zoom: 17
                    });
                    var point = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: [data.latitude, data.longitude],
                        },
                    }, {
                        preset: "islands#blackDotIconWithCaption",
                        iconColor: "#0074FF",
                    });
                    pickupItemMap.geoObjects.add(point);
                });
            }

        } else {
            pickupItemMap.destroy();
            $('.js-pickup-item').each(function(i, e) {
                $(e).show();
            });
            buttonSpan.text('Забрать отсюда');
            mapContainer.css({'height': '0px', 'width': '0'}).hide();
        }
    });
    $(document).on('click', '.js-pickupModal-checkbox', function(e) {
        var elem = $(this);
        var itemContainer = elem.parents('.js-pickupModal-item');
        var id = parseInt(itemContainer.find('.js-pickup-modal-id').text());
        $('.js-pickup-item input[type="checkbox"]').prop('checked', false);
        $('.js-pickup-item[data-id="'+id+'"]').find('input[type="checkbox"]').prop("checked",true).trigger('change');
        elem.parents('.modal').modal('hide');
        return false;
    });

    $(document).on('hidden.bs.modal','#pickup-modal', function () {
        $(".js-pickupModal-map").html('');
    });
    /**
     * Самовывоз end
     */


    /**
     * модуль СДЭК
     */
    $(document).on('change', 'input[name="Order[sub_delivery_id]"]', function() {
        var item = $(this);
        var action = item.data('action');
        var container = $('.js-sub-delivery');

        $('.js-items-sub_delivery_id').removeClass('active');
        item.parents('.js-items-sub_delivery_id').addClass('active');

        $.ajax({
            url: action,
            type: 'post',
            // dataType: 'json',
            data: item.parents('form').serialize(),
            success: function(data) {
                container.html(data);
            }
        });

        return false;
    });

    /**
     * Модуль доставка - новыый end
     */


    function number_format(number, decimals, dec_point, separator ) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof separator === 'undefined') ? ',' : separator ,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k).toFixed(prec);
        };
        // Фиксим баг в IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
});