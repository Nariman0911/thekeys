$(document).ready(function() {
    $(window).scroll(function(){
        fixMenu();
    });
    
    fixMenu()

    function fixMenu() {
        var $menu = $(".header-fix");
        if ($(this).scrollTop() <= 150){
            $menu.removeClass("fixed");
        } else if($(this).scrollTop() > 150) {
            $menu.addClass("fixed");
        }
    }
    
	/**
	 * ajax на формах
	 */
	$('form[data-type="ajax-form"]').on('click', function(event) {
        var elem = event.target;
        var dataSend = elem.getAttribute('data-send');
        $(elem).addClass('active');
        if (dataSend=='ajax') {
            var
                button   = $(elem),
                form     = button.parents('form'),
                type     = form.attr('method'),
                formId   = form.attr('id');

            // form.loader('show');

            if(form.hasClass('form-file')){
                var data = new FormData(form.get(0));
                $.ajax({
                    type: type,
                    data: data,
                    enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,
                    async: false,
                    cache: false,
                    success: function (html) {
                        var newForm = $(html).find('#'+formId);

                        $('#'+formId).html(newForm.html());

                        if($('#'+formId).find('input[data-mask=phone]').is('.data-mask')){
                            $('#'+formId).find('input[data-mask=phone]')
                                .mask('+7(999)999-99-99', {
                                    'placeholder':'_',
                                    'completed':function() {
                                        //console.log('ok');
                                    }
                                });
                        }
                        
                        if($('#'+formId).find('input[data-phoneMask="phone"]').is('.phone-mask')){
                            $(".phone-mask").inputmask("+7(999)999-99-99", {inputmode: "numeric"});
                        }

                        $.getScript('https://www.google.com/recaptcha/api.js', function () {});
                    }
                });
            } else{
                $.ajax({
                    type: type,
                    data:  form.serialize(), //formData,
                    dataType: 'html',
                    success: function(html) {
                        var frm = '#'+formId;
                        if('#'+formId === '#order-form'){
                            frm = '.js-buyOneClick-form';
                        }

                        var newForm = $(html).find(frm);

                        $(frm).html(newForm.html());

                            if($(frm).find('input[data-mask="phone"]').is('.data-mask')){
                                $(frm).find('input[data-mask="phone"]')
                                    .mask("+7(999)999-99-99", {
                                        'placeholder':'_',
                                        'completed':function() {
                                            //console.log("ok");
                                        }
                                    });
                            }
                            if($(frm).find('input[data-phoneMask="phone"]').is('.phone-mask')){
                                $(".phone-mask").inputmask("+7(999) 999-99-99", {inputmode: "numeric"});
                            }
                                
                            $.getScript("https://www.google.com/recaptcha/api.js", function () {});
                        },
                });  
            }

            return false;
        }
    });

    // Инициализация маски
    $(".phone-mask").inputmask(phoneMaskTemplate, {inputmode: "numeric"});

    /*
     * Menu при адаптации
    */
    // Событие открывания меню на телефонах
    $('.js-menu-mobileNav').on('click', function() {
        if($(this).hasClass('active')){
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(this).removeClass('active');
            $(".mobileNav, .mobileNav-box, .mobileNav__icon-close").removeClass('active');
        } else {
            $('html').addClass('htmlmenu');
            $('body').addClass('bodymenu');
            $(this).addClass('active');
            $(".mobileNav").addClass('active');
            $(".mobileNav-box").addClass('active');
            $(".mobileNav__icon-close").addClass('active');
        }
        return false;
    });
    $('.mobileNav').on('click', function(e){
        console.log($(e.target));
        if($(e.target).hasClass('mobileNav__icon-close') || $(e.target).parents('.mobileNav__icon-close').length > 0 || $(e.target).hasClass('mobileNav')){
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(".js-menu-mobileNav, .mobileNav, .mobileNav-box, .mobileNav__icon-close").removeClass('active');
        }
    });
    var menumain = $('.header .menu .menu-main').html();
    $('.mobileNav .menu-mobile').prepend(menumain);

    $('.mobileNav .menu-mobile li ul').wrap('<div class="menu-mobile-wrapper"></div>');
    $('.mobileNav .menu-mobile li a').click(function(){
        var parent = $(this).parent();
        if(parent.hasClass('submenuItem')){
            parent.find(".menu-mobile-wrapper:first").addClass('active').prepend('<a class="prevLink" href="#">Назад</a>');
            return false;
        }
    });

    $('.mobileNav .menu-mobile li.listItemParent').each(function(i){
        if($(this).hasClass('submenuItem')){
            var el = $(this).find('a:first');
            var link_href = el.attr('href');
            link = "<a class='listItem-nav__link' href='"+link_href+"'>"+el.html()+"</a>";
            $(this).find(".menu-mobile-wrapper ul:eq(0)").prepend("<li class='listItem listItem-nav'>" + link + "</li>");
            appendLi($(this).find(".menu-mobile-wrapper ul:eq(0) > li.submenuItem"), link);
        }
    });

    function appendLi(element, link_prev){
        var link_res;
        var count = 1;
        element.each(function(index){
            link_res = link_prev;
            if($(this).hasClass('listItemFloor')){
                link_prev = '<a class="listItem-nav__link" href="#">Магазины</a>'
            }
            if($(this).hasClass('submenuItem')){
                var el = $(this).find('a:first');
                var link_href = el.attr('href');
                var link_active = "<a class='listItem-nav__link' href='"+link_href+"'>"+el.html()+"</a>";
                link_res += link_active;
                $(this).find(".menu-mobile-wrapper ul:eq(0)").prepend("<li class='listItem listItem-nav'>" + link_prev + link_active + "</li>");
                appendLi($(this).find(".menu-mobile-wrapper ul:eq(0) > li.submenuItem"), link_res);
            }
        })
    }

    $(document).delegate('.mobileNav .menu-mobile .prevLink', 'click', function(){
        var el = $(this);
        var parent = $(this).parent();
        if(parent.hasClass('active')){
            parent.removeClass('active');
            setTimeout(function(){
                el.remove();
            }, 600);
        }
        return false;
    });
    /**********************************************/

    /*
     * Действия при изменении окна 
    */
    $(window).resize(function () {
        getMoreReview();
    });

    /**********************************************/
    /*
     *** Кнопка больше информации СЕО-ТЕКСТ ***
    */
    $('.js-seo-txt-section-link').on('click', function(){
        var parent = $('.js-seo-txt-section');
        var txt = $(this).data('text');
        var txt_hidden = $(this).html();
        var height = parent.find('.js-seo-txt-more').height();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            parent.removeClass('active').attr('style', '');
            $(this).html(txt).data('text', txt_hidden);
        } else {
            $(this).addClass('active');
            parent.addClass('active').css('max-height', height);
            $(this).html(txt).data('text', txt_hidden);
        }
        return false;
    });
    /**********************************************/

    /*if($('form').hasClass('form')){
        findFilled();
    }*/

    /*
     *  Отзывы
    */
    // фунция определения длины текста, если много добавляем кнопку
    function getMoreReview() {
        if($('div').hasClass('js-reviews-item')){
            $('.js-reviews-item').each(function () {
                var desc = $(this).find('.js-reviews-desc').height();
                var text = $(this).find('.js-reviews-text').height();
                if(text >= desc) {
                    $(this).find('.js-reviews-more').show();
                } else {
                    $(this).find('.js-reviews-more').hide();
                }
            });
        }
    }
    getMoreReview();

    // Клик по кнопке читать весь отзыв
    $(document).delegate('.js-reviews-more', 'click', function(){
        var modal = $('.js-reviewsModal');
        var content = $(this).parents('.js-reviews-item').html();
        modal.find('.reviews-box').html(content);
        modal.modal('show');
        return false;
    });
    /* Воспроизведение аудиоотзыва */
    $(document).delegate('.js-review-audio-play', 'click', function(){
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#review-hidden-player').get(0).pause();
        } else {
            $('.js-review-audio-play').removeClass('active');
            $(this).addClass('active');
            var pl = $('#review-hidden-player').get(0);
            pl.src = $(this).attr('data-src');
            pl.play();
        }
        return false;
    });
     /*
     *****************************************
    */

    if($('div').hasClass('js-theKeys-container')){
        var block = $('.js-theKeys-container');
        $(window).scroll (function () {
            var scrollTop = $(window).scrollTop();
            var innerHeight = $(window).innerHeight();
            var x = block.position().top - scrollTop - innerHeight;
            $('.js-theKeys-carousel').css('transform', 'translateX('+ x +'px)')
        });
        $(document).scroll(function(){    
        });

    }
});


function findFilled() {
    $('.form .form-group-filled').each(function(){
        var input = $(this).find('input');
        if(input.length > 0){
            var i = input.val().indexOf('_');
            if(input.hasClass('phone-mask') && i < 16){
                i = '-1';
            }
        } else {
            var input = $(this).find('textarea');
            var i = '-1';
        }

        if(input.val().length > 0 && i == -1){
            input.addClass('filled');
        } else{
            input.removeClass('filled');
        }
    });
    $('.form .form-group-filled input, .form .form-group-filled textarea').on('focusout', function (event) {
        var i = $(this).val().indexOf('_');
        if($(this).hasClass('phone-mask') && i < 16){
            i = '-1';
        }
        if($(this).val().length > 0 && i == -1){
            $(this).addClass('filled');
        } else{
            $(this).removeClass('filled');
        }
    });
}

// @prepros-append "./lib/slick.js"
// @prepros-append "./lib/jquery.inputmask.js"
// @prepros-append "./lib/fancybox.js"
// @prepros-append "./lib/jquery.cookie.js"
// @prepros-append "./lib/_carousel.js"
// @prepros-append "./lib/_calc.js"
// @prepros-append "store.js"